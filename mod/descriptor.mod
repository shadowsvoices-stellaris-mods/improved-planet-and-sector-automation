name="Improved Planet and Sector Automation"
path="mod/improved-planet-automation"
tags={
	"Buildings"
	"Economy"
	"Utilities"
	"AI"
}
supported_version="3.6.*"
picture="mod_splash.jpg"
