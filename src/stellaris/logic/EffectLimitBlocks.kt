package stellaris.logic

import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

open class EffectLimitBlock(blockKey: String? = "limit") : ConditionalBlock<EffectLimitBlock>(blockKey) {

    override fun newChildBlock(key: String): EffectLimitBlock {
        return EffectLimitBlock(key)
    }

}

open class CountryEffectLimitBlock(blockKey: String? = "limit") :
    RawCountryConditionalBlock<CountryEffectLimitBlock>(blockKey) {

    override fun newChildBlock(key: String): CountryEffectLimitBlock {
        return CountryEffectLimitBlock(key)
    }

}

open class PlanetEffectLimitBlock(blockKey: String? = "limit") :
    RawPlanetaryConditionalBlock<PlanetEffectLimitBlock>(blockKey) {

    override fun newChildBlock(key: String): PlanetEffectLimitBlock {
        return PlanetEffectLimitBlock(key)
    }

}

open class CountPopBlock(blockKey: String = "count_owned_pop") : TemplateBlock(blockKey) {

    private var limitBlock: PopConditionalBlock? = null
        get() {
            if (field == null) {
                field = PopConditionalBlock("limit")
                children.add(field!!)
            }
            return field
        }

    fun limit(block: PopConditionalBlock.() -> Unit) {
        limitBlock!!.block()
    }

    private var count: Int? = null
    private var countCmp: Comparators? = null

    fun count(count: Int, cmp: Comparators = Comparators.EQ) {
        this.count = count
        this.countCmp = cmp
    }

    override fun renderAfterChildren(render: TemplateRender) {
        require(count != null) { "a count block requires an integer value for the count condition" }
        render.add("count $countCmp $count")
        super.renderAfterChildren(render)
    }
}

fun ConditionalBlock<*>.countOwnedPop(block: CountPopBlock.() -> Unit): CountPopBlock {
    return addChild(CountPopBlock("count_owned_pop").also(block))
}
