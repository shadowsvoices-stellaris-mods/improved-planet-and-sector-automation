package stellaris.logic

import ipa.colonyautomation.ColonyAutomationPriority
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender
import stellaris.types.Job
import stellaris.types.Variables

/**
 * See: https://stellaris.paradoxwikis.com/Conditions
 */
abstract class ConditionalBlock<T : ConditionalBlock<T>>(blockKey: String?) : TemplateBlock(blockKey) {

    val conditions: MutableList<String> = mutableListOf()

    /**
     * workaround: decide which combinator to use
     * @deprecated
     */
    @Deprecated("problematic, dont use ")
    protected fun childBlockKey(): String {
        when (blockKey) {
            "AND" -> return "AND"
            "OR" -> return "OR"
            "NOR" -> return "NOR"
            "modifier" -> return "AND"
            "trigger" -> return "AND"
            "available" -> return "AND"
            "NOT" -> return "NOT" // TODO verify NOT works correctly, in the past there were issues, see git history
            else -> {
                error("invalid blockKey: $blockKey")
            }
        }
    }

    override fun render(render: TemplateRender) {
        // omit empty conditional blocks
        if (children.isEmpty() && !hasConditions()) return
        super.render(render)
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        super.renderBeforeChildren(render)
        if (hasConditions()) {
            renderConditions(render)
        }
    }

    open fun hasConditions(): Boolean {
        return conditions.isNotEmpty()
    }

    open fun renderConditions(render: TemplateRender) {
        for (condition in conditions) {
            render.add(condition)
        }
    }

    fun childBlock(key: String, block: T.() -> Unit): T {
        val child = newChildBlock(key)
        child.block()
        children.add(child)
        return child
    }

    abstract fun newChildBlock(key: String): T;

    fun OR(block: T.() -> Unit): T {
        return childBlock("OR", block)
    }

    fun AND(block: T.() -> Unit): T {
        return childBlock("AND", block)
    }

    fun NAND(block: T.() -> Unit): T {
        return childBlock("NAND", block)
    }

    /**
     * experimental, needs testing
     * Returns true if at least amount conditions return true.
     */
    fun calcTrueIf(amount: Int, cmp: Comparators, block: T.() -> Unit): T {
        return childBlock("calc_true_if", block).also { conditions.add("amount $cmp $amount") }
    }

    fun NOR(block: T.() -> Unit): T {
        return childBlock("NOR", block)
    }

    fun NOT(block: T.() -> Unit): T {
        return childBlock("NOT", block)
    }

    fun has(key: String, cmp: Comparators = Comparators.EQ, value: String = "yes") {
        conditions.add("$key $cmp $value")
    }

    fun rawCondition(condition: String) {
        conditions.add(condition)
    }

    fun hasModifier(modifier: String) {
        conditions.add("has_modifier = $modifier")
    }

    fun hasAvailableJobs(job: String) {
        conditions.add("has_available_jobs = $job")
    }

    fun hasAvailableJobs(job: Job) {
        conditions.add("has_available_jobs = $job")
    }

    fun hasColonyAutomationPriority(priority: ColonyAutomationPriority) {
        checkVariable(Variables.ColonyAutomationPriority, Comparators.EQ, priority.priority)
    }

    fun hasForbiddenJobs(job: String) {
        conditions.add("has_forbidden_jobs = $job")
    }

    fun hasForbiddenJobs(job: Job) {
        conditions.add("has_forbidden_jobs = $job")
    }

    fun checkVariableArithmetic(which: String, cmp: Comparators, value: Any) {
        conditions.add("check_variable_arithmetic = { which = $which value $cmp $value }")
        // check_variable_arithmetic = { which = value:bpa_assigned_jobs_for_minerals value <= 1 }
    }

    fun checkVariable(name: String, cmp: Comparators = Comparators.EQ, value: Any) {
        conditions.add("check_variable = { which = $name value $cmp $value }")
    }

    fun checkVariable(name: Variables, cmp: Comparators = Comparators.EQ, value: Any) {
        checkVariable(name.key, cmp, value)
    }

    fun exists(ref: String) {
        conditions.add("exists = $ref")
    }

    fun always(flag: Boolean) {
        conditions.add("always = ${flag.toYesNo()}")
    }

}
