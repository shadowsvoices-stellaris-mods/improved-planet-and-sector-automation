package stellaris.logic

open class PlanetaryScriptedTrigger(triggerName: String) :
    RawPlanetaryConditionalBlock<PlanetConditionalBlock>(triggerName) {

    init {
        comment = "Scripted Trigger: $triggerName"
    }

    override fun newChildBlock(key: String): PlanetConditionalBlock {
        return PlanetConditionalBlock(key)
    }

}
