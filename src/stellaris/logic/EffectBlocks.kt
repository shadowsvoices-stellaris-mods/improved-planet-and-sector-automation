package stellaris.logic

import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender
import stellaris.types.Variables

abstract class EffectBlock<T : EffectBlock<T>>(blockKey: String?) : TemplateBlock(blockKey) {

    // TODO support limit = { ... } conditional blocks

    val actions: MutableList<String> = mutableListOf()

    override fun renderBeforeChildren(render: TemplateRender) {
        // actions must be rendered before children for logic like "remove modifier then re-add" to work
        if (actions.isNotEmpty()) {
            for (action in actions) {
                render.add(action)
            }
        }
    }

    abstract fun newChildBlock(key: String? = null): T

    fun ifBlock(block: T.() -> Unit): T {
        return addChild(newChildBlock("if").also(block))
    }

    fun elseIfBlock(block: T.() -> Unit): T {
        return addChild(newChildBlock("else_if").also(block))
    }

    fun elseBlock(block: T.() -> Unit): T {
        return addChild(newChildBlock("else").also(block))
    }

    fun setGlobalFlag(name: String) {
        actions.add("set_global_flag = $name")
    }

    fun removeGlobalFlag(name: String) {
        actions.add("remove_global_flag = $name")
    }

    fun countryEvent(id: String) {
        actions.add("country_event = { id = $id }")
    }

    fun setVariable(name: String, value: Int) {
        actions.add("set_variable = { which = $name value = $value }")
    }

    fun setVariable(name: Variables, value: Int) {
        setVariable(name.key, value)
    }

    fun removeModifier(name: String) {
        actions.add("remove_modifier = $name")
    }

    fun addModifier(name: String, block: (EffectBlock<T>.() -> Unit)? = null) {
        children.add(newChildBlock("add_modifier").also {
            it.actions.add("modifier = $name")
            if (block != null) it.block()
        })
    }

}

class GenericEffectBlock(key: String? = null) : EffectBlock<GenericEffectBlock>(key) {

    private var limit: EffectLimitBlock? = null
        get() {
            if (field == null) {
                field = EffectLimitBlock("limit")
            }
            return field!!
        }

    fun limit(block: EffectLimitBlock.() -> Unit): EffectLimitBlock {
        return limit!!.also(block)
    }

    override fun newChildBlock(key: String?): GenericEffectBlock {
        return GenericEffectBlock(key)
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        limit?.render(render)
        super.renderBeforeChildren(render)
    }

    fun everyCountry(block: CountryEffectBlock.() -> Unit) {
        val child = CountryEffectBlock("every_country")
        child.block()
        addChild(child)
    }

}

class CountryEffectBlock(key: String? = null) : EffectBlock<CountryEffectBlock>(key) {

    private var limit: CountryEffectLimitBlock? = null
        get() {
            if (field == null) {
                field = CountryEffectLimitBlock("limit")
            }
            return field!!
        }

    fun limit(block: CountryEffectLimitBlock.() -> Unit): CountryEffectLimitBlock {
        return limit!!.also(block)
    }

    override fun newChildBlock(key: String?): CountryEffectBlock {
        return CountryEffectBlock(key)
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        limit?.render(render)
        super.renderBeforeChildren(render)
    }

    fun everyOwnedPlanet(block: PlanetEffectBlock.() -> Unit) {
        val child = PlanetEffectBlock("every_owned_planet")
        child.block()
        addChild(child)
    }

    fun countryEvent(id: String, days: Int? = null, randomDays: Int? = null) {
        addChild(newChildBlock("country_event").also {
            it.actions.add("id = $id")
            if (days != null) it.actions.add("days = $days")
            if (randomDays != null) it.actions.add("random = $randomDays")
        })
    }

}

class PlanetEffectBlock(key: String? = null) : EffectBlock<PlanetEffectBlock>(key) {

    private var limit: PlanetEffectLimitBlock? = null
        get() {
            if (field == null) {
                field = PlanetEffectLimitBlock("limit")
            }
            return field!!
        }

    fun setPlanetFlag(flag: String) {
        actions.add("set_planet_flag = $flag")
    }

    fun limit(block: PlanetEffectLimitBlock.() -> Unit): PlanetEffectLimitBlock {
        return limit!!.also(block)
    }

    override fun newChildBlock(key: String?): PlanetEffectBlock {
        return PlanetEffectBlock(key)
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        limit?.render(render)
        super.renderBeforeChildren(render)
    }

    fun planetEvent(id: String, days: Int? = null, randomDays: Int? = null) {
        addChild(newChildBlock("planet_event").also {
            it.actions.add("id = $id")
            if (days != null) it.actions.add("days = $days")
            if (randomDays != null) it.actions.add("random = $randomDays")
        })
    }

    fun increaseBlockerFor(prefix: String, max: Int) {
            ifBlock {
                limit {
                    hasModifier(prefix + "1")
                }
                removeModifier(prefix + "1")
                addModifier(prefix + "2")
            }
            for (i in 2 until max) {
                elseIfBlock {
                    limit {
                        hasModifier(prefix + i)
                    }
                    removeModifier(prefix + i)
                    addModifier(prefix + (i + 1))
                }
            }

            // end of range cannot increase anymore
            elseIfBlock {
                limit {
                    hasModifier(prefix + max)
                }
                // NO-OP for last modifier in range, cannot increase anymore
            }

            // default action if no modifier was found: add 1
            elseBlock {
                addModifier(prefix + "1")
            }
    }

    fun decreaseBlockerFor(prefix: String, max: Int) {
            ifBlock {
                limit {
                    hasModifier(prefix + "1")
                }
                removeModifier(prefix + "1")
            }
            for (i in 2..max) {
                elseIfBlock {
                    limit {
                        hasModifier(prefix + i)
                    }
                    removeModifier(prefix + i)
                    addModifier(prefix + (i - 1))
                }
            }
    }

}
