package stellaris.logic


fun Boolean.toYesNo(): String {
    if (this) return "yes"
    else return "no"
}
