package stellaris.logic

import ipa.scriptedtriggers.IpaBuildCityForBuildings
import ipa.scriptedtriggers.IpaNeedMoreJobsCheck
import ipa.scriptedtriggers.IpaUnityAllowed
import ipa.scriptedtriggers.IpaUpgradeMinFreeSlots
import stellaris.template.TemplateRender
import stellaris.types.*
import kotlin.reflect.KMutableProperty0

abstract class RawPlanetaryConditionalBlock<T : RawPlanetaryConditionalBlock<T>>(blockKey: String?) :
    ConditionalBlock<T>(blockKey) {

    private var controller: CountryConditionalBlock? = null
    private var owner: CountryConditionalBlock? = null

    protected fun countryChildBlock(
        field: KMutableProperty0<CountryConditionalBlock?>,
        type: String,
        block: CountryConditionalBlock.() -> Unit
    ) {
        var b = field.get()
        if (b == null) {
            b = CountryConditionalBlock(type)
            b.compact = true
            b.omitBlockKey = true
            field.set(b)
        }
        b.block()
    }

    fun owner(block: CountryConditionalBlock.() -> Unit) {
        countryChildBlock(::owner, "owner", block)
    }

    fun controller(block: CountryConditionalBlock.() -> Unit) {
        countryChildBlock(::controller, "controller", block)
    }

    fun hasDesignations(group: PlanetDesignationGroup, flag: Boolean = true) {
        has(group.triggerName, Comparators.EQ, flag.toYesNo())
    }

    /**
     * IndustrialAny is a virtual designation, containing Industrial+Foundry+Factory designations.
     */
    fun hasDesignationIndustrialAny(flag: Boolean = true) {
        if (flag) {
            OR {
                hasDesignations(PlanetDesignationGroup.Industrial, true)
                hasDesignations(PlanetDesignationGroup.Factory, true)
                hasDesignations(PlanetDesignationGroup.Foundry, true)
            }

        } else {
            AND {
                hasDesignations(PlanetDesignationGroup.Industrial, false)
                hasDesignations(PlanetDesignationGroup.Factory, false)
                hasDesignations(PlanetDesignationGroup.Foundry, false)
            }
        }
    }

    fun hasDesignation(designation: String) {
        conditions.add("has_designation = $designation")
    }

    fun hasDesignation(designation: PlanetDesignation) {
        hasDesignation(designation.code)
    }

    fun freeJobs(cmp: Comparators, value: Int) {
        // TODO investigate, might be disabled/broken!
        conditions.add("free_jobs $cmp $value")
    }

    fun numUnemployed(cmp: Comparators, value: Int) {
        conditions.add("num_unemployed $cmp $value")
    }

    fun numPops(cmp: Comparators, value: Int) {
        conditions.add("num_pops $cmp $value")
    }

    fun needMoreJobsCheck(value: Boolean) {
        conditions.add("${IpaNeedMoreJobsCheck.Name} = ${value.toYesNo()}")
    }

    // TODO find better name, like noNeedForMoreJobs
    fun disableIfNoNeedForJobs() {
        needMoreJobsCheck(false)
        // conditions.add("has_unemployed_or_servants = no")
        // AND {
        //     freeJobs(Comparators.GTEQ, 0)
        //     numUnemployed(Comparators.LTEQ, 0)
        // }
    }

    fun freeAmenities(cmp: Comparators, value: Int) {
        conditions.add("free_amenities $cmp $value")
    }

    fun freeHousing(cmp: Comparators, value: Int) {
        conditions.add("free_housing $cmp $value")
    }

    fun freeDistrictSlots(cmp: Comparators, value: Int) {
        conditions.add("free_district_slots $cmp $value")
    }

    fun freeBuildingSlots(cmp: Comparators, value: Int) {
        conditions.add("free_building_slots $cmp $value")
    }

    fun upgradeMinFreeSlotsAllowed(flag: Boolean = true) {
        conditions.add("${IpaUpgradeMinFreeSlots.Name} = ${flag.toYesNo()}")
    }

    fun buildCityForBuildingSlots(flag: Boolean = true) {
        conditions.add("${IpaBuildCityForBuildings.Name} = ${flag.toYesNo()}")
    }

    fun unityAllowed(flag: Boolean = true) {
        conditions.add("${IpaUnityAllowed.Name} = ${flag.toYesNo()}")
    }

    fun planetCrime(cmp: Comparators, value: Int) {
        conditions.add("planet_crime $cmp $value")
    }

    fun numFreeDistrictsAny(cmp: Comparators, value: Int) {
        numFreeDistricts("any", cmp, value)
    }

    fun numFreeDistricts(type: String, cmp: Comparators, value: Int) {
        conditions.add("num_free_districts = { type = $type value $cmp $value }")
    }

    fun hasPlanetFlag(flag: Boolean) {
        conditions.add("has_planet_flag = ${flag.toYesNo()}")
    }

    fun numFreeDistricts(type: District, cmp: Comparators, value: Int) {
        numFreeDistricts(type.code, cmp, value)
    }

    fun numDistricts(type: String, cmp: Comparators, value: Int) {
        conditions.add("num_districts = { type = $type value $cmp $value }")
    }

    fun numDistricts(type: District, cmp: Comparators, value: Int) {
        numDistricts(type.code, cmp, value)
    }

    fun isPlanetClass(clazz: String) {
        conditions.add("is_planet_class = $clazz")
    }

    fun isPlanetClassHabitat() {
        isPlanetClass("pc_habitat")
    }

    fun isPlanetClassRingworld() {
        OR {
            isPlanetClass("pc_ringworld_habitable")
            isPlanetClass("pc_shattered_ring_habitable")
        }
    }

    fun numBuildings(type: String, cmp: Comparators, value: Int) {
        conditions.add("num_buildings = { type = $type value $cmp $value }")
    }

    fun numBuildings(type: Building, cmp: Comparators, value: Int) {
        numBuildings(type.code, cmp, value)
    }

    fun numAssignedJobs(job: String, cmp: Comparators, value: Int) {
        conditions.add("num_assigned_jobs = { job = $job value $cmp $value }")
    }

    fun anyOwnedPop(block: PopConditionalBlock.() -> Unit): PopConditionalBlock {
        val anyOwnedPop = PopConditionalBlock("any_owned_pop")
        val inner = anyOwnedPop.AND(block)
        addChild(anyOwnedPop)
        return inner
    }

    fun addResearchRequirements() {
        AND {
            comment = "non-gestalt empires require consumer goods for research"
            owner {
                isGestalt = false
                checkMissingIncome(Resource.ConsumerGoods)
            }
        }

        AND {
            comment = "gestalt hive empires require minerals for research, gestalt machines require only energy"
            owner {
                isHiveEmpire = true
                checkMissingIncome(Resource.Minerals)
            }
        }
    }

    override fun hasConditions(): Boolean {
        return super.hasConditions()
            || owner?.hasConditions() == true || owner?.children?.isNotEmpty() == true
            || controller?.hasConditions() == true || controller?.children?.isNotEmpty() == true
    }

    override fun renderConditions(render: TemplateRender) {
        super.renderConditions(render)

        if (owner?.hasConditions() == true || owner?.children?.isNotEmpty() == true) {
            render.indentedBlock("owner =") {
                // render.indentedBlock("${childBlockKey()} =") {
                owner!!.render(render)
                // }
            }
        }

        if (controller?.hasConditions() == true || controller?.children?.isNotEmpty() == true) {
            render.indentedBlock("controller =") {
                // render.indentedBlock("${childBlockKey()} =") {
                controller!!.render(render)
                // }
            }
        }
    }

}


open class PlanetConditionalBlock(blockKey: String?) : RawPlanetaryConditionalBlock<PlanetConditionalBlock>(blockKey) {
    override fun newChildBlock(key: String): PlanetConditionalBlock {
        return PlanetConditionalBlock(key)
    }
}
