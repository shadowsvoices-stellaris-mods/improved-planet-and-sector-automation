package stellaris.logic;

enum class Comparators(val symbol: String) {
    LT("<"),
    LTEQ("<="),
    EQ("="),
    GT(">"),
    GTEQ(">="),
    NEQ("!=");

    override fun toString(): String {
        return symbol
    }
}
