package stellaris.logic

import ipa.Policies
import stellaris.types.Resource
import stellaris.types.Variables

open class CountryScriptedTrigger(triggerName: String) :
    RawCountryConditionalBlock<CountryConditionalBlock>(triggerName) {

    init {
        comment = "Scripted Trigger: $triggerName"
    }

    fun addMinimumIncome(resource: Resource, variable: Variables, steps: Array<Int>) {
        // when (resource) {
        //     Resources.ConsumerGoods -> AND {
        //         usesConsumerGoods = true
        //         OR { addMinimumIncomeRaw(resource, variable, steps) }
        //     }
        //     else -> OR { addMinimumIncomeRaw(resource, variable, steps) }
        // }
        OR { addMinimumIncomeRaw(resource.code, variable, steps) }
    }

    private fun CountryConditionalBlock.addMinimumIncomeRaw(
        resource: String,
        variable: Variables,
        steps: Array<Int>
    ) {
        for (step in steps) {
            AND {
                checkVariable(variable, Comparators.EQ, step)
                hasMonthlyIncome(resource, Comparators.LTEQ, step)
            }
        }
    }

    fun addDesiredIncome(resource: Resource, variable: Variables, steps: Array<Int>) {
        // when (resource) {
        //     Resources.ConsumerGoods -> AND {
        //         usesConsumerGoods = true
        //         OR { addDesiredIncomeRaw(resource, variable, steps) }
        //     }
        //     else -> OR { addDesiredIncomeRaw(resource, variable, steps) }
        // }
        OR { addDesiredIncomeRaw(resource.code, variable, steps) }
    }

    private fun CountryConditionalBlock.addDesiredIncomeRaw(
        resource: String,
        variable: Variables,
        steps: Array<Int>
    ) {
        // always true when set to unlimited option to keep producing indefinitely
        checkVariable(variable, Comparators.GTEQ, Policies.incomeStepUnlimited)
        for (step in steps) {
            AND {
                checkVariable(variable, Comparators.EQ, step)
                hasMonthlyIncome(resource, Comparators.LTEQ, step)
            }
        }
    }

    override fun newChildBlock(key: String): CountryConditionalBlock {
        return CountryConditionalBlock(key)
    }

}
