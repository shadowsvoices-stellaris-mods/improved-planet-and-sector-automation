package stellaris.logic

import stellaris.template.TemplateRender

open class ScriptValue(blockKey: String? = null) : ConditionalBlock<ScriptValue>(blockKey) {

    val statements: MutableList<String> = mutableListOf()

    override fun render(render: TemplateRender) {
        // omit empty statement blocks
        if (children.isEmpty() && !hasStatements()) return
        super.render(render)
    }

    override fun hasConditions(): Boolean {
        return super.hasConditions() || hasStatements()
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        if (base != null) render.add("base = $base")
        if (min != null) render.add("min = $min")
        if (max != null) render.add("max = $max")
        if (hasStatements()) {
            renderStatements(render)
        }
        super.renderBeforeChildren(render)
    }

    open fun hasStatements(): Boolean {
        return statements.isNotEmpty()
    }

    open fun renderStatements(render: TemplateRender) {
        for (statement in statements) {
            render.add(statement)
        }
    }

    override fun newChildBlock(key: String): ScriptValue {
        return ScriptValue(key)
    }

    var base: Int? = null
    var min: Int? = null
    var max: Int? = null

    fun add(reference: String) {
        statements.add("add = $reference")
    }

    fun addModifier(name: String) {
        add("modifier:$name")
    }

    fun addScriptValue(name: String) {
        add("value:$name")
    }

    fun subtractScriptValue(name: String) {
        add("value:$name")
    }

    fun subtract(reference: String) {
        statements.add("subtract = $reference")
    }

    fun subtractModifier(name: String) {
        subtract("modifier:$name")
    }

    fun modifier(block: (ScriptValue.() -> Unit)) {
        addChild(newChildBlock("modifier").also(block))
    }

    // TODO improve parameters?
    fun complexTriggerModifier(trigger: String, mode: String, parameters: Map<String, Any>? = null) {
        val ctm = newChildBlock("complex_trigger_modifier")
        ctm.statements.add("trigger = $trigger")
        ctm.statements.add("mode = $mode")

        if (!parameters.isNullOrEmpty()) ctm.addChild(GenericConditionalBlock("parameters").also {
            for ((key, value) in parameters) {
                it.conditions.add("$key = $value")
            }
        })

        addChild(ctm)
    }

}

