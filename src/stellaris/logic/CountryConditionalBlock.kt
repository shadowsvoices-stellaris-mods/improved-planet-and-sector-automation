package stellaris.logic

import stellaris.template.TemplateRender
import stellaris.types.Building
import stellaris.types.Civic
import stellaris.types.Resource

abstract class RawCountryConditionalBlock<T : RawCountryConditionalBlock<T>>(blockKey: String?) :
    ConditionalBlock<T>(blockKey) {

    private var checkFlags = mutableMapOf<String, Boolean?>()

    var isAi: Boolean? by FlagsDelegate(checkFlags, "is_ai")
    var isGestalt: Boolean? by FlagsDelegate(checkFlags, "is_gestalt")
    var isLithoid: Boolean? by FlagsDelegate(checkFlags, "is_lithoid")
    var isHiveEmpire: Boolean? by FlagsDelegate(checkFlags, "is_hive_empire")
    var isMachineEmpire: Boolean? by FlagsDelegate(checkFlags, "is_machine_empire")
    var usesFood: Boolean? by FlagsDelegate(checkFlags, "ipa_country_uses_food")
    var usesConsumerGoods: Boolean? by FlagsDelegate(checkFlags, "country_uses_consumer_goods")

    private var checkMissingIncomes: MutableMap<Resource, Boolean> = mutableMapOf()
    private var checkDesiredIncomes: MutableMap<Resource, Boolean> = mutableMapOf()

    fun checkMissingIncome(resource: Resource, missing: Boolean? = true) {
        if (missing != null) {
            checkMissingIncomes[resource] = missing
        } else {
            checkMissingIncomes.remove(resource)
        }
    }

    fun checkDesiredIncome(resource: Resource, missing: Boolean? = true) {
        if (missing != null) {
            checkDesiredIncomes[resource] = missing
        } else {
            checkDesiredIncomes.remove(resource)
        }
    }

    fun hasPolicyFlag(flag: String) {
        conditions.add("has_policy_flag = $flag")
    }

    fun hasMonthlyIncome(resource: String, cmp: Comparators, value: Int) {
        conditions.add("has_monthly_income = { resource = $resource value $cmp $value }")
    }

    fun hasMonthlyIncome(resource: Resource, cmp: Comparators, value: Int) {
        hasMonthlyIncome(resource.code, cmp, value)
    }

    fun hasMonthlyIncome(resource: String, cmp: Comparators, value: String) {
        conditions.add("has_monthly_income = { resource = $resource value $cmp $value }")
    }

    fun hasMonthlyIncome(resource: Resource, cmp: Comparators, value: String) {
        hasMonthlyIncome(resource.code, cmp, value)
    }

    fun allowGrowthBuilding(building: String, flag: Boolean) {
        val trigger = when (building) {
            Building.spawning_pool.code -> "ipa_allow_growth_hive_spawning_pool"
            Building.offspring_nest.code -> "ipa_allow_growth_hive_spawning_pool"
            Building.robot_assembly_plant.code -> "ipa_allow_growth_robot_assembly"
            Building.machine_assembly_plant.code -> "ipa_allow_growth_machine_assembly"
            Building.machine_assembly_complex.code -> "ipa_allow_growth_machine_assembly"
            Building.clinic.code -> "ipa_allow_growth_clinic"
            Building.hospital.code -> "ipa_allow_growth_hospital"
            Building.clone_vats.code -> "ipa_allow_growth_clone_vats"
            Building.necrophage_elevation_chamber.code -> "ipa_allow_growth_necroid_elevation"
            Building.necrophage_house_of_apotheosis.code -> "ipa_allow_growth_necroid_elevation"
            else -> throw Exception("invalid growth building: $building")
        }

        conditions.add("$trigger = ${flag.toYesNo()}")
    }

    fun hasTechnology(technology: String) {
        conditions.add("has_technology = $technology")
    }

    fun hasEventChain(eventChain: String) {
        conditions.add("has_event_chain = $eventChain")
    }

    fun hasOrigin(origin: String) {
        conditions.add("has_origin = $origin")
    }

    fun hasAscensionPerk(ascensionPerk: String) {
        conditions.add("has_ascension_perk = $ascensionPerk")
    }

    fun hasAuthority(authority: String) {
        conditions.add("has_authority = $authority")
    }

    fun hasCountryFlag(flag: String) {
        conditions.add("has_country_flag = $flag")
    }

    fun hasValidCivic(civic: Civic) {
        conditions.add("has_valid_civic = $civic")
    }

    fun hasValidCivic(civic: String) {
        conditions.add("has_valid_civic = $civic")
    }

    fun isCountryType(type: String) {
        conditions.add("is_country_type = $type")
    }

    override fun hasConditions(): Boolean {
        return super.hasConditions()
            || checkFlags.isNotEmpty()
            || checkMissingIncomes.isNotEmpty()
            || checkDesiredIncomes.isNotEmpty()
    }

    override fun renderConditions(render: TemplateRender) {
        super.renderConditions(render)

        for ((flag, value) in checkFlags) {
            if (value != null) render.add("$flag = ${value.toYesNo()}")
        }

        for ((resource, missing) in checkMissingIncomes) {
            render.add("${resource.triggerMissingIncome} = ${missing.toYesNo()}")
        }

        for ((resource, missing) in checkDesiredIncomes) {
            render.add("${resource.triggerMissingDesiredIncome} = ${missing.toYesNo()}")
        }
    }

    fun setMissingIncomeFromResources(resources: Array<Resource>) {
        for (resource in resources) {
            checkMissingIncomes[resource] = true
        }
    }

    fun addFoodIncomeBlock() {
        AND {
            isLithoid = false
            checkMissingIncome(Resource.Food)
        }
        AND {
            isLithoid = true
            checkMissingIncome(Resource.Minerals)
        }
    }

}

open class CountryConditionalBlock(blockKey: String?) : RawCountryConditionalBlock<CountryConditionalBlock>(blockKey) {

    override fun newChildBlock(key: String): CountryConditionalBlock {
        return CountryConditionalBlock(key)
    }

}

