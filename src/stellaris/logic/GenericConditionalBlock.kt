package stellaris.logic

class GenericConditionalBlock(blockKey: String? = null) : ConditionalBlock<GenericConditionalBlock>(blockKey) {
    override fun newChildBlock(key: String): GenericConditionalBlock {
        return GenericConditionalBlock(key)
    }
}
