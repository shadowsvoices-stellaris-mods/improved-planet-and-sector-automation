package stellaris.logic

import kotlin.reflect.KProperty

class FlagsDelegate(val flags: MutableMap<String, Boolean?>, val flag: String) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): Boolean? {
        return flags[flag]
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean?) {
        flags[flag] = value
    }
}
