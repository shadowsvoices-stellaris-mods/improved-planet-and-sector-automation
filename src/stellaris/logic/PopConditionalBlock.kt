package stellaris.logic

import stellaris.types.CitizenshipType
import stellaris.types.Job

open class PopConditionalBlock(blockKey: String? = null) : RawPlanetaryConditionalBlock<PopConditionalBlock>(blockKey) {

    override fun newChildBlock(key: String): PopConditionalBlock {
        return PopConditionalBlock(key)
    }

    fun isUnemployed(value: Boolean) {
        conditions.add("is_unemployed = ${value.toYesNo()}")
    }

    fun hasCitizenshipType(type: CitizenshipType, cmp: Comparators = Comparators.EQ) {
        conditions.add("has_citizenship_type $cmp { type = $type }")
    }

    fun hasJob(job: Job) {
        conditions.add("has_job = $job")
    }

}

