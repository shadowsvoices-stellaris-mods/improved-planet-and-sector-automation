package stellaris.events

import stellaris.logic.PlanetConditionalBlock
import stellaris.logic.PlanetEffectBlock

class PlanetEvent(eventId: String, block: (PlanetEvent.() -> Unit)? = null) : AbstractEvent(eventId, "planet_event") {

    private var trigger: PlanetConditionalBlock? = null
        get() {
            if (field == null) {
                field = PlanetConditionalBlock("trigger")
                children.add(field!!)
            }
            return field!!
        }

    private var immediate: PlanetEffectBlock? = null
        get() {
            if (field == null) {
                field = PlanetEffectBlock("immediate")
                children.add(field!!)
            }
            return field!!
        }


    init {
        if (block != null) this.block()
    }

    fun immediate(block: PlanetEffectBlock.() -> Unit) {
        immediate!!.block()
    }

    fun trigger(block: PlanetConditionalBlock.() -> Unit) {
        trigger!!.block()
    }

}
