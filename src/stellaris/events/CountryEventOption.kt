package stellaris.events

import stellaris.logic.CountryConditionalBlock
import stellaris.logic.CountryEffectBlock
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

class CountryEventOption(val name: String, block: (CountryEventOption.() -> Unit)? = null) : TemplateBlock("option") {

    var customTooltip: String? = null

    private var trigger: CountryConditionalBlock? = null
        get() {
            if (field == null) {
                field = CountryConditionalBlock("trigger")
                children.add(field!!)
            }
            return field!!
        }

    private var hiddenEffect: CountryEffectBlock? = null
        get() {
            if (field == null) {
                field = CountryEffectBlock("hidden_effect")
                children.add(field!!)
            }
            return field!!
        }

    private var immediate: CountryEffectBlock? = null
        get() {
            if (field == null) {
                field = CountryEffectBlock("immediate")
                children.add(field!!)
            }
            return field!!
        }

    init {
        comment = "EventOption: $name"
        if (block != null) this.block()
    }

    fun trigger(block: CountryConditionalBlock.() -> Unit) {
        trigger!!.block()
    }

    fun hiddenEffect(block: CountryEffectBlock.() -> Unit) {
        hiddenEffect!!.block()
    }

    fun immediate(block: CountryEffectBlock.() -> Unit) {
        immediate!!.block()
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("name = $name")
        if (!customTooltip.isNullOrEmpty())
            render.add("custom_tooltip = $customTooltip")
    }

}
