package stellaris.events

import stellaris.logic.toYesNo
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

abstract class AbstractEvent(
    val eventId: String,
    eventType: String = "event",
    block: (AbstractEvent.() -> Unit)? = null
) : TemplateBlock(eventType) {

    var title: String? = null
    var desc: String? = null
    var hideWindow: Boolean = false
    var isTriggeredOnly: Boolean = true


    init {
        comment = "CountryEvent: $eventId"
        if (block != null) this.block()
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("id = $eventId")

        if (!title.isNullOrBlank()) {
            render.add("title = $title")
        }

        if (!desc.isNullOrBlank()) {
            render.add("desc = $desc")
        }

        render.add("hide_window = ${hideWindow.toYesNo()}")
        render.add("is_triggered_only = ${isTriggeredOnly.toYesNo()}")
    }

}


