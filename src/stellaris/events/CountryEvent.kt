package stellaris.events

import stellaris.types.Variables
import stellaris.logic.Comparators
import stellaris.logic.CountryConditionalBlock
import stellaris.logic.CountryEffectBlock
import stellaris.template.TemplateRender

class CountryEvent(eventId: String, block: (CountryEvent.() -> Unit)? = null) :
    AbstractEvent(eventId, "country_event") {

    private var trigger: CountryConditionalBlock? = null
        get() {
            if (field == null) {
                field = CountryConditionalBlock("trigger")
                children.add(field!!)
            }
            return field!!
        }

    private var immediate: CountryEffectBlock? = null
        get() {
            if (field == null) {
                field = CountryEffectBlock("immediate")
                children.add(field!!)
            }
            return field!!
        }

    var options: MutableList<CountryEventOption> = mutableListOf()


    init {
        if (block != null) this.block()
    }

    fun immediate(block: CountryEffectBlock.() -> Unit) {
        immediate!!.block()
    }

    fun trigger(block: CountryConditionalBlock.() -> Unit) {
        trigger!!.block()
    }

    fun option(name: String, block: (CountryEventOption.() -> Unit)? = null) {
        options.add(CountryEventOption("$eventId.$name", block))
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        super.renderBeforeChildren(render)

        if (options.isNotEmpty()) {
            render.add()
            for (option in options) {
                option.render(render)
            }
        }
    }

    fun addSelectOptionsForVariable(variable: Variables, options: Map<String, String>) {
        for ((optionId, optionValue) in options) {
            option(optionId) {
                trigger {
                    checkVariable(variable, Comparators.EQ, optionValue)
                }
                // return to parent
                hiddenEffect {
                    countryEvent(eventId)
                }
            }
        }
    }

    fun addToggleOption(variable: Variables) {
        val yesValue = 1
        val noValue = 0

        option(variable.key + "_yes") {
            trigger {
                checkVariable(variable, Comparators.EQ, yesValue)
            }
            hiddenEffect {
                setVariable(variable, noValue)
                countryEvent(eventId) // trigger self
            }
        }
        option(variable.key + "_no") {
            trigger {
                checkVariable(variable, Comparators.EQ, noValue)
            }
            hiddenEffect {
                setVariable(variable, yesValue)
                countryEvent(eventId) // trigger self
            }
        }
    }

    fun addSubmenuWithIndicators(variable: Variables, submenuEventId: String, valueToOption: Map<String, String>) {
        for ((value, option) in valueToOption) {
            option(option) {
                trigger {
                    checkVariable(variable, Comparators.EQ, value)
                }
                hiddenEffect {
                    countryEvent(submenuEventId)
                }
            }
        }
    }

}
