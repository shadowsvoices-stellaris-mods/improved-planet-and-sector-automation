package stellaris.events

import stellaris.logic.GenericConditionalBlock
import stellaris.logic.GenericEffectBlock

class GenericEvent(eventId: String, block: (GenericEvent.() -> Unit)? = null) : AbstractEvent(eventId, "event") {

    private var trigger: GenericConditionalBlock? = null
        get() {
            if (field == null) {
                field = GenericConditionalBlock("trigger")
                children.add(field!!)
            }
            return field!!
        }

    private var immediate: GenericEffectBlock? = null
        get() {
            if (field == null) {
                field = GenericEffectBlock("immediate")
                children.add(field!!)
            }
            return field!!
        }

    init {
        if (block != null) this.block()
    }

    fun immediate(block: GenericEffectBlock.() -> Unit) {
        immediate!!.block()
    }

    fun trigger(block: GenericConditionalBlock.() -> Unit) {
        trigger!!.block()
    }

}
