package stellaris.colonyautomation

import stellaris.template.TemplateBlock
import stellaris.types.Building
import stellaris.types.Job

open class ColonyAutomationException(key: String) : ColonyAutomation(key) {

    private var jobChanges: TemplateBlock? = null

    fun jobChange(job: Job, change: Int, block: (ColonyAutomationJobChange.() -> Unit)) {
        jobChange(job.symbol, change, block)
    }

    fun jobChange(job: String, change: Int, block: (ColonyAutomationJobChange.() -> Unit)) {
        if (jobChanges == null) {
            jobChanges = TemplateBlock("job_changes")
            addChild(jobChanges!!)
        }
        val childKey = if (change > 0) "${job}_increase" else "${job}_decrease"
        jobChanges!!.addChild(ColonyAutomationJobChange(job, change, childKey), block)
    }

    override fun building(building: Building, block: (ColonyAutomationBuilding.() -> Unit)) {
        if (buildings == null) {
            buildings = TemplateBlock("buildings")
            addChild(buildings!!)
        }
        buildings!!.addChild(ColonyAutomationBuilding(building), block)
    }

}
