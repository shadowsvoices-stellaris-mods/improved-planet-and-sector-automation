package stellaris.colonyautomation

import stellaris.logic.PlanetConditionalBlock
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

class ColonyAutomationJobChange(val job: String, val amount: Int, blockKey: String) : TemplateBlock(blockKey) {

    var available: PlanetConditionalBlock? = null

    fun available(block: PlanetConditionalBlock.() -> Unit) {
        if (available == null) {
            available = PlanetConditionalBlock("available")
            children.add(available!!)
        }
        available!!.block()
    }

    override fun renderAfterChildren(render: TemplateRender) {
        render.add("job = $job")
        render.add("amount = $amount")
    }

}
