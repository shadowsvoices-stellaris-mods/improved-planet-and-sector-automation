package stellaris.colonyautomation

import stellaris.logic.PlanetConditionalBlock
import stellaris.logic.toYesNo
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender
import stellaris.types.Building
import stellaris.types.District
import stellaris.types.DistrictGroup

open class ColonyAutomation(key: String) : TemplateBlock(key) {

    var category: String? = null
    var emergency: Boolean? = true

    protected val priorityDistricts: MutableList<District> = mutableListOf()
    private var available: PlanetConditionalBlock? = null
    protected var buildings: TemplateBlock? = null

    fun addPriorityDistrict(district: District) {
        priorityDistricts.add(district)
    }

    fun addPriorityDistrict(districts: DistrictGroup) {
        for (district in districts) {
            priorityDistricts.add(district)
        }
    }

    fun available(block: PlanetConditionalBlock.() -> Unit) {
        if (available == null) {
            available = PlanetConditionalBlock("available")
            children.add(available!!)
        }
        available!!.block()
    }

    open fun building(building: Building, block: (ColonyAutomationBuilding.() -> Unit)) {
        if (buildings == null) {
            buildings = TemplateBlock("buildings")
            addChild(buildings!!)
        }
        buildings!!.addChild(ColonyAutomationBuilding(building, (buildings!!.children.size + 1).toString()), block)
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        category?.let { render.add("category = $it") }
        emergency?.let { render.add("emergency = ${it.toYesNo()}") }
    }

    override fun renderAfterChildren(render: TemplateRender) {
        if (priorityDistricts.isNotEmpty()) {
            render.add("prio_districts = {")
            render.indent++
            for (district in priorityDistricts) {
                render.add(district.code)
            }
            render.indent--
            render.add("}")
        }
    }

}
