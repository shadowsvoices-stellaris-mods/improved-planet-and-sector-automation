package stellaris.colonyautomation

import stellaris.logic.PlanetConditionalBlock
import stellaris.logic.toYesNo
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender
import stellaris.types.Building

class ColonyAutomationCategory(category: String) : TemplateBlock(category) {

    private var available: PlanetConditionalBlock? = null

    fun available(block: PlanetConditionalBlock.() -> Unit) {
        if (available == null) {
            available = PlanetConditionalBlock("available")
            children.add(available!!)
        }
        available!!.block()
    }

}
