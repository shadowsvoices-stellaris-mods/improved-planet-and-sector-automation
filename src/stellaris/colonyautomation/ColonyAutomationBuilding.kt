package stellaris.colonyautomation

import stellaris.logic.PlanetConditionalBlock
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender
import stellaris.types.Building

class ColonyAutomationBuilding(val building: Building, key: String = building.code) : TemplateBlock(key) {

    private var available: PlanetConditionalBlock? = null
    private var upgradeTrigger: PlanetConditionalBlock? = null

    fun available(block: PlanetConditionalBlock.() -> Unit) {
        if (available == null) {
            available = PlanetConditionalBlock("available")
            children.add(available!!)
        }
        available!!.block()
    }

    fun upgradeTrigger(block: PlanetConditionalBlock.() -> Unit) {
        if (upgradeTrigger == null) {
            upgradeTrigger = PlanetConditionalBlock("upgrade_trigger")
            children.add(upgradeTrigger!!)
        }
        upgradeTrigger!!.block()
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("building = ${building.code}")
    }

}
