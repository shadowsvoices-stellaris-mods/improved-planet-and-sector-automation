package stellaris.types

import ipa.scriptedtriggers.IpaBuildCityForBuildings

enum class Variables(val key: String, val defaultValue: Int? = null) {
    Initialized("ipa_initialized", 1),

    IncomeEnergyMinimum("ipa_income_energy_minimum", 5),
    IncomeMineralsMinimum("ipa_income_minerals_minimum", 5),

    IncomeFoodMinimum("ipa_income_food_minimum", 1),
    IncomeFoodDesired("ipa_income_food_desired", 5),

    IncomeConsumerGoodsMinimum("ipa_income_consumer_goods_minimum", 1),
    IncomeConsumerGoodsDesired("ipa_income_consumer_goods_desired", 5),

    IncomeAlloysMinimum("ipa_income_alloys_minimum", 5),
    IncomeAlloysDesired("ipa_income_alloys_desired", 10),

    IncomeStrategicMinimum("ipa_income_strategic_minimum", 0),
    IncomeStrategicDesired("ipa_income_strategic_desired", 1),

    OptionsAllowUpgradeStronghold("ipa_options_allow_upgrade_strongholds", 1), // 0 = no, 1 = yes
    OptionsAllowAgriFarmBuildings("ipa_options_allow_agri_farm_buildings", 0), // 0 = no, 1 = yes
    OptionsForceShowIncomeOptions("ipa_options_force_show_income_options", 0), // 0 = no, 1 = yes

    OptionsAllowGrowthClinic("ipa_options_allow_growth_clinic", 0),
    OptionsAllowGrowthHospital("ipa_options_allow_growth_hospital", 0),
    OptionsAllowGrowthRobotAssembly("ipa_options_allow_growth_robot_assembly", 0),
    OptionsAllowGrowthCloneVats("ipa_options_allow_growth_clone_vats", 0),
    OptionsAllowGrowthMachineAssembly("ipa_options_allow_growth_machine_assembly", 0),
    OptionsAllowGrowthHiveSpawningPool("ipa_options_allow_growth_hive_spawning_pool", 0),
    OptionsAllowGrowthNecroidElevation("ipa_options_allow_growth_necroid_elevation", 0),

    OptionsUpgradeMinFreeSlots("ipa_options_upgrade_min_free_slots", 0), // num
    OptionsDisallowUnity("ipa_options_disallow_unity", 0), // 0 = enabled, 1 = disabled
    OptionsPreBuildJobs("ipa_options_pre_build_jobs", 1), // num
    OptionsBuildCityForBuildings("ipa_options_build_city_for_buildings", IpaBuildCityForBuildings.Options.ResearchOnly.value),

    OptionsManageJobAmenities("ipa_options_manage_job_amenities", 0),
    OptionsManageJobCrime("ipa_options_manage_job_crime", 0),
    OptionsManageJobBioTrophy("ipa_options_manage_job_bio_trophy", 0),

    ColonyAutomationPriority("ipa_colony_automation_priority", 1),
}
