package stellaris.types

enum class Job(val symbol: String, val modifierAdd: String = "job_${symbol}_add") {

    MaintenanceDrone("maintenance_drone"),
    PatrolDrone("patrol_drone"),
    BioTrophy("bio_trophy"),
    Enforcer("enforcer"),
    Clerk("clerk"),
    ;

    override fun toString(): String {
        return symbol
    }
}
