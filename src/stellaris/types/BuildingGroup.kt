package stellaris.types

enum class BuildingGroup(vararg val buildings: Building) : Iterable<Building> {
    CrimeT1(
        Building.precinct_house,
        Building.sentinel_posts
    ),
    HousingBuildingsT1(
        Building.luxury_residence,
        Building.hive_warren,
        Building.drone_storage,
    ),
    HousingBuildingsT2(
        Building.paradise_dome,
        Building.expanded_warren,
        Building.drone_megastorage,
    ),

    AmenitiesBuildingsT1(
        Building.holo_theatres,
    ),
    AmenitiesBuildingsT2(
        Building.hyper_entertainment_forum,
    ),

    CapitalBuildings(
        Building.colony_shelter,
        Building.capital,
        Building.major_capital,
        Building.system_capital,
        Building.deployment_post,
        Building.machine_capital,
        Building.machine_major_capital,
        Building.machine_system_capital,
        Building.hive_capital,
        Building.hive_major_capital,
        Building.hab_capital,
        Building.hab_major_capital,
        Building.resort_capital,
        Building.slave_capital,
        Building.slave_major_capital,
    ),

    UnityJobsT1(
        Building.bureaucratic_1,
        Building.temple,
        Building.uplink_node,
        Building.hive_node,
    ),
    UnityJobsT2(
        Building.bureaucratic_2,
        Building.holotemple,
        Building.network_junction,
        Building.hive_cluster,
    ),
    UnityJobsT3(
        Building.bureaucratic_3,
        Building.sacred_nexus,
        Building.system_conflux,
        Building.hive_confluence,
    ),

    UnityJobsBooster(
        Building.alpha_hub,
        Building.autocurating_vault,
        Building.citadel_of_faith,
        Building.corporate_vault,
    ),

    UnityMemorialsT1(
        Building.autochthon_monument,
        Building.simulation_1,
        Building.sensorium_1,
    ),
    UnityMemorialsT2(
        Building.heritage_site,
        Building.simulation_2,
        Building.sensorium_2,
    ),
    UnityMemorialsT3(
        Building.hypercomms_forum,
        Building.simulation_3,
        Building.sensorium_3,
    )
    ;


    override fun iterator(): Iterator<Building> {
        return buildings.iterator()
    }

}
