package stellaris.types

enum class Civic(val symbol: String) {

    MachineServitor("civic_machine_servitor"),
    ;

    override fun toString(): String {
        return symbol
    }

}
