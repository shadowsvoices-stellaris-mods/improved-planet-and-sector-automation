package stellaris.types

enum class CitizenshipType(val symbol: String) {

    OrganicTrophy("citizenship_organic_trophy");

    override fun toString(): String {
        return symbol
    }

}
