package stellaris.types

enum class BuildingJobTypes {
    Leader,
    Specialist, // counts as complex for gestalt
    Worker // counts as worker for gestalt
}
