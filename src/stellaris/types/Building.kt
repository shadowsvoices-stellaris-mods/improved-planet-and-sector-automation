package stellaris.types

/**
 * Definition of buildings.
 *
 * NOTICE upkeepResources is not universally implemented for all buildings (yet?) and has to be set for a `CountryConditionalBlock` via `setMissingIncomeFromResources`
 */
@Suppress("EnumEntryName")
enum class Building(
    val code: String,
    val upkeepResources: Array<Resource> = arrayOf(),
    val jobType: BuildingJobTypes? = null,
) {
    // growth buildings
    spawning_pool("building_spawning_pool"),
    offspring_nest("building_offspring_nest"),
    robot_assembly_plant("building_robot_assembly_plant"),
    machine_assembly_plant("building_machine_assembly_plant"),
    machine_assembly_complex("building_machine_assembly_complex"),
    clinic("building_clinic"),
    hospital("building_hospital"),
    clone_vats("building_clone_vats"),
    necrophage_elevation_chamber("building_necrophage_elevation_chamber"),
    necrophage_house_of_apotheosis("building_necrophage_house_of_apotheosis"),

    // === unity ===

    // unity jobs for regular organics
    bureaucratic_1("building_bureaucratic_1", arrayOf(Resource.Energy, Resource.ConsumerGoods)),
    bureaucratic_2("building_bureaucratic_2", arrayOf(Resource.Energy, Resource.ConsumerGoods, Resource.RareCrystals)),
    bureaucratic_3("building_bureaucratic_3", arrayOf(Resource.Energy, Resource.ConsumerGoods, Resource.RareCrystals)),

    // unity jobs for spiritualist
    temple("building_temple", arrayOf(Resource.Energy, Resource.ConsumerGoods)),
    holotemple("building_holotemple", arrayOf(Resource.Energy, Resource.ConsumerGoods, Resource.RareCrystals)),
    sacred_nexus("building_sacred_nexus", arrayOf(Resource.Energy, Resource.ConsumerGoods, Resource.RareCrystals)),

    // unity jobs for gestalt: machine
    uplink_node("building_uplink_node", arrayOf(Resource.Energy)),
    network_junction("building_network_junction", arrayOf(Resource.Energy, Resource.RareCrystals)),
    system_conflux("building_system_conflux", arrayOf(Resource.Energy, Resource.RareCrystals)),

    // unity jobs for gestalt: hive
    hive_node("building_hive_node", arrayOf(Resource.Energy, Resource.Food)),
    hive_cluster("building_hive_cluster", arrayOf(Resource.Energy, Resource.Food, Resource.RareCrystals)),
    hive_confluence("building_hive_confluence", arrayOf(Resource.Energy, Resource.Food, Resource.RareCrystals)),

    // === unity memorials (unity from ascension perks) ===

    // unity memorials for regular organics (also applies to spiritualists)
    autochthon_monument("building_autochthon_monument"),
    heritage_site("building_heritage_site"),
    hypercomms_forum("building_hypercomms_forum"),

    // unity memorials for gestalt: machine
    simulation_1("building_simulation_1"),
    simulation_2("building_simulation_2"),
    simulation_3("building_simulation_3"),

    // unity memorials for gestalt: hive
    sensorium_1("building_sensorium_1"),
    sensorium_2("building_sensorium_2"),
    sensorium_3("building_sensorium_3"),

    // unity boosters (for job based unity)
    alpha_hub("building_alpha_hub"),
    autocurating_vault("building_autocurating_vault"),
    citadel_of_faith("building_citadel_of_faith"),
    corporate_vault("corporate_vault"),

    // crime buildings
    precinct_house("building_precinct_house"),
    sentinel_posts("building_sentinel_posts"),

    // foundry (alloy production)
    foundry_1("building_foundry_1"),
    foundry_2("building_foundry_2"),
    foundry_3("building_foundry_3"),

    // factory (consumer goods production)
    factory_1("building_factory_1"),
    factory_2("building_factory_2"),
    factory_3("building_factory_3"),

    // Research
    supercomputer("building_supercomputer"),
    institute("building_institute"),
    research_lab_1("building_research_lab_1"),
    research_lab_2("building_research_lab_2"),
    research_lab_3("building_research_lab_3"),

    // mod: plentiful traditions
    plentiful_traditions_university_of_wisdom("building_plentiful_traditions_university_of_wisdom"),
    plentiful_traditions_cybernetics_facility("building_plentiful_traditions_cybernetics_facility"),
    plentiful_traditions_experimental_weapons_facility("building_plentiful_traditions_experimental_weapons_facility"),
    plentiful_traditions_biogenesis_lab("building_plentiful_traditions_biogenesis_lab"),

    // mod: ancient cache of technologies
    dark_matter_synthersizer("building_dark_matter_synthersizer", arrayOf(Resource.Energy)), // Dimensional Harvester
    ae_dark_matter_synthersizer("building_ae_dark_matter_synthersizer", arrayOf(Resource.Energy)), // Dimensional Nexus
    fe_machine_dome(
        "building_fe_machine_dome",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy)
    ), // Personality Uplink
    ae_machine_dome("building_ae_machine_dome", arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy)), // Infinity Core
    fe_institute(
        "building_fe_institute",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy)
    ), // Delta Science Complex
    ae_institute(
        "building_ae_institute",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy)
    ), // Alpha Master Research Complex
    super_assembly_machine(
        "building_super_assembly_machine",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy, Resource.Energy)
    ), // Hard-Light Machine Assembly
    ae_super_assembly_machine(
        "building_ae_super_assembly_machine",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy, Resource.Energy)
    ), // Alpha Machine Assembly
    super_assembly(
        "building_super_assembly",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy, Resource.Energy)
    ), // Delta Machine Assembly
    ae_super_assembly(
        "building_ae_super_assembly",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy, Resource.Energy)
    ), // Alpha Machine Assembly
    ae_ancient_control_center("building_ae_ancient_control_center"), // Void Nexus
    stellarite_generator(
        "building_stellarite_generator",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy, Resource.Energy)
    ), // Stellarite Generator
    stellarite_generator_ae(
        "building_stellarite_generator_ae",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy, Resource.Energy)
    ), // Advanced Stellarite Generator
    stellarite_nexus(
        "building_stellarite_nexus",
        arrayOf(Resource.DarkMatter, Resource.AcotDarkEnergy, Resource.Energy)
    ), // Stellarite Energy Conduit

    // mod: gigastructural engineering
    giga_institute_1("building_giga_institute_1", arrayOf(Resource.ExoticGases)),
    giga_institute_2("building_giga_institute_2", arrayOf(Resource.ExoticGases)),
    giga_supercomputer_1("building_giga_supercomputer_1", arrayOf(Resource.ExoticGases)),
    giga_supercomputer_2("building_giga_supercomputer_2", arrayOf(Resource.ExoticGases)),
    giga_shroud_capacitor("building_giga_shroud_capacitor"), // Shroud Capacitor

    // Housing T1
    luxury_residence("building_luxury_residence"),
    hive_warren("building_hive_warren"),
    drone_storage("building_drone_storage"),

    // Housing T2
    paradise_dome("building_paradise_dome"),
    expanded_warren("building_expanded_warren"),
    drone_megastorage("building_drone_megastorage"),

    // amenities T1
    holo_theatres("building_holo_theatres"),

    // amenities T2
    hyper_entertainment_forum("building_hyper_entertainment_forum"),

    // Capitals
    colony_shelter("building_colony_shelter"),
    capital("building_capital"),
    major_capital("building_major_capital"),
    system_capital("building_system_capital"),
    deployment_post("building_deployment_post"),
    machine_capital("building_machine_capital"),
    machine_major_capital("building_machine_major_capital"),
    machine_system_capital("building_machine_system_capital"),
    hive_capital("building_hive_capital"),
    hive_major_capital("building_hive_major_capital"),
    hab_capital("building_hab_capital"),
    hab_major_capital("building_hab_major_capital"),
    resort_capital("building_resort_capital"),
    slave_capital("building_slave_capital"),
    slave_major_capital("building_slave_major_capital"),

    // production enhancers
    mineral_purification_plant("building_mineral_purification_plant"),
    mineral_purification_hub("building_mineral_purification_hub"),
    energy_grid("building_energy_grid"),
    energy_nexus("building_energy_nexus"),
    food_processing_facility("building_food_processing_facility"),
    food_processing_center("building_food_processing_center"),
    ministry_production("building_ministry_production"),
    production_center("building_production_center"),
    psi_corps("building_psi_corps"),

    // strategic
    crystal_mines("building_crystal_mines"),
    gas_extractors("building_gas_extractors"),
    mote_harvesters("building_mote_harvesters"),
    chemical_plant("building_chemical_plant"),
    refinery("building_refinery"),
    crystal_plant("building_crystal_plant"),

    // embassy
    embassy("building_embassy", arrayOf(Resource.Energy, Resource.RareCrystals)),
    grand_embassy("building_grand_embassy", arrayOf(Resource.Energy, Resource.RareCrystals)),

    // military
    stronghold("building_stronghold"),
    fortress("building_fortress"),

    // trade
    // TODO support trade
    commercial_zone("building_commercial_zone"),
    commercial_megaplex("building_commercial_megaplex"),

    // misc
    hydroponics_farm("building_hydroponics_farm"),
    betharian_power_plant("building_betharian_power_plant"),

    // machine servitors
    organic_sanctuary("building_organic_sanctuary", arrayOf(Resource.Energy)),
    organic_paradise("building_organic_paradise", arrayOf(Resource.Energy, Resource.RareCrystals)),

    maintenance_depot("maintenance_depot")
}
