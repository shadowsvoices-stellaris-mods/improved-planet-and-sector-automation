package stellaris.types

@Suppress("EnumEntryName")
enum class District(val code: String) {
    City("district_city"),
    Hive("district_hive"),
    Nexus("district_nexus"),

    Mining("district_mining"),
    MiningUncapped("district_mining_uncapped"),
    Generator("district_generator"),
    GeneratorUncapped("district_generator_uncapped"),
    Farming("district_farming"),
    FarmingUncapped("district_farming_uncapped"),
    Industrial("district_industrial"),

    ArcologyHousing("district_arcology_housing"),
    ArcologyOrganicHousing("district_organic_housing"),
    ArcologyArmsIndustry("district_arcology_arms_industry"),         // TODO support, add to groups
    ArcologyCivilianIndustry("district_arcology_civilian_industry"), // TODO support, add to groups
    ArcologyLeisure("district_arcology_leisure"),
    ArcologyAdministrative("district_arcology_administrative"),

    HabitatHousing("district_hab_housing"),
    HabitatEnergy("district_hab_energy"),
    HabitatIndustrial("district_hab_industrial"),
    HabitatResearch("district_hab_science"),
    HabitatCommercial("district_hab_commercial"),
    HabitatMining("district_hab_mining"),
    HabitatCultural("district_hab_cultural"),

    SrwCommercial("district_srw_commercial"), // TODO support, add to groups

    RingworldCity("district_rw_city"),
    RingworldHive("district_rw_hive"),
    RingworldNexus("district_rw_nexus"),
    RingworldGenerator("district_rw_generator"),
    RingworldCommercial("district_rw_commercial"),
    RingworldResearch("district_rw_science"),
    RingworldFarming("district_rw_farming"),
    RingworldIndustrial("district_rw_industrial"),

    // mod: gigastructural engineering
    giga_planet_science("district_giga_planet_science"), // Research Laboratory on Gas Giants and Planetary Computers
}
