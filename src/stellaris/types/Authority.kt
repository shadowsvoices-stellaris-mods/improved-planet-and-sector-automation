package stellaris.types

enum class Authority(val symbol: String) {

    MachineIntelligence("auth_machine_intelligence"),
    HiveMind("auth_hive_mind"),
    Corporate("auth_corporate"),
    Dictatorial("auth_dictatorial"),
    Imperial("auth_imperial"),
    Oligarchic("auth_oligarchic"),
    Democratic("auth_democratic"),
    AncientMachineIntelligence("auth_ancient_machine_intelligence");

    override fun toString(): String {
        return symbol
    }

}
