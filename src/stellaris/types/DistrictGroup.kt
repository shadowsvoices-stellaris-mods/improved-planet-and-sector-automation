package stellaris.types

enum class DistrictGroup(vararg val districts: District) : Iterable<District> {

    Housing(
        District.City,
        District.HabitatHousing,
        District.Hive,
        District.Nexus,
        District.RingworldCity,
        District.RingworldHive,
        District.RingworldNexus,
        District.ArcologyHousing,
        District.ArcologyOrganicHousing
    ),
    Mining(
        District.Mining,
        District.HabitatMining,
        District.MiningUncapped
    ),
    Generator(
        District.Generator,
        District.RingworldGenerator,
        District.HabitatEnergy,
        District.GeneratorUncapped
    ),
    Farming(
        District.Farming,
        District.RingworldFarming,
        District.FarmingUncapped,
    ),
    Research(
        District.RingworldResearch,
        District.HabitatResearch,
        District.giga_planet_science,
    ),
    Leisure(
        District.HabitatCultural,
        District.ArcologyLeisure,
    ),
    Trade(
        District.RingworldCommercial,
        District.HabitatCommercial,
        District.SrwCommercial,
    ),
    Industrial(
        District.Industrial,
        District.HabitatIndustrial,
        District.RingworldIndustrial,
        District.ArcologyCivilianIndustry,
        District.ArcologyArmsIndustry,
    );

    override fun iterator(): Iterator<District> {
        return districts.iterator()
    }

}
