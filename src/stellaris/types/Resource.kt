package stellaris.types

import ipa.Policies

enum class ResourceType {
    Basic,
    Strategic
}

enum class Resource(
    val code: String,
    val type: ResourceType,
    val varIncomeMinimum: Variables? = null,
    val varIncomeDesired: Variables? = null
) {

    Minerals(
        "minerals",
        ResourceType.Basic,
        varIncomeMinimum = Variables.IncomeMineralsMinimum
    ),
    Energy(
        "energy",
        ResourceType.Basic,
        varIncomeMinimum = Variables.IncomeEnergyMinimum
    ),
    Food(
        "food",
        ResourceType.Basic,
        varIncomeMinimum = Variables.IncomeFoodMinimum,
        varIncomeDesired = Variables.IncomeFoodDesired
    ),

    Alloys(
        "alloys",
        ResourceType.Basic,
        varIncomeMinimum = Variables.IncomeAlloysMinimum,
        varIncomeDesired = Variables.IncomeAlloysDesired
    ),
    ConsumerGoods(
        "consumer_goods",
        ResourceType.Basic,
        varIncomeMinimum = Variables.IncomeConsumerGoodsMinimum,
        varIncomeDesired = Variables.IncomeConsumerGoodsDesired
    ),

    RareCrystals(
        "rare_crystals",
        ResourceType.Strategic,
        varIncomeMinimum = Variables.IncomeStrategicMinimum,
        varIncomeDesired = Variables.IncomeStrategicDesired
    ),
    ExoticGases(
        "exotic_gases",
        ResourceType.Strategic,
        varIncomeMinimum = Variables.IncomeStrategicMinimum,
        varIncomeDesired = Variables.IncomeStrategicDesired
    ),
    VolatileMotes(
        "volatile_motes",
        ResourceType.Strategic,
        varIncomeMinimum = Variables.IncomeStrategicMinimum,
        varIncomeDesired = Variables.IncomeStrategicDesired
    ),
    DarkMatter(
        "sr_dark_matter",
        ResourceType.Strategic,
        varIncomeMinimum = Variables.IncomeStrategicMinimum,
        varIncomeDesired = Variables.IncomeStrategicDesired
    ),

    AcotDarkEnergy(
        "acot_sr_dark_energy",
        ResourceType.Strategic,
        varIncomeMinimum = Variables.IncomeStrategicMinimum,
        varIncomeDesired = Variables.IncomeStrategicDesired
    ),
    AcotStellarite(
        "acot_sr_stellarite",
        ResourceType.Strategic,
        varIncomeMinimum = Variables.IncomeStrategicMinimum,
        varIncomeDesired = Variables.IncomeStrategicDesired
    );

    val triggerMissingIncome: String
        get() {
            requireNotNull(varIncomeMinimum) { "Resource $this does not support 'ipa_missing_income_$code' check" }
            return "ipa_missing_income_$code"
        }

    val triggerMissingDesiredIncome: String
        get() {
            requireNotNull(varIncomeDesired) { "Resource $this does not support 'ipa_missing_desired_income_$code' check" }
            return "ipa_missing_desired_income_$code"
        }

    val incomeSteps: Array<Int>
        get() {
            return when (type) {
                ResourceType.Basic -> Policies.incomeSteps
                ResourceType.Strategic -> Policies.incomeStepsStrategic
            }
        }

}
