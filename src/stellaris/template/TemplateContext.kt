package stellaris.template

class TemplateContext(val modPath: String, val docPath: String, cb: (TemplateContext.() -> Unit)? = null) {

    val files: MutableList<AbstractFile<TemplateRender>> = mutableListOf()

    init {
        if (cb != null) {
            this.cb();
        }
    }

    fun <T : AbstractFile<TemplateRender>> addFile(file: T): T {
        files.add(file)
        return file
    }

    fun writeFiles() {
        for (file in files) {
            println("writing '${file.target}'")
            file.writeFile(modPath)
        }
    }

}
