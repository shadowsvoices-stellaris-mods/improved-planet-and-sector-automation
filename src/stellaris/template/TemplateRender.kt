package stellaris.template

open class TemplateRender(val indentSequence: String = "\t") {

    val lines: StringBuilder = StringBuilder()

    var indent = 0

    open fun add(content: String = "") {
        if (!content.isBlank()) {
            lines.appendLine(indentSequence.repeat(indent) + content)
        } else {
            lines.appendLine()
        }
    }

    fun content(): String {
        return lines.toString()
    }

    open fun indented(block: TemplateRender.() -> Unit) {
        indent++
        block()
        indent--
    }

    open fun indentedBlock(blockName: String, block: TemplateRender.() -> Unit) {
        add("$blockName {")
        indent++
        block()
        indent--
        add("}")
    }

}
