package stellaris.template

open class TranslationFile(target: String, val language: String) :
    AbstractFile<TemplateRender>(target) {

    val translations: MutableMap<String, String> = mutableMapOf()

    init {
        addBOM = true
    }

    fun add(key: String, value: String) {
        translations[key] = value
    }

    override fun newTemplateRender(): TemplateRender {
        return TemplateRender("    ")
    }

    override fun writeContent(render: TemplateRender) {
        render.add("$language:")
        render.indent++

        for ((key, value) in translations) {
            render.add("$key: \"$value\"")
        }

        render.indent--
    }

}
