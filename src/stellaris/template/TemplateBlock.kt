package stellaris.template

open class TemplateBlock(val blockKey: String?) {

    var comment: String? = null

    val children: MutableList<TemplateBlock> = mutableListOf()

    var compact: Boolean = false

    var omitBlockKey: Boolean = false

    open fun render(render: TemplateRender) {
        if (!comment.isNullOrBlank())
            render.add("# $comment")

        if (!blockKey.isNullOrBlank() && !omitBlockKey) {
            render.add("$blockKey = {")
            render.indent++
        }

        renderBeforeChildren(render)

        if (children.isNotEmpty()) {
            if (!compact) render.add()
            for (child in children) {
                child.render(render)
            }
        }

        renderAfterChildren(render)

        if (!blockKey.isNullOrBlank() && !omitBlockKey) {
            render.indent--
            render.add("}")
        }

        if (!compact) render.add()
    }

    protected open fun renderBeforeChildren(render: TemplateRender) {

    }

    protected open fun renderAfterChildren(render: TemplateRender) {

    }

    fun <T : TemplateBlock> addChild(child: T, cb: (T.() -> Unit)? = null): T {
        if (cb != null) child.cb()
        children.add(child)
        return child
    }

}
