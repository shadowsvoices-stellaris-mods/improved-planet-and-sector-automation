package stellaris.sector

import stellaris.logic.toYesNo
import stellaris.template.TemplateRender

class SectorBuilding(constructKey: String) :
    SectorConstruct("building", constructKey) {

    var priority: Boolean? = null

    override fun renderBeforeChildren(render: TemplateRender) {
        super.renderBeforeChildren(render)
        priority?.let { render.add("priority = ${it.toYesNo()}") }
    }

}
