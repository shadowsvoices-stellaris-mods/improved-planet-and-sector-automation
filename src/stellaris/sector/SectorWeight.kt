package stellaris.sector

import stellaris.logic.PlanetConditionalBlock
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

class SectorWeight(val sectorConstruct: SectorConstruct) : TemplateBlock("weight") {

    var baseWeight: Double = 1.0

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("weight = $baseWeight")
    }

    private fun modifierConditional(
        factor: Double,
        combinatorCondition: String = "OR",
        block: (PlanetConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        val modifier = SectorWeightModifier(factor)
        val conditionalBlock = modifier.childBlock(combinatorCondition, block)

        // move immediate conditional comment up to modifier block
        modifier.comment = conditionalBlock.comment
        conditionalBlock.comment = null

        children.add(modifier)
        return modifier
    }

    fun modifierOR(
        factor: Double,
        block: (PlanetConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        return modifierConditional(factor, "OR", block)
    }

    fun modifierAND(
        factor: Double,
        block: (PlanetConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        // modifier is AND by default
        // return modifierConditional(factor, "AND", block)
        val modifier = SectorWeightModifier(factor)
        modifier.block()
        children.add(modifier)
        return modifier
    }

    fun modifierNOT(
        factor: Double,
        block: (PlanetConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        return modifierConditional(factor, "NOT", block)
    }

    fun modifierNOR(
        factor: Double,
        block: (PlanetConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        return modifierConditional(factor, "NOR", block)
    }

    fun modifierNAND(
        factor: Double,
        block: (PlanetConditionalBlock.() -> Unit)
    ): SectorWeightModifier {
        return modifierConditional(factor, "NAND", block)
    }

}
