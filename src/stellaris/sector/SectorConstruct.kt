package stellaris.sector

import stellaris.logic.toYesNo
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

abstract class SectorConstruct(val constructType: String, val constructKey: String) : TemplateBlock(constructType) {

    val weight = SectorWeight(this)
    var exemptFromJobsCheck: Boolean? = true

    init {
        comment = "$constructType: $constructKey"
        children.add(weight)
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("key = $constructKey")
        exemptFromJobsCheck?.let { render.add("exempt_from_jobs_check = ${it.toYesNo()}") }
    }

    fun weight(weight: Double, block: (SectorWeight.() -> Unit)? = null) {
        this.weight.baseWeight = weight
        if (block != null) {
            this.weight.block()
        }
    }

}
