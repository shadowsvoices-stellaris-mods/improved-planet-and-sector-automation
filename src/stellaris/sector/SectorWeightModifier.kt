package stellaris.sector

import stellaris.logic.PlanetConditionalBlock
import stellaris.template.TemplateRender

class SectorWeightModifier(val factor: Double) : PlanetConditionalBlock("modifier") {

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add("factor = ${"%.2f".format(factor)}")
        super.renderBeforeChildren(render)
    }

}
