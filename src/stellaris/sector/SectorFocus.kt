package stellaris.sector

import stellaris.types.Building
import stellaris.types.District
import stellaris.logic.toYesNo
import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

open class SectorFocus(val focusName: String, block: (SectorFocus.() -> Unit)? = null) : TemplateBlock(focusName) {

    var aiWeight: Int? = 0
    var clearBlockers = true
    var hidden: Boolean? = null

    var experimental: Boolean = false

    val priorities: SectorPriorities = SectorPriorities()

    init {
        comment = "Sector focus: $focusName"

        if (block != null) this.block()
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        render.add()
        if (aiWeight != null) {
            render.indentedBlock("ai_weight =") {
                render.add("weight = $aiWeight")
            }
        }

        render.add()
        render.add("clear_blockers = ${clearBlockers.toYesNo()}")
        hidden?.let {
            render.add("hidden = ${it.toYesNo()}")
        }
    }

    fun addBuilding(
        key: Building,
        weight: Double,
        priority: Boolean? = null,
        exemptFromJobsCheck: Boolean? = true,
        block: (SectorWeight.() -> Unit)? = null
    ): SectorBuilding {
        val building = SectorBuilding(key.code)
        building.weight(weight, block)
        priority?.let { building.priority = it }
        exemptFromJobsCheck?.let { building.exemptFromJobsCheck = it }
        children.add(building)
        return building
    }

    fun addDistrict(
        key: District,
        weight: Double,
        exemptFromJobsCheck: Boolean? = true,
        block: (SectorWeight.() -> Unit)? = null
    ): SectorDistrict {
        val district = SectorDistrict(key.code)
        district.weight(weight, block)
        exemptFromJobsCheck?.let { district.exemptFromJobsCheck = it }
        children.add(district)
        return district
    }

}
