package stellaris.various

import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

open class DefinesBlock(blockKey: String) : TemplateBlock(blockKey) {

    data class DefineItem(val key: String, val value: Any, val comment: String? = null)

    protected val defines: MutableMap<String, DefineItem> = mutableMapOf()

    init {
        comment = "Defines Block: $blockKey"
    }

    override fun renderBeforeChildren(render: TemplateRender) {
        super.renderBeforeChildren(render)

        for ((_, item) in defines) {
            if (!item.comment.isNullOrBlank()) render.add("# ${item.comment}")


            render.add("${item.key} = ${prepareValue(item.value)}")
        }
    }

    protected fun prepareValue(value: Any): String {
        if (value is String) {
            return "\"$value\""
        }
        if (value is Array<*>) {
            return value.joinToString(separator = " ", prefix = "{ ", postfix = " }") {
                if (it == null) ""
                else prepareValue(it)
            }
        }
        return value.toString()
    }

    fun addDefine(key: String, value: Any, comment: String? = null) {
        defines[key] = DefineItem(key, value, comment)
    }

}
