package stellaris.various

import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

class EmpirePolicy(blockKey: String) : TemplateBlock(blockKey) {

    class PolicyOption(val name: String, val flags: Array<String>)

    var notGestalt = false

    var options: MutableList<PolicyOption> = mutableListOf()

    override fun renderBeforeChildren(render: TemplateRender) {

        render.add()
        render.indentedBlock("potential =") {
            render.add("always = yes")
            if (notGestalt) render.add("NOT = { has_ethic = \"ethic_gestalt_consciousness\" }")
            // render.add("ia_ai = ${toYesNo(allowAi)}")
        }

        render.add()
        render.indentedBlock("allow =") {
            render.add("always = yes")
            if (notGestalt) render.add("NOT = { has_ethic = \"ethic_gestalt_consciousness\" }")
            // render.add("ia_ai = ${toYesNo(allowAi)}")
        }

        for (option in options) {
            render.add()
            render.indentedBlock("option =") {
                render.add("name = \"${option.name}\"")

                render.add()
                render.indentedBlock("policy_flags =") {
                    for (flag in option.flags) {
                        render.add(flag)
                    }
                }

                render.add()
                render.indentedBlock("valid =") {
                    render.add("always = yes")
                }

            }
        }
    }

    fun addOption(name: String, flags: Array<String>) {
        options.add(PolicyOption(name, flags))
    }
}
