package stellaris.various

import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

open class OnActionBlock(onActionName: String) : TemplateBlock(onActionName) {

    protected val events: MutableList<String> = mutableListOf()

    override fun renderBeforeChildren(render: TemplateRender) {
        super.renderBeforeChildren(render)

        if (events.size > 0) {
            render.indentedBlock("events =") {
                for (event in events) {
                    render.add(event)
                }
            }
        }
    }

    fun event(name: String) {
        events.add(name)
    }

}
