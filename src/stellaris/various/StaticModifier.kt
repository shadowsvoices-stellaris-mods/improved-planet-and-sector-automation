package stellaris.various

import stellaris.template.TemplateBlock
import stellaris.template.TemplateRender

open class StaticModifier(blockKey: String) : TemplateBlock(blockKey) {

    val effects: MutableList<String> = mutableListOf()

    var icon: String? = null
    var iconFrame: Int? = null
    var customTooltip: String? = null

    override fun renderBeforeChildren(render: TemplateRender) {
        super.renderBeforeChildren(render)

        if (icon != null) render.add("icon = \"$icon\"")
        if (iconFrame != null) render.add("icon_frame = $iconFrame")
        if (customTooltip != null) render.add("custom_tooltip = $customTooltip")

        if (hasEffects()) {
            renderEffects(render)
        }
    }

    open fun hasEffects(): Boolean {
        return effects.isNotEmpty()
    }

    open fun renderEffects(render: TemplateRender) {
        for (statement in effects) {
            render.add(statement)
        }
    }

    open fun addEffect(name: String, value: String) {
        effects.add("$name = $value")
    }

}

