package ipa.scriptValues

import ipa.staticModifiers.*
import stellaris.logic.ScriptValue
import stellaris.template.TemplateFile
import stellaris.types.Job

class JobCounts : TemplateFile("common/script_values/ipa_job_counts.txt") {

    companion object {
        val MaintenanceDrone =
            BaseJobCount(Job.MaintenanceDrone, SuppressJobMaintenanceDrone.Prefix, SuppressJobMaintenanceDrone.Max)
        val PatrolDrone = BaseJobCount(Job.PatrolDrone, SuppressJobPatrolDrone.Prefix, SuppressJobPatrolDrone.Max)
        val BioTrophy = BaseJobCount(Job.BioTrophy, SuppressJobBioTrophy.Prefix, SuppressJobBioTrophy.Max)
        val Enforcer = BaseJobCount(Job.Enforcer, SuppressJobEnforcer.Prefix, SuppressJobEnforcer.Max)
        val Clerk = BaseJobCount(Job.Clerk, SuppressJobClerk.Prefix, SuppressJobClerk.Max)
    }

    init {
        for (job in arrayOf(
            MaintenanceDrone,
            PatrolDrone,
            BioTrophy,
            Enforcer,
            Clerk
        )) {
            job.addScriptValueBlocksToTemplate(this)
        }
    }

    class BaseJobCount(
        private val job: Job,
        private val suppressedPrefix: String,
        private val suppressedMax: Int
    ) {

        val Free = "ipa_job_${job}_free"
        val Used = "ipa_job_${job}_used"
        val Suppressed = "ipa_job_${job}_suppressed"
        val Total = "ipa_job_${job}_total"
        val Available = "ipa_job_${job}_available"

        fun addScriptValueBlocksToTemplate(templateFile: TemplateFile) {
            templateFile.addBlock(ScriptValue(Free)) {
                base = 0
                // total number of jobs
                addModifier(job.modifierAdd)
                // subtract any jobs in use
                complexTriggerModifier("num_assigned_jobs", "subtract", mapOf("job" to job.symbol))
            }

            // EXPERIMENTAL
            // TODO verify this works!!
            templateFile.addBlock(ScriptValue(Suppressed)) {
                base = 0
                for (i in 1..suppressedMax) {
                    modifier {
                        add("$i")
                        hasModifier(suppressedPrefix + i)
                    }
                }
            }

            templateFile.addBlock(ScriptValue(Used)) {
                base = 0
                complexTriggerModifier("num_assigned_jobs", "add", mapOf("job" to job.symbol))
            }

            // TODO verify this works!
            templateFile.addBlock(ScriptValue(Total)) {
                base = 0
                addScriptValue(Suppressed)
                addModifier(job.modifierAdd)
            }

            // TODO verify this works!
            templateFile.addBlock(ScriptValue(Available)) {
                base = 0
                addModifier(job.modifierAdd)
            }

        }
    }

}
