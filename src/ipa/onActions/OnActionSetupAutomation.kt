import ipa.GENERATE_COLONY_AUTOMATION
import ipa.GENERATE_JOB_MANAGEMENT_EVENTS
import ipa.events.managejobs.ManageJobsGestalt
import ipa.events.managejobs.ManageJobsRegular
import stellaris.template.TemplateFile
import stellaris.various.OnActionBlock

class OnActionSetupAutomation : TemplateFile("common/on_actions/01_ipa.txt") {
    init {

        addBlock(OnActionBlock("on_game_start_country")) {
            event("ipa_settings_set_defaults.1")
        }

        addBlock(OnActionBlock("on_monthly_pulse")) {
            if (GENERATE_JOB_MANAGEMENT_EVENTS) {
                event(ManageJobsGestalt.IdUpdateStart)
                event(ManageJobsRegular.IdUpdateStart)
            }
            if (GENERATE_COLONY_AUTOMATION) event("ipa_colony_automation_priority.0")
        }

    }
}
