package ipa.events.managejobs

import ipa.scriptValues.JobCounts
import ipa.staticModifiers.SuppressJobClerk
import ipa.staticModifiers.SuppressJobEnforcer
import stellaris.events.CountryEvent
import stellaris.events.GenericEvent
import stellaris.events.PlanetEvent
import stellaris.logic.Comparators
import stellaris.logic.countOwnedPop
import stellaris.template.TemplateFile
import stellaris.types.Job
import stellaris.types.Variables

class ManageJobsRegular : TemplateFile("events/ipa_manage_jobs_regular.txt") {

    companion object {
        const val Namespace = "ipa_manage_jobs_regular"
        const val IdUpdateStart = "$Namespace.1"
        const val IdUpdateCountry = "$Namespace.2"

        const val IdClerkJobUpdate = "$Namespace.3"
        const val IdClerkJobReset = "$Namespace.4"

        const val IdEnforcerJobUpdate = "$Namespace.5"
        const val IdEnforcerJobReset = "$Namespace.6"
    }

    init {
        namespace = Namespace

        val amenitiesIncreaseLTEQ = 10
        val amenitiesDecreaseGTEQ = 18

        addBlock(GenericEvent(IdUpdateStart)) {
            comment = "global monthly event"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                everyCountry {
                    ifBlock {
                        limit {
                            OR {
                                isCountryType("default")
                                isCountryType("fallen_empire") // why include fallen empires here?
                                isCountryType("awakened_fallen_empire")
                            }
                        }
                        // TODO could use everyOwnedPlanet { ... } here directly?
                        countryEvent(IdUpdateCountry, 15)
                    }
                }
            }
        }

        addBlock(CountryEvent(IdUpdateCountry)) {
            comment = "update each country"
            hideWindow = true
            isTriggeredOnly = true

            immediate {

                // clerk jobs
                ifBlock {
                    limit {
                        isGestalt = false
                        isAi = false
                        checkVariable(Variables.OptionsManageJobAmenities, Comparators.EQ, 1)
                    }
                    everyOwnedPlanet {
                        planetEvent(IdClerkJobUpdate)
                    }
                }
                elseBlock {
                    everyOwnedPlanet {
                        planetEvent(IdClerkJobReset)
                    }
                }

                // enforcer jobs
                ifBlock {
                    limit {
                        isGestalt = false
                        isAi = false
                        checkVariable(Variables.OptionsManageJobCrime, Comparators.EQ, 1)
                    }
                    everyOwnedPlanet {
                        planetEvent(IdEnforcerJobUpdate)
                    }
                }
                elseBlock {
                    everyOwnedPlanet {
                        planetEvent(IdEnforcerJobReset)
                    }
                }

            }
        }

        addBlock(PlanetEvent(IdClerkJobUpdate)) {
            comment = "clerk: update planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                // increase blocks
                ifBlock {
                    comment = "if amenities >= $amenitiesDecreaseGTEQ then increase blocked jobs by one"
                    limit {
                        AND {
                            freeAmenities(Comparators.GTEQ, amenitiesDecreaseGTEQ)
                            checkVariableArithmetic("value:" + JobCounts.Clerk.Available, Comparators.GTEQ, 1)

                            // TODO logic works, apply to crime/bio trophy as well
                            //OR {
                            //    // don't increase blocked jobs if no job is used/exists, this prevents raising the modifier to max without need
                            //    checkVariableArithmetic("value:" + JobCounts.Clerk.Free, Comparators.GTEQ, 1)
                            //    countOwnedPop {
                            //        limit {
                            //            hasJob(Job.Clerk)
                            //        }
                            //        count(0, Comparators.GT)
                            //    }
                            //}
                        }
                    }

                    this@ifBlock.increaseBlockerFor(
                        SuppressJobClerk.Prefix,
                        SuppressJobClerk.Max
                    )
                }

                // decrease blocks
                elseIfBlock {
                    comment = "if amenities <= $amenitiesIncreaseLTEQ then decrease blocked jobs by one"
                    limit {
                        AND {
                            freeAmenities(Comparators.LTEQ, amenitiesIncreaseLTEQ)
                            checkVariableArithmetic("value:" + JobCounts.Clerk.Free, Comparators.LTEQ, 0)
                        }
                    }

                    this@elseIfBlock.decreaseBlockerFor(
                        SuppressJobClerk.Prefix,
                        SuppressJobClerk.Max
                    )
                }

            }
        }

        addBlock(PlanetEvent(IdClerkJobReset)) {
            comment = "clerk: reset planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                for (i in 1..SuppressJobClerk.Max) {
                    removeModifier("${SuppressJobClerk.Prefix}$i")
                }
            }
        }

        addBlock(PlanetEvent(IdEnforcerJobUpdate)) {
            comment = "enforcer: update planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {

                // increase blocks
                ifBlock {
                    comment = "if crime <= 0 then increase blocked jobs by one"
                    limit {
                        AND {
                            planetCrime(Comparators.LTEQ, 0)
                            countOwnedPop {
                                // don't increase blocked jobs if no job is used, this prevents raising the modifier without need
                                limit {
                                    hasJob(Job.Enforcer)
                                }
                                count(0, Comparators.GT)
                            }
                            // allow only up to 2 unemployed pops
                            // numUnemployed(Comparators.GT,1)
                        }
                    }

                    this@ifBlock.increaseBlockerFor(
                        SuppressJobEnforcer.Prefix,
                        SuppressJobEnforcer.Max
                    )
                }

                // decrease blocks
                elseIfBlock {
                    comment = "if crime is relevant or on the way to becoming relevant"
                    limit {
                        OR {
                            comment = "if crime is high"
                            planetCrime(Comparators.GTEQ, 25)
                            AND {
                                comment = "OR any crime situation is present"
                                planetCrime(Comparators.GTEQ, 5)
                                OR {
                                    hasModifier("criminal_underworld")
                                    hasModifier("drone_deviancy")
                                    hasModifier("drone_corruption")
                                }
                            }
                        }
                    }

                    this@elseIfBlock.decreaseBlockerFor(
                        SuppressJobEnforcer.Prefix,
                        SuppressJobEnforcer.Max
                    )
                }

            }
        }

        addBlock(PlanetEvent(IdEnforcerJobReset)) {
            comment = "enforcer: reset planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                for (i in 1..SuppressJobEnforcer.Max) {
                    removeModifier("${SuppressJobEnforcer.Prefix}$i")
                }
            }
        }
    }

}
