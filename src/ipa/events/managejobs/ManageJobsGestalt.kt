package ipa.events.managejobs

import ipa.scriptValues.JobCounts
import ipa.staticModifiers.SuppressJobBioTrophy
import ipa.staticModifiers.SuppressJobMaintenanceDrone
import ipa.staticModifiers.SuppressJobPatrolDrone
import stellaris.events.CountryEvent
import stellaris.events.GenericEvent
import stellaris.events.PlanetEvent
import stellaris.logic.Comparators
import stellaris.logic.countOwnedPop
import stellaris.template.TemplateFile
import stellaris.types.Job
import stellaris.types.Variables

class ManageJobsGestalt : TemplateFile("events/ipa_manage_jobs_gestalt.txt") {

    companion object {
        const val Namespace = "ipa_manage_jobs_gestalt"
        const val IdUpdateStart = "$Namespace.1"
        const val IdUpdateCountry = "$Namespace.2"

        const val IdMaintenanceDroneUpdate = "$Namespace.3"
        const val IdMaintenanceDroneReset = "$Namespace.4"

        const val IdPatrolDroneUpdate = "$Namespace.5"
        const val IdPatrolDroneReset = "$Namespace.6"

        const val IdBioTrophyUpdate = "$Namespace.7"
        const val IdBioTrophyReset = "$Namespace.8"
    }

    init {
        namespace = Namespace

        val maintenanceIncreaseLTEQ = 10
        val maintenanceDecreaseGTEQ = 18

        addBlock(GenericEvent(IdUpdateStart)) {
            comment = "global monthly event"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                everyCountry {
                    ifBlock {
                        limit {
                            OR {
                                isCountryType("default")
                                // TODO could filter more concisely upfront?
                                //isAI(false)
                                //isGestalt(true)
                                isCountryType("fallen_empire") // why include fallen empires here?
                                isCountryType("awakened_fallen_empire")
                            }
                        }
                        // TODO could use everyOwnedPlanet { ... } here directly?
                        countryEvent(IdUpdateCountry, 15)
                    }
                }
            }
        }

        addBlock(CountryEvent(IdUpdateCountry)) {
            comment = "update each country"
            hideWindow = true
            isTriggeredOnly = true

            immediate {

                // maintenance drones
                ifBlock {
                    limit {
                        isGestalt = true
                        isAi = false
                        checkVariable(Variables.OptionsManageJobAmenities, Comparators.EQ, 1)
                    }
                    everyOwnedPlanet {
                        planetEvent(IdMaintenanceDroneUpdate)
                    }
                }
                elseBlock {
                    everyOwnedPlanet {
                        planetEvent(IdMaintenanceDroneReset)
                    }
                }

                // patrol drones
                ifBlock {
                    limit {
                        isGestalt = true
                        isAi = false
                        checkVariable(Variables.OptionsManageJobCrime, Comparators.EQ, 1)
                    }
                    everyOwnedPlanet {
                        planetEvent(IdPatrolDroneUpdate)
                    }
                }
                elseBlock {
                    everyOwnedPlanet {
                        planetEvent(IdPatrolDroneReset)
                    }
                }

                // bio trophies
                ifBlock {
                    limit {
                        isGestalt = true
                        isAi = false
                        checkVariable(Variables.OptionsManageJobBioTrophy, Comparators.EQ, 1)
                    }
                    everyOwnedPlanet {
                        planetEvent(IdBioTrophyUpdate)
                    }
                }
                elseBlock {
                    everyOwnedPlanet {
                        planetEvent(IdBioTrophyReset)
                    }
                }
            }
        }

        addBlock(PlanetEvent(IdMaintenanceDroneUpdate)) {
            comment = "maintenance drones: update planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                // increase blocks
                ifBlock {
                    comment = "if amenities >= $maintenanceDecreaseGTEQ then increase blocked jobs by one"
                    limit {
                        AND {
                            freeAmenities(Comparators.GTEQ, maintenanceDecreaseGTEQ)
                            checkVariableArithmetic("value:" + JobCounts.MaintenanceDrone.Available, Comparators.GTEQ, 1)

                            // TODO logic works, apply to crime/bio trophy as well
                            //OR {
                            //    // don't increase blocked jobs if no job is used/exists, this prevents raising the modifier to max without need
                            //    checkVariableArithmetic("value:" + JobCounts.MaintenanceDroneFree, Comparators.GTEQ, 1)
                            //    countOwnedPop {
                            //        limit {
                            //            hasJob(Job.MaintenanceDrone)
                            //        }
                            //        count(0, Comparators.GT)
                            //    }
                            //}
                        }
                    }

                        this@ifBlock.increaseBlockerFor(
                        SuppressJobMaintenanceDrone.Prefix,
                        SuppressJobMaintenanceDrone.Max
                    )
                }

                // decrease blocks
                elseIfBlock {
                    comment = "if amenities <= $maintenanceIncreaseLTEQ then decrease blocked jobs by one"
                    limit {
                        AND {
                            freeAmenities(Comparators.LTEQ, maintenanceIncreaseLTEQ)
                            checkVariableArithmetic("value:" + JobCounts.MaintenanceDrone.Free, Comparators.LTEQ, 0)
                        }
                    }

                    this@elseIfBlock.  decreaseBlockerFor(
                        SuppressJobMaintenanceDrone.Prefix,
                        SuppressJobMaintenanceDrone.Max
                    )
                }

            }
        }

        addBlock(PlanetEvent(IdMaintenanceDroneReset)) {
            comment = "maintenance drones: reset planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                for (i in 1..SuppressJobMaintenanceDrone.Max) {
                    removeModifier("${SuppressJobMaintenanceDrone.Prefix}$i")
                }
            }
        }

        addBlock(PlanetEvent(IdPatrolDroneUpdate)) {
            comment = "patrol drones: update planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {

                // increase blocks
                ifBlock {
                    comment = "if crime <= 0 then increase blocked jobs by one"
                    limit {
                        AND {
                            planetCrime(Comparators.LTEQ, 0)
                            countOwnedPop {
                                // don't increase blocked jobs if no job is used, this prevents raising the modifier without need
                                limit {
                                    hasJob(Job.PatrolDrone)
                                }
                                count(0, Comparators.GT)
                            }
                            // allow only up to 2 unemployed pops
                            // numUnemployed(Comparators.GT,1)
                        }
                    }

                    this@ifBlock.increaseBlockerFor(
                        SuppressJobPatrolDrone.Prefix,
                        SuppressJobPatrolDrone.Max
                    )
                }

                // decrease blocks
                elseIfBlock {
                    comment = "if crime is relevant or on the way to becoming relevant"
                    limit {
                        OR {
                            comment = "if crime is high"
                            planetCrime(Comparators.GTEQ, 25)
                            AND {
                                comment = "OR any crime situation is present"
                                planetCrime(Comparators.GTEQ, 5)
                                OR {
                                    hasModifier("criminal_underworld")
                                    hasModifier("drone_deviancy")
                                    hasModifier("drone_corruption")
                                }
                            }
                        }
                    }

                    this@elseIfBlock.decreaseBlockerFor(
                        SuppressJobPatrolDrone.Prefix,
                        SuppressJobPatrolDrone.Max
                    )
                }

            }
        }

        addBlock(PlanetEvent(IdPatrolDroneReset)) {
            comment = "patrol drones: reset planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                for (i in 1..SuppressJobPatrolDrone.Max) {
                    removeModifier("${SuppressJobPatrolDrone.Prefix}$i")
                }
            }
        }

        addBlock(PlanetEvent(IdBioTrophyUpdate)) {
            comment = "bio trophies: update planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                ifBlock {
                    limit {
                        checkVariableArithmetic("value:" + JobCounts.BioTrophy.Free, Comparators.GTEQ, 2)
                    }

                    this@ifBlock.increaseBlockerFor(
                        SuppressJobBioTrophy.Prefix,
                        SuppressJobBioTrophy.Max
                    )
                }

                elseIfBlock {
                    limit {
                        checkVariableArithmetic("value:" + JobCounts.BioTrophy.Free, Comparators.LTEQ, 0)
                    }

                    this@elseIfBlock.decreaseBlockerFor(
                        SuppressJobBioTrophy.Prefix,
                        SuppressJobBioTrophy.Max
                    )
                }

            }
        }

        addBlock(PlanetEvent(IdBioTrophyReset)) {
            comment = "bio trophies: reset planet"
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                for (i in 1..SuppressJobBioTrophy.Max) {
                    removeModifier("${SuppressJobBioTrophy.Prefix}$i")
                }
            }
        }
    }

}
