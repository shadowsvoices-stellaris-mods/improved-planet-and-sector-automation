package ipa.events

import stellaris.template.TemplateFile
import stellaris.logic.Comparators
import stellaris.events.CountryEvent
import stellaris.types.Variables

class IpaSettingsSetDefaults : TemplateFile("events/ipa_settings_set_defaults.txt", {
    namespace = "ipa_settings_set_defaults"

    // Main Menu
    addBlock(CountryEvent("$namespace.1")) {
        title = "$eventId.title"
        desc = "$eventId.desc"
        isTriggeredOnly = true
        hideWindow = true

        trigger {
            checkVariable(Variables.Initialized, Comparators.NEQ, 1)
        }

        immediate {
            for (variable in Variables.values()) {
                if (variable.defaultValue != null) {
                    setVariable(variable.key, variable.defaultValue)
                }
            }
        }

    }

})
