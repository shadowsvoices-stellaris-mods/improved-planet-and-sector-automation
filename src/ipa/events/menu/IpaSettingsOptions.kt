package ipa.events.menu

import stellaris.template.TemplateFile
import stellaris.events.CountryEvent
import ipa.scriptedtriggers.IpaBuildCityForBuildings
import stellaris.types.Variables

class IpaSettingsOptions : TemplateFile("events/ipa_settings_options.txt", {
    namespace = "ipa_settings_options"

    // Main Menu
    addBlock(CountryEvent("ipa_settings_options.1")) {
        title = "$eventId.title"
        desc = "$eventId.desc"

        option("allow_growth_buildings") {
            hiddenEffect {
                countryEvent("ipa_settings_options_allow_growth_buildings.1")
            }
        }

        // upgrade min free slots submenu with indicator
        addSubmenuWithIndicators(
            Variables.OptionsUpgradeMinFreeSlots,
            "ipa_settings_options_upgrade_min_free_slots.1",
            mutableMapOf<String, String>().also {
                for (i in IpaSettingsOptionsUpgradeMinFreeSlots.optionValues) {
                    it[i.toString()] = "upgrade_min_free_slots_$i"
                }
            }
        )

        // pre-build jobs submenu with indicator
        addSubmenuWithIndicators(
            Variables.OptionsPreBuildJobs,
            "ipa_settings_options_pre_build_jobs.1",
            mutableMapOf<String, String>().also {
                for (i in IpaSettingsOptionsPreBuildJobs.optionValues) {
                    it[i.toString()] = "pre_build_jobs_$i"
                }
            }
        )

        // build city districts
        addSubmenuWithIndicators(
            Variables.OptionsBuildCityForBuildings,
            "ipa_settings_options_build_city_for_buildings.1",
            mutableMapOf<String, String>().also {
                for (opt in IpaBuildCityForBuildings.Options.values()) {
                    it[opt.value.toString()] = "build_city_for_buildings_${opt.key}"
                }
            }
        )

        addToggleOption(Variables.OptionsAllowUpgradeStronghold)
        addToggleOption(Variables.OptionsAllowAgriFarmBuildings)
        addToggleOption(Variables.OptionsForceShowIncomeOptions)
        addToggleOption(Variables.OptionsDisallowUnity)

        // Back Option
        option("back") {
            hiddenEffect {
                // go back to main menu
                countryEvent("ipa_settings_main.1")
            }
        }

    }

})
