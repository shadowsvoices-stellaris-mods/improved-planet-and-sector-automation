package ipa.events.menu

import stellaris.template.TemplateFile
import stellaris.events.CountryEvent
import stellaris.types.Variables

class IpaSettingsManageJobs : TemplateFile("events/ipa_settings_manage_jobs.txt", {
    namespace = "ipa_settings_manage_jobs"

    // Main Menu
    addBlock(CountryEvent("$namespace.1")) {
        title = "$eventId.title"
        desc = "$eventId.desc"

        addToggleOption(Variables.OptionsManageJobAmenities)
        addToggleOption(Variables.OptionsManageJobCrime)
        addToggleOption(Variables.OptionsManageJobBioTrophy)

        // Back Option
        option("back") {
            hiddenEffect {
                // go back to main menu
                countryEvent("ipa_settings_main.1")
            }
        }

    }

})
