package ipa.events.menu

import stellaris.types.Variables
import stellaris.template.TemplateFile
import stellaris.logic.toYesNo
import stellaris.events.CountryEvent

class IpaSettingsOptionsAllowGrowthBuildings : TemplateFile("events/ipa_settings_options_allow_growth_buildings.txt", {
    namespace = "ipa_settings_options_allow_growth_buildings"

    // Main Menu
    addBlock(CountryEvent("$namespace.1")) {
        title = "$eventId.title"
        desc = "$eventId.desc"

        val subPolicies = mapOf(
            "ipa_allow_growth_clinic" to Variables.OptionsAllowGrowthClinic,
            "ipa_allow_growth_hospital" to Variables.OptionsAllowGrowthHospital,
            "ipa_allow_growth_robot_assembly" to Variables.OptionsAllowGrowthRobotAssembly,
            "ipa_allow_growth_clone_vats" to Variables.OptionsAllowGrowthCloneVats,
            "ipa_allow_growth_machine_assembly" to Variables.OptionsAllowGrowthMachineAssembly,
            "ipa_allow_growth_hive_spawning_pool" to Variables.OptionsAllowGrowthHiveSpawningPool,
            "ipa_allow_growth_necroid_elevation" to Variables.OptionsAllowGrowthNecroidElevation
        )

        for ((subPolicy, policyVar) in subPolicies) {

            val optionPrefix = subPolicy.replace("ipa_allow_growth_", "")
            // option: toggle allowed -> forbidden
            option("${optionPrefix}_allowed") {
                trigger { conditions.add("$subPolicy = ${true.toYesNo()}") }

                hiddenEffect {
                    setVariable(policyVar, 1)
                    countryEvent("$namespace.1")
                }
            }

            // option: toggle forbidden -> allowed
            option("${optionPrefix}_forbidden") {
                trigger { conditions.add("$subPolicy = ${false.toYesNo()}") }

                hiddenEffect {
                    setVariable(policyVar, 0)
                    countryEvent("$namespace.1")
                }
            }

        }

        // Back Option
        option("back") {
            hiddenEffect {
                countryEvent("ipa_settings_options.1")
            }
        }

    }

})
