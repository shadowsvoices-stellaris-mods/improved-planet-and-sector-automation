package ipa.events.menu

import stellaris.types.Variables
import stellaris.template.TemplateFile
import stellaris.logic.Comparators
import stellaris.events.CountryEvent

class IpaSettingsOptionsUpgradeMinFreeSlots : TemplateFile("events/ipa_settings_options_upgrade_min_free_slots.txt") {

    companion object {
        val optionValues = 0..10
    }

    init {
        namespace = "ipa_settings_options_upgrade_min_free_slots"

        // Main Menu
        addBlock(CountryEvent("$namespace.1")) {
            title = "$eventId.title"
            desc = "$eventId.desc"

            for (i in optionValues) {

                // selected option variant
                option("option$i.current") {
                    trigger {
                        checkVariable(Variables.OptionsUpgradeMinFreeSlots, Comparators.EQ, i)
                    }

                    hiddenEffect {
                        setVariable(Variables.OptionsUpgradeMinFreeSlots, i)
                        countryEvent("$namespace.1") // show dialog again
                    }
                }

                // not selected option variant
                option("option$i.change") {
                    trigger {
                        NOT {
                            checkVariable(Variables.OptionsUpgradeMinFreeSlots, Comparators.EQ, i)
                        }
                    }

                    hiddenEffect {
                        countryEvent("$namespace.1") // show dialog again
                        setVariable(Variables.OptionsUpgradeMinFreeSlots, i)
                    }
                }

            }

            // Back Option
            option("back") {
                hiddenEffect {
                    countryEvent("ipa_settings_options.1")
                }
            }

        }
    }
}
