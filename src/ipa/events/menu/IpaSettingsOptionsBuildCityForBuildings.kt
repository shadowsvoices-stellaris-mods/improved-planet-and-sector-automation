package ipa.events.menu

import stellaris.template.TemplateFile
import stellaris.logic.Comparators
import stellaris.events.CountryEvent
import ipa.scriptedtriggers.IpaBuildCityForBuildings
import stellaris.types.Variables

class IpaSettingsOptionsBuildCityForBuildings : TemplateFile("events/ipa_settings_options_build_city_for_buildings.txt") {

    init {
        namespace = "ipa_settings_options_build_city_for_buildings"

        // Main Menu
        addBlock(CountryEvent("$namespace.1")) {
            title = "$eventId.title"
            desc = "$eventId.desc"

            for (opt in IpaBuildCityForBuildings.Options.values()) {

                // selected option variant
                option("option_${opt.key}.current") {
                    trigger {
                        checkVariable(Variables.OptionsBuildCityForBuildings, Comparators.EQ, opt.value)
                    }

                    hiddenEffect {
                        setVariable(Variables.OptionsBuildCityForBuildings, opt.value)
                        countryEvent("$namespace.1") // show dialog again
                    }
                }

                // not selected option variant
                option("option_${opt.key}.change") {
                    trigger {
                        NOT {
                            checkVariable(Variables.OptionsBuildCityForBuildings, Comparators.EQ, opt.value)
                        }
                    }

                    hiddenEffect {
                        countryEvent("$namespace.1") // show dialog again
                        setVariable(Variables.OptionsBuildCityForBuildings, opt.value)
                    }
                }

            }

            // Back Option
            option("back") {
                hiddenEffect {
                    countryEvent("ipa_settings_options.1")
                }
            }

        }
    }
}
