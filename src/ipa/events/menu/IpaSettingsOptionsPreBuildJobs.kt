package ipa.events.menu

import stellaris.template.TemplateFile
import stellaris.logic.Comparators
import stellaris.events.CountryEvent
import stellaris.types.Variables

class IpaSettingsOptionsPreBuildJobs : TemplateFile("events/ipa_settings_options_pre_build_jobs.txt") {

    companion object {
        val optionValues = arrayOf(0, 1, 2, 3, 4, 5, 10)
    }

    init {
        namespace = "ipa_settings_options_pre_build_jobs"

        // Main Menu
        addBlock(CountryEvent("$namespace.1")) {
            title = "$eventId.title"
            desc = "$eventId.desc"

            for (i in optionValues) {

                // selected option variant
                option("option$i.current") {
                    trigger {
                        checkVariable(Variables.OptionsPreBuildJobs, Comparators.EQ, i)
                    }

                    hiddenEffect {
                        setVariable(Variables.OptionsPreBuildJobs, i)
                        countryEvent("$namespace.1") // show dialog again
                    }
                }

                // not selected option variant
                option("option$i.change") {
                    trigger {
                        NOT {
                            checkVariable(Variables.OptionsPreBuildJobs, Comparators.EQ, i)
                        }
                    }

                    hiddenEffect {
                        countryEvent("$namespace.1") // show dialog again
                        setVariable(Variables.OptionsPreBuildJobs, i)
                    }
                }

            }

            // Back Option
            option("back") {
                hiddenEffect {
                    countryEvent("ipa_settings_options.1")
                }
            }

        }
    }
}
