package ipa.events.menu

import ipa.Policies
import stellaris.types.Variables

class IpaSettingsEnergyMinimum : IpaSettingsIncomeGeneric(
    "ipa_settings_energy_minimum",
    Variables.IncomeEnergyMinimum,
    Policies.incomeSteps
)

class IpaSettingsFoodMinimum : IpaSettingsIncomeGeneric(
    "ipa_settings_food_minimum",
    Variables.IncomeFoodMinimum,
    Policies.incomeSteps
)

class IpaSettingsFoodDesired : IpaSettingsIncomeGeneric(
    "ipa_settings_food_desired",
    Variables.IncomeFoodDesired,
    Policies.incomeSteps
)

class IpaSettingsMineralsMinimum : IpaSettingsIncomeGeneric(
    "ipa_settings_minerals_minimum",
    Variables.IncomeMineralsMinimum,
    Policies.incomeSteps
)

class IpaSettingsAlloysDesired : IpaSettingsIncomeGeneric(
    "ipa_settings_alloys_desired",
    Variables.IncomeAlloysDesired,
    Policies.incomeSteps
)

class IpaSettingsAlloysMinimum : IpaSettingsIncomeGeneric(
    "ipa_settings_alloys_minimum",
    Variables.IncomeAlloysMinimum,
    Policies.incomeSteps
)

class IpaSettingsConsumerGoodsMinimum : IpaSettingsIncomeGeneric(
    "ipa_settings_consumer_goods_minimum",
    Variables.IncomeConsumerGoodsMinimum,
    Policies.incomeSteps
)

class IpaSettingsConsumerGoodsDesired : IpaSettingsIncomeGeneric(
    "ipa_settings_consumer_goods_desired",
    Variables.IncomeConsumerGoodsDesired,
    Policies.incomeSteps
)

class IpaSettingsStrategicMinimum : IpaSettingsIncomeGeneric(
    "ipa_settings_strategic_minimum",
    Variables.IncomeStrategicMinimum,
    Policies.incomeStepsStrategic
)

class IpaSettingsStrategicDesired : IpaSettingsIncomeGeneric(
    "ipa_settings_strategic_desired",
    Variables.IncomeStrategicDesired,
    Policies.incomeStepsStrategic
)
