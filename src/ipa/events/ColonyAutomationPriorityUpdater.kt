package ipa.events

import ipa.colonyautomation.ColonyAutomationPriority
import stellaris.events.CountryEvent
import stellaris.events.GenericEvent
import stellaris.template.TemplateFile
import stellaris.types.Variables

class ColonyAutomationPriorityUpdater : TemplateFile("events/ipa_colony_automation_priority.txt") {

    companion object {
        const val Namespace = "ipa_colony_automation_priority"
    }

    val prioritiesSortedByDay = ColonyAutomationPriority.values().sortedBy { it.day }

    init {
        namespace = Namespace
        require(prioritiesSortedByDay.size <= 30) { "Cannot generate more then 30 priorities" }

        addBlock(GenericEvent("$namespace.0")) {
            hideWindow = true
            isTriggeredOnly = true

            immediate {
                everyCountry {
                    ifBlock {
                        limit {
                            OR {
                                isAi = false
                            }
                        }

                        for (priority in prioritiesSortedByDay) {
                            val day = if (priority.day > 0) priority.day else null
                            countryEvent("$namespace.${priority.priority}", day)
                        }
                    }
                }
            }
        }

        for (priority in prioritiesSortedByDay) {
            addBlock(CountryEvent("$namespace.${priority.priority}")) {
                hideWindow = true
                isTriggeredOnly = true

                immediate {
                    setVariable(Variables.ColonyAutomationPriority, priority.priority)
                }
            }
        }

    }


}
