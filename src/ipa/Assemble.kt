package ipa

import OnActionSetupAutomation
import ipa.colonyautomation.ColonyAutomationCategories
import ipa.colonyautomation.designations.*
import ipa.colonyautomation.exceptions.*
import ipa.events.ColonyAutomationPriorityUpdater
import ipa.events.IpaSettingsSetDefaults
import ipa.events.managejobs.ManageJobsGestalt
import ipa.events.managejobs.ManageJobsRegular
import ipa.events.menu.*
import ipa.ndefines.IpaNAIDefines
import ipa.ndefines.IpaNEconomyDefines
import ipa.ndefines.IpaNInterfaceDefines
import ipa.scriptValues.JobCounts
import ipa.scriptedtriggers.*
import ipa.staticModifiers.*
import ipa.translations.*
import stellaris.template.TemplateContext
import stellaris.template.TemplateFile
import ipa.sectorfocus.Basic as SectorFocusBasic
import ipa.sectorfocus.Designated as SectorFocusDesignated
import ipa.sectorfocus.DesignatedWIP as SectorFocusDesignatedWIP
import ipa.sectorfocus.Mixed as SectorFocusMixed
import ipa.sectorfocus.Strict as SectorFocusStrict
import ipa.sectorfocus.StrictWIP as SectorFocusStrictWIP

object Assemble {

    @JvmStatic
    fun main(args: Array<String>) {
        val ctx = TemplateContext("./mod", "./doc")

        if (GENERATE_SECTOR_AUTOMATION) {
            ctx.addFile(SectorFocusBasic())
            ctx.addFile(SectorFocusDesignated())
            ctx.addFile(SectorFocusMixed())
            ctx.addFile(SectorFocusStrict())
            ctx.addFile(SectorFocusStrictWIP())
            ctx.addFile(SectorFocusDesignatedWIP())
        }

        ctx.addFile(TemplateFile("common/defines/01_ipa.txt") {
            addBlock(IpaNEconomyDefines())
            addBlock(IpaNAIDefines())
            addBlock(IpaNInterfaceDefines())
        })

        ctx.addFile(IpaHasDesignations("common/scripted_triggers/ipa_designations.txt"))

        ctx.addFile(IpaMissingIncomes("common/scripted_triggers/ipa_income.txt"))

        ctx.addFile(TemplateFile("common/scripted_triggers/ipa_options.txt") {
            addBlock(IpaAllowGrowthClinic())
            addBlock(IpaAllowGrowthHospital())
            addBlock(IpaAllowGrowthRobotAssembly())
            addBlock(IpaAllowGrowthCloneVats())
            addBlock(IpaAllowGrowthMachineAssembly())
            addBlock(IpaAllowGrowthHiveSpawningPool())
            addBlock(IpaAllowGrowthNecroidElevation())
            addBlock(IpaUpgradeMinFreeSlots())
            addBlock(IpaUnityAllowed())
            addBlock(IpaNeedMoreJobsCheck())
            addBlock(IpaBuildCityForBuildings())
        })

        ctx.addFile(TemplateFile("common/scripted_triggers/ipa_country.txt") {
            addBlock(IpaCountryUsesFood())
        })

        if (GENERATE_DISABLE_VANILLA_COLONY_AUTOMATION) {
            // overwrite vanilla files to disable them
            ctx.addFile(TemplateFile("common/colony_automation_exceptions/00_crisis_exceptions.txt"))
            for (file in arrayOf(
                "00_capital_automation.txt",
                "00_capital_hive_automation.txt",
                "00_capital_machine_automation.txt",
                "00_capital_machine_servitor_automation.txt",
                "00_colony_automation.txt",
                "00_colony_machine_automation.txt",
                "00_factory_automation.txt",
                "00_farming_automation.txt",
                "00_fortress_automation.txt",
                "00_foundry_automation.txt",
                "00_generator_automation.txt",
                "00_industrial_automation.txt",
                "00_mining_automation.txt",
                "00_penal_automation.txt",
                "00_refinery_automation.txt",
                "00_research_automation.txt",
                "00_resort_automation.txt",
                "00_ringworld_automation.txt",
                "00_ringworld_hive_automation.txt",
                "00_ringworld_machine_automation.txt",
                "00_rural_automation.txt",
                "00_slave_automation.txt",
                "00_trade_automation.txt",
                "00_unity_automation.txt",
                "00_urban_automation.txt",
            )) {
                ctx.addFile(TemplateFile("common/colony_automation/$file"))
            }
        }

        if (GENERATE_COLONY_AUTOMATION) {
            require(GENERATE_DISABLE_VANILLA_COLONY_AUTOMATION) { "Cannot generate colony automation logic without disabling vanilla logic" }

            // colony automation
            ctx.addFile(TemplateFile("common/colony_automation/ipa_capital.txt") {
                addBlock(ColonyAutomationCapital())
                addBlock(ColonyAutomationCapitalHive())
                addBlock(ColonyAutomationCapitalMachine())
                addBlock(ColonyAutomationCapitalMachineServitor())
                addBlock(ColonyAutomationDefault())
                addBlock(ColonyAutomationDefaultMachine())
            })
            ctx.addFile(TemplateFile("common/colony_automation/ipa_default.txt") {
                addBlock(ColonyAutomationDefault())
                addBlock(ColonyAutomationDefaultMachine())
            })
            ctx.addFile(TemplateFile("common/colony_automation/ipa_resources.txt") {
                addBlock(ColonyAutomationIndustrialStrict())
                addBlock(ColonyAutomationFarmingStrict())
                addBlock(ColonyAutomationMiningStrict())
                addBlock(ColonyAutomationGeneratorStrict())

                addBlock(ColonyAutomationIndustrialMixed())
                addBlock(ColonyAutomationFarmingMixed())
                addBlock(ColonyAutomationMiningMixed())
                addBlock(ColonyAutomationGeneratorMixed())
            })

            // colony automation exceptions
            ctx.addFile(TemplateFile("common/colony_automation_exceptions/ipa_common.txt") {
                addBlock(CapitalBuildings())
                addBlock(HousingEmergency())
                addBlock(HousingPrebuild())
            })
            ctx.addFile(TemplateFile("common/colony_automation_exceptions/ipa_gestalt_machine.txt") {
                addBlock(GestaltMachineCrime())
                addBlock(GestaltMachineAmenities())
                addBlock(GestaltMachineAmenitiesJobs())
                addBlock(GestaltMachineAssembly())
                addBlock(GestaltBioTrophy())
            })
            ctx.addFile(TemplateFile("common/colony_automation_exceptions/ipa_research.txt") {
                addBlock(ResearchAutomation())
            })

            // categories
            ctx.addFile(ColonyAutomationCategories())

            // helper event
            ctx.addFile(ColonyAutomationPriorityUpdater())
        }

        // events: setup
        ctx.addFile(IpaSettingsSetDefaults())

        // events: menu
        ctx.addFile(IpaSettingsMain())
        ctx.addFile(IpaSettingsEnergyMinimum())
        ctx.addFile(IpaSettingsMineralsMinimum())
        ctx.addFile(IpaSettingsFoodMinimum())
        ctx.addFile(IpaSettingsFoodDesired())
        ctx.addFile(IpaSettingsAlloysMinimum())
        ctx.addFile(IpaSettingsAlloysDesired())
        ctx.addFile(IpaSettingsConsumerGoodsMinimum())
        ctx.addFile(IpaSettingsConsumerGoodsDesired())
        ctx.addFile(IpaSettingsStrategicMinimum())
        ctx.addFile(IpaSettingsStrategicDesired())
        ctx.addFile(IpaSettingsOptions())
        ctx.addFile(IpaSettingsOptionsAllowGrowthBuildings())
        ctx.addFile(IpaSettingsOptionsUpgradeMinFreeSlots())
        ctx.addFile(IpaSettingsOptionsPreBuildJobs())
        ctx.addFile(IpaSettingsOptionsBuildCityForBuildings())

        if (GENERATE_JOB_MANAGEMENT_EVENTS) {
            // menu: manage jobs
            ctx.addFile(IpaSettingsManageJobs())

            // events: manage jobs
            ctx.addFile(ManageJobsGestalt())
            ctx.addFile(ManageJobsRegular())

            // static modifiers
            ctx.addFile(SuppressJobMaintenanceDrone())
            ctx.addFile(SuppressJobPatrolDrone())
            ctx.addFile(SuppressJobBioTrophy())
            ctx.addFile(SuppressJobEnforcer())
            ctx.addFile(SuppressJobClerk())
        }

        // script values
        ctx.addFile(JobCounts())

        // on action
        ctx.addFile(OnActionSetupAutomation())

        // translations
        ctx.addFile(TranslationEnglish())
        ctx.addFile(TranslationFrench())
        ctx.addFile(TranslationGerman())
        ctx.addFile(TranslationPolish())
        ctx.addFile(TranslationRussian())
        ctx.addFile(TranslationSpanish())
        ctx.addFile(TranslationChineseSimple())
        ctx.addFile(TranslationBrazPor())

        ctx.writeFiles()
    }

}
