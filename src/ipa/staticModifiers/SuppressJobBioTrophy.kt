package ipa.staticModifiers

import stellaris.template.TemplateFile
import stellaris.various.StaticModifier

class SuppressJobBioTrophy : TemplateFile("common/static_modifiers/ipa_suppress_job_bio_trophy.txt") {

    companion object {
        const val Name = "ipa_suppress_job_bio_trophy"
        const val Prefix = Name + "_"
        const val Max = 30
    }

    init {
        for (i in 1..Max) {
            addBlock(StaticModifier(Prefix + i)) {
                // icon = "gfx/interface/icons/planet_modifiers/AutoDroneControl_Maintenance.dds"
                icon = "gfx/interface/icons/planet_modifiers/pm_unknown.dds"
                iconFrame = 1
                addEffect("job_bio_trophy_add", (-i).toString())
                customTooltip = Name
            }
        }
    }
}

