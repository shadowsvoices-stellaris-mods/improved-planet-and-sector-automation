package ipa.staticModifiers

import stellaris.template.TemplateFile
import stellaris.various.StaticModifier

class SuppressJobEnforcer : TemplateFile("common/static_modifiers/ipa_suppress_job_enforcer.txt") {

    companion object {
        const val Name = "ipa_suppress_job_enforcer"
        const val Prefix = Name + "_"
        const val Max = 10
    }

    init {
        for (i in 1..Max) {
            addBlock(StaticModifier(Prefix + i)) {
                // icon = "gfx/interface/icons/planet_modifiers/AutoDroneControl_Maintenance.dds"
                icon = "gfx/interface/icons/planet_modifiers/pm_unknown.dds"
                iconFrame = 1
                addEffect("job_enforcer_add", (-i).toString())
                customTooltip = Name
            }
        }
    }
}

