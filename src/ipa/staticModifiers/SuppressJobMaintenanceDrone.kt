package ipa.staticModifiers

import stellaris.template.TemplateFile
import stellaris.various.StaticModifier

class SuppressJobMaintenanceDrone : TemplateFile("common/static_modifiers/ipa_suppress_job_maintenance_drone.txt") {

    companion object {
        const val Name = "ipa_suppress_job_maintenance_drone"
        const val Prefix = Name + "_"
        const val Max = 150 // TODO rewrite this in the future to not need multiple blocks and endlessly chained if conditions
    }

    init {
        for (i in 1..Max) {
            addBlock(StaticModifier(Prefix + i)) {
                // icon = "gfx/interface/icons/planet_modifiers/AutoDroneControl_Maintenance.dds"
                icon = "gfx/interface/icons/planet_modifiers/pm_unknown.dds"
                iconFrame = 1
                addEffect("job_maintenance_drone_add", (-i).toString())
                customTooltip = Name
            }
        }
    }
}

