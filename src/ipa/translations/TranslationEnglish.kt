package ipa.translations

import ipa.ModInfo
import ipa.Policies
import ipa.events.menu.IpaSettingsOptionsPreBuildJobs
import ipa.events.menu.IpaSettingsOptionsUpgradeMinFreeSlots
import ipa.scriptedtriggers.IpaBuildCityForBuildings
import ipa.staticModifiers.*
import stellaris.template.TranslationFile

open class TranslationEnglish(target: String, language: String) : TranslationFile(target, language) {

    constructor() : this("localisation/english/ipa_l_english.yml", "l_english")

    init {
        addCommonTranslations()
    }

    fun addCommonTranslations() {
        addVarious()
        addMenuOptions()
        addMenuIncomes()
        addStaticModifiersBlockJob()
    }

    open fun addMenuIncomes() {
        addMenuIncomeTranslation("energy_minimum", "£energy£ §BMinimum §!Energy Income", Policies.incomeSteps)
        addMenuIncomeTranslation("minerals_minimum", "£minerals£ §BMinimum §!Minerals Income", Policies.incomeSteps)
        addMenuIncomeTranslation("food_minimum", "£food£ §BMinimum §!Food Income", Policies.incomeSteps)
        addMenuIncomeTranslation("food_desired", "£food£ §EDesired §!Food Income", Policies.incomeSteps)
        addMenuIncomeTranslation("alloys_minimum", "£alloys£ §BMinimum §!Alloys Income", Policies.incomeSteps)
        addMenuIncomeTranslation("alloys_desired", "£alloys£ §EDesired §!Alloys Income", Policies.incomeSteps)
        addMenuIncomeTranslation(
            "consumer_goods_minimum",
            "£consumer_goods£ §BMinimum §!Consumer Goods Income",
            Policies.incomeSteps
        )
        addMenuIncomeTranslation(
            "consumer_goods_desired",
            "£consumer_goods£ §EDesired §!Consumer Goods Income",
            Policies.incomeSteps
        )
        addMenuIncomeTranslation(
            "strategic_minimum",
            "£volatile_motes£ £exotic_gases£ £rare_crystals£ §BMinimum §!Strategic Income",
            Policies.incomeStepsStrategic
        )
        addMenuIncomeTranslation(
            "strategic_desired",
            "£volatile_motes£ £exotic_gases£ £rare_crystals£ §EDesired §!Strategic Income",
            Policies.incomeStepsStrategic
        )
    }

    open fun addMenuIncomeTranslation(eventKey: String, dialogLabel: String, steps: Array<Int>) {
        // Main Menu
        add("ipa_settings_main.1.$eventKey.disabled", "$dialogLabel: §RDisabled")
        for (step in steps) {
            add("ipa_settings_main.1.$eventKey.step$step", "$dialogLabel: §G$step")
        }
        add("ipa_settings_main.1.$eventKey.unlimited", "$dialogLabel: §RUnlimited")

        // Sub Menu
        add("ipa_settings_$eventKey.1.title", dialogLabel)
        add("ipa_settings_$eventKey.1.desc", "")
        add("ipa_settings_$eventKey.1.disabled", "$dialogLabel: §RDisabled")
        add("ipa_settings_$eventKey.1.unlimited", "$dialogLabel: §RUnlimited")
        for (step in steps) {
            add("ipa_settings_$eventKey.1.step$step", "$dialogLabel: §G$step")
        }
        add("ipa_settings_$eventKey.1.back", "§RBack")
    }

    open fun addVarious() {
        // Sector Types
        add("st_designated", "Designated Focus")
        add("st_designated_desc", "The Sector will focus on planet designations and develop them accordingly.")

        add("st_mixed", "Mixed Focus")
        add(
            "st_mixed_desc",
            "The Sector will build on all planets, regardless of planet designations. This is inherently less efficient due to the loss of specialization bonuses but may be advantageous in the early game."
        )

        add("st_strict", "Strict Focus")
        add(
            "st_strict_desc",
            "The Sector will focus on planet designations but will never cross-build mining/farming/generator districts. If a planet is designated as a mining planet it will not build generator or farming districts."
        )

        add("st_designated_wip", "Designated Focus (WIP)")
        add(
            "st_designated_wip_desc",
            "Includes work-in-progress features and changes that may not be fully ready yet. If this designation is causing problems please report them and consider switching to non-wip variant of the same focus."
        )

        add("st_strict_wip", "Strict Focus (WIP)")
        add(
            "st_strict_wip_desc",
            "Includes work-in-progress features and changes that may not be fully ready yet. If this designation is causing problems please report them and consider switching to non-wip variant of the same focus."
        )
    }

    open fun addMenuOptions() {
        // Edicts
        add("edict_ipa_settings_menu", "§BImproved Planet & Sector Automation")

        // Settings Menu
        "ipa_settings_main".let { prefix ->
            add("$prefix.1.title", "Improved Planet and Sector Automation: §Bv${ModInfo.Version}")
            add(
                "$prefix.1.desc",
                "Improved Planet and Sector Automation: §Bv${ModInfo.Version} §!Updated: ${ModInfo.UpdatedAt}"
            )
            add("$prefix.1.options", "§BOptions")
            add("$prefix.1.close", "§RClose")
        }

        "ipa_settings_options".let { prefix ->
            add("$prefix.1.title", "§BOptions")
            add("$prefix.1.desc", "")
            add("$prefix.1.back", "§RBack")
            add("$prefix.1.allow_growth_buildings", "Configure: Growth buildings")
            for (i in IpaSettingsOptionsUpgradeMinFreeSlots.optionValues) {
                add("$prefix.1.upgrade_min_free_slots_$i", "Building upgrade min free slots: §G$i")
            }
            for (i in IpaSettingsOptionsPreBuildJobs.optionValues) {
                add("$prefix.1.pre_build_jobs_$i", "Pre-build jobs: §G$i")
            }
            add("$prefix.1.build_city_for_buildings_disabled", "Build city for building slots: §RDisabled")
            add(
                "$prefix.1.build_city_for_buildings_research_only",
                "Build city for building slots: §GResearch Designations"
            )
            add("$prefix.1.build_city_for_buildings_everywhere", "Build city for building slots: §GAll Designations")
            add("$prefix.1.ipa_options_allow_upgrade_strongholds_yes", "Upgrade Strongholds: §GYes")
            add("$prefix.1.ipa_options_allow_upgrade_strongholds_no", "Upgrade Strongholds: §RNo")
            add("$prefix.1.ipa_options_allow_agri_farm_buildings_yes", "Farm Building on Agri Designation: §GYes")
            add("$prefix.1.ipa_options_allow_agri_farm_buildings_no", "Farm Building on Agri Designation: §RNo")
            add("$prefix.1.ipa_options_force_show_income_options_yes", "Force Show Income Options: §GYes")
            add("$prefix.1.ipa_options_force_show_income_options_no", "Force Show Income Options: §RNo")
            add("$prefix.1.ipa_options_disallow_unity_yes", "Disallow unity buildings: §GYes")
            add("$prefix.1.ipa_options_disallow_unity_no", "Disallow unity buildings: §RNo")
        }

        // Manage Jobs Menu
        add("ipa_settings_main.1.manage_jobs", "§BManage Jobs")
        "ipa_settings_manage_jobs".let { prefix ->
            add("$prefix.1.title", "§BManage Jobs")
            add(
                "$prefix.1.desc",
                "Job management controls if certain jobs should be enabled/disabled automatically to optimize efficiency. For example when planet amenities are above a certain threshold then maintenance/clerk jobs will be disabled."
            )
            add("$prefix.1.back", "§RBack")
            add("$prefix.1.ipa_options_manage_job_amenities_yes", "Manage Amenities Jobs: §GYes")
            add("$prefix.1.ipa_options_manage_job_amenities_no", "Manage Amenities Jobs: §RNo")
            add("$prefix.1.ipa_options_manage_job_crime_yes", "Manage Crime Jobs: §GYes")
            add("$prefix.1.ipa_options_manage_job_crime_no", "Manage Crime Jobs: §RNo")
            add("$prefix.1.ipa_options_manage_job_bio_trophy_yes", "Manage Bio Trophy Jobs: §GYes")
            add("$prefix.1.ipa_options_manage_job_bio_trophy_no", "Manage Bio Trophy Jobs: §RNo")
        }


        // Settings Submenu: growth buildings
        "ipa_settings_options_allow_growth_buildings".let { prefix ->
            add("$prefix.1.title", "§BAllow growth buildings")
            add("$prefix.1.desc", "")
            add("$prefix.1.clinic_allowed", "§GAllow §BOrganic Clinic")
            add("$prefix.1.clinic_forbidden", "§RForbid §BOrganic Clinic")
            add("$prefix.1.hospital_allowed", "§GAllow §BHospital")
            add("$prefix.1.hospital_forbidden", "§RForbid §BHospital")
            add("$prefix.1.robot_assembly_allowed", "§GAllow §BRobot Assembly")
            add("$prefix.1.robot_assembly_forbidden", "§RForbid §BRobot Assembly")
            add("$prefix.1.clone_vats_allowed", "§GAllow §BClone Vats")
            add("$prefix.1.clone_vats_forbidden", "§RForbid §BClone Vats")
            add("$prefix.1.machine_assembly_allowed", "§GAllow §BMachine Assembly Planet")
            add("$prefix.1.machine_assembly_forbidden", "§RForbid §BMachine Assembly Planet")
            add("$prefix.1.hive_spawning_pool_allowed", "§GAllow §BHive Spawning Pool")
            add("$prefix.1.hive_spawning_pool_forbidden", "§RForbid §BHive Spawning Pool")
            add("$prefix.1.necroid_elevation_allowed", "§GAllow §BNecroid Elevation")
            add("$prefix.1.necroid_elevation_forbidden", "§RForbid §BNecroid Elevation")
            add("$prefix.1.back", "§RBack")
        }


        // Settings Submenu: settings upgrade min free slots
        "ipa_settings_options_upgrade_min_free_slots".let { prefix ->
            add("$prefix.1.title", "§BBuilding upgrade min free slots")
            add(
                "$prefix.1.desc",
                "Require a specified amount of free slots before upgrading production buildings (factory, foundry, research labs). This conserves strategic resources while there is no need to expend them. Buildings will always be upgraded if there are no more free slots available on the planet and more jobs are required."
            )
            for (i in 0..10) {
                add("$prefix.1.option$i.current", "§GRequire $i free slots")
                add("$prefix.1.option$i.change", "§BRequire $i free slots")
            }
            add("$prefix.1.back", "§RBack")
        }


        // Settings Submenu: pre-build jobs
        "ipa_settings_options_pre_build_jobs".let { prefix ->
            add(
                "$prefix.1.title", "§Bre-build jobs"
            )
            add(
                "$prefix.1.desc",
                "§RWARNING: §BThere is currently no way to check for disabled jobs. The Stellaris trigger free_jobs includes disabled jobs which means that you have to set this setting higher if you frequently disable jobs (for example in gestalt machine empires where you almost always disable maintenance jobs). I do not have a good workaround for this problem yet." +
                    "\\n" + "§REXPERIMENTAL: §BAttempt to pre-build a specified amount of jobs. This setting can be used in growth/resettlement heavy empires or when assimilating and transferring pops to your worlds from conquest to better cope with new pops and reduce the time spent in unemployment." +
                    "\\n" + "0 pre-build jobs will result in new jobs being created once a unemployed pop is present on a planet." +
                    "\\n" + "1 pre-build job will result in the automation trying to maintain at least 1 free job."
            )
            for (i in IpaSettingsOptionsPreBuildJobs.optionValues) {
                add("$prefix.1.option$i.current", "§GPre-build jobs: $i")
                add("$prefix.1.option$i.change", "§BPre-build jobs: $i")
            }
            add("$prefix.1.back", "§RBack")
        }

        // Settings Submenu: build city for buildings
        "ipa_settings_options_build_city_for_buildings".let { prefix ->
            add(
                "$prefix.1.title", "§BBuild city districts for building slots"
            )
            add(
                "$prefix.1.desc",
                "§RWARNING: §BExperimental feature only available in Strict/Designated experimental sector focus variants."
                    + "\\n Build city districts to create building slots even if enough housing/amenities are satisfied. Might create unbalanced worlds in the longer run that require manual intervention. This feature is in testing and unforeseen problems might occur."
            )

            val keyToTranslation = mutableMapOf(
                IpaBuildCityForBuildings.Options.Disabled.key to "Disabled",
                IpaBuildCityForBuildings.Options.ResearchOnly.key to "Research Designations",
                IpaBuildCityForBuildings.Options.Everywhere.key to "All Designations",
            )

            for ((key, translation) in keyToTranslation) {
                add("$prefix.1.option_$key.current", "§WBuild city for slots: §G$translation")
                add("$prefix.1.option_$key.change", "§WBuild city for slots: §R$translation")
            }

            add("$prefix.1.back", "§RBack")
        }

    }

    fun addStaticModifiersBlockJob() {
        SuppressJobMaintenanceDrone.apply {
            add(
                Name,
                "§BImproved Planet and Sector Automation: §WBlocks a number of Maintenance Drone jobs when they are not needed to improve colony efficiency."
            )
            for (i in 1..Max) {
                add(Prefix + i, "Block Maintenance Drones: $i")
            }
        }

        SuppressJobPatrolDrone.apply {
            add(
                Name,
                "§BImproved Planet and Sector Automation: §WBlocks a number of Patrol Drone jobs when they are not needed to improve colony efficiency."
            )
            for (i in 1..Max) {
                add(Prefix + i, "Block Patrol Drones: $i")
            }
        }

        SuppressJobBioTrophy.apply {
            add(
                Name,
                "§BImproved Planet and Sector Automation: §WBlocks a number of Bio Trophy jobs when they are not needed to allow automation to continue to build other jobs."
            )
            for (i in 1..Max) {
                add(Prefix + i, "Block Bio Trophy Jobs: $i")
            }
        }

        SuppressJobClerk.apply {
            add(
                Name,
                "§BImproved Planet and Sector Automation: §WBlocks a number of Clerk jobs when they are not needed to allow automation to continue to build other jobs."
            )
            for (i in 1..Max) {
                add(Prefix + i, "Block Clerk Jobs: $i")
            }
        }

        SuppressJobEnforcer.apply {
            add(
                Name,
                "§BImproved Planet and Sector Automation: §WBlocks a number of Enforcer jobs when they are not needed to allow automation to continue to build other jobs."
            )
            for (i in 1..Max) {
                add(Prefix + i, "Block Enforcer Jobs: $i")
            }
        }
    }

}
