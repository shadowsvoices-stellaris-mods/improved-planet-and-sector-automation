package ipa.translations

import ipa.ModInfo
import ipa.Policies
import ipa.events.menu.IpaSettingsOptionsPreBuildJobs
import ipa.events.menu.IpaSettingsOptionsUpgradeMinFreeSlots

class TranslationGerman : TranslationEnglish("localisation/german/ipa_l_german.yml", "l_german") {

    override fun addMenuIncomes() {
        super.addMenuIncomes()

        addMenuIncomeTranslation("energy_minimum", "£energy£ §BMinimum §!Energie Einkommen", Policies.incomeSteps)
        addMenuIncomeTranslation("minerals_minimum", "£minerals£ §BMinimum §!Mineralien Einkommen", Policies.incomeSteps)
        addMenuIncomeTranslation("food_minimum", "£food£ §BMinimum §!Nahrung Einkommen", Policies.incomeSteps)
        addMenuIncomeTranslation("food_desired", "£food£ §EGewünschtes §!Nahrung Einkommen", Policies.incomeSteps)
        addMenuIncomeTranslation("alloys_minimum", "£alloys£ §BMinimum §!Legierungen Einkommen", Policies.incomeSteps)
        addMenuIncomeTranslation("alloys_desired", "£alloys£ §EGewünschtes §!Legierungen Einkommen", Policies.incomeSteps)
        addMenuIncomeTranslation(
            "consumer_goods_minimum",
            "£consumer_goods£ §BMinimum §!Konsumgüter Einkommen",
            Policies.incomeSteps
        )
        addMenuIncomeTranslation(
            "consumer_goods_desired",
            "£consumer_goods£ §EGewünschtes §!Konsumgüter Einkommen",
            Policies.incomeSteps
        )
        addMenuIncomeTranslation(
            "strategic_minimum",
            "£volatile_motes£ £exotic_gases£ £rare_crystals£ §BMinimum §!Strategisches Einkommen",
            Policies.incomeStepsStrategic
        )
        addMenuIncomeTranslation(
            "strategic_desired",
            "£volatile_motes£ £exotic_gases£ £rare_crystals£ §EGewünschtes §!Strategisches Einkommen",
            Policies.incomeStepsStrategic
        )
    }

    override fun addMenuIncomeTranslation(eventKey: String, dialogLabel: String, steps: Array<Int>) {
        super.addMenuIncomeTranslation(eventKey, dialogLabel, steps)

        // Main Menu
        add("ipa_settings_main.1.$eventKey.disabled", "$dialogLabel: §RDeaktiviert")
        for (step in steps) {
            add("ipa_settings_main.1.$eventKey.step$step", "$dialogLabel: §G$step")
        }
        add("ipa_settings_main.1.$eventKey.unlimited", "$dialogLabel: §RUnbegrenzt")

        // Sub Menu
        add("ipa_settings_$eventKey.1.title", dialogLabel)
        add("ipa_settings_$eventKey.1.desc", "")
        add("ipa_settings_$eventKey.1.disabled", "$dialogLabel: §RDeaktiviert")
        add("ipa_settings_$eventKey.1.unlimited", "$dialogLabel: §RUnbegrenzt")
        for (step in steps) {
            add("ipa_settings_$eventKey.1.step$step", "$dialogLabel: §G$step")
        }
        add("ipa_settings_$eventKey.1.back", "§RZurück")
    }

    override fun addVarious() {
        super.addVarious()

        // Sector Types
        add("st_designated", "Festgelegter Fokus")
        add("st_designated_desc", "Der Sektor folgt den Kolonie-Zuweisungen und entwickelt den Planeten entsprechend.")

        add("st_mixed", "Gemischter Fokus")
        add(
            "st_mixed_desc",
            "Der Sektor baut auf allen Planeten unabhängig der Kolonie-Zuweisung. Dies ist langfristig gesehen weniger effizient aufgrund des Verlust des Spezialisierungs Bonus kann jedoch zu Begin des Spiels eine ausgewogenere Wirtschaft erzielen."
        )

        add("st_strict", "Strikter Fokus")
        add(
            "st_strict_desc",
            "Der Sektor folgt den Kolonie-Zuweisungen strikt und wird auf Kolonien keine Bergbau/Landwirtschaft/Generator Distrikte gleichzeitig bauen."
        )

        add("st_designated_wip", "Festgelegter Fokus (WIP)")
        add(
            "st_designated_wip_desc",
            "Enthält Änderungen, die eventuell noch in Bearbeitung sind und noch nicht vollständig fertig. Falls dieser Fokus Probleme verursucht bitte melden Sie die Probleme. Falls die Probleme den Spielverlauf stören auf die reguläre nicht-WIP Variante wechseln."
        )

        add("st_strict_wip", "Strikter Fokus (WIP)")
        add(
            "st_strict_wip_desc",
            "Enthält Änderungen, die eventuell noch in Bearbeitung sind und noch nicht vollständig fertig. Falls dieser Fokus Probleme verursucht bitte melden Sie die Probleme. Falls die Probleme den Spielverlauf stören auf die reguläre nicht-WIP Variante wechseln."
        )
    }

    override fun addMenuOptions() {
        super.addMenuOptions()

        // Edicts
        add("edict_ipa_settings_menu", "§BImproved Planet & Sector Automation")

        // Settings Menu
        "ipa_settings_main".let { prefix ->
            add("$prefix.1.title", "Improved Planet and Sector Automation: §Bv${ModInfo.Version}")
            add(
                "$prefix.1.desc",
                "Improved Planet and Sector Automation: §Bv${ModInfo.Version} §!Aktualisiert: ${ModInfo.UpdatedAt}"
            )
            add("$prefix.1.options", "§BOptionen")
            add("$prefix.1.close", "§RSchließen")
        }

        "ipa_settings_options".let { prefix ->
            add("$prefix.1.title", "§BOptionen")
            add("$prefix.1.desc", "")
            add("$prefix.1.back", "§RZurück")
            add("$prefix.1.allow_growth_buildings", "Konfigurieren: Wachstums Gebäude")
            for (i in IpaSettingsOptionsUpgradeMinFreeSlots.optionValues) {
                add("$prefix.1.upgrade_min_free_slots_$i", "Gebäude aufrüsten minimum freie Bauplätze: §G$i")
            }
            for (i in IpaSettingsOptionsPreBuildJobs.optionValues) {
                add("$prefix.1.pre_build_jobs_$i", "Vorgebaute Arbeitsplätze: §G$i")
            }
            add("$prefix.1.ipa_options_allow_upgrade_strongholds_yes", "Aufrüsten von Bollwerken: §GJa")
            add("$prefix.1.ipa_options_allow_upgrade_strongholds_no", "Aufrüsten von Bollwerken: §RNein")
            add("$prefix.1.ipa_options_allow_agri_farm_buildings_yes", "Farm Gebäude auf Landwirtschaft Kolonien: §GJa")
            add("$prefix.1.ipa_options_allow_agri_farm_buildings_no", "Farm Gebäude auf Landwirtschaft Kolonien: §RNein")
            add("$prefix.1.ipa_options_force_show_income_options_yes", "Erzwinge Anzeige Einkommens Optionen: §GJa")
            add("$prefix.1.ipa_options_force_show_income_options_no", "Erzwinge Anzeige Einkommens Optionen: §RNein")
            add("$prefix.1.ipa_options_disallow_unity_yes", "Verbiete Einheits Gebäude: §GJa")
            add("$prefix.1.ipa_options_disallow_unity_no", "Verbiete Einheits Gebäude: §RNein")
        }


        // Settings Submenu: growth buildings
        "ipa_settings_options_allow_growth_buildings".let { prefix ->
            add("$prefix.1.title", "§BAllow growth buildings")
            add("$prefix.1.desc", "")
            add("$prefix.1.clinic_allowed", "§GErlaube §Borganische Kliniken")
            add("$prefix.1.clinic_forbidden", "§RVerbiete §Borganische Kliniken")
            add("$prefix.1.hospital_allowed", "§GErlaube §BKrankenhäuser")
            add("$prefix.1.hospital_forbidden", "§RVerbiete §BKrankenhäuser")
            add("$prefix.1.robot_assembly_allowed", "§GErlaube §BRoboter-Montageanlagen")
            add("$prefix.1.robot_assembly_forbidden", "§RVerbiete §BRoboter-Montageanlagen")
            add("$prefix.1.clone_vats_allowed", "§GErlaube §BClone Vats")
            add("$prefix.1.clone_vats_forbidden", "§RVerbiete §BClone Vats")
            add("$prefix.1.machine_assembly_allowed", "§GErlaube §BMaschinen-Montageanlage")
            add("$prefix.1.machine_assembly_forbidden", "§RVerbiete §BMaschinen-Montageanlage")
            add("$prefix.1.hive_spawning_pool_allowed", "§GErlaube §BHive Spawning Pool")
            add("$prefix.1.hive_spawning_pool_forbidden", "§RVerbiete §BHive Spawning Pool")
            add("$prefix.1.necroid_elevation_allowed", "§GErlaube §BNecroid Elevation")
            add("$prefix.1.necroid_elevation_forbidden", "§RVerbiete §BNecroid Elevation")
            add("$prefix.1.back", "§RZurück")
        }


        // Settings Submenu: settings upgrade min free slots
        "ipa_settings_options_upgrade_min_free_slots".let { prefix ->
            add("$prefix.1.title", "§BGebäude aufrüsten minimum freie Bauplätze")
            add(
                "$prefix.1.desc",
                "Require a specified amount of free slots before upgrading production buildings (factory, foundry, research labs). This conserves strategic resources while there is no need to expend them. Buildings will always be upgraded if there are no more free slots available on the planet and more jobs are required."
            )
            for (i in 0..10) {
                add("$prefix.1.option$i.current", "§GMindestens $i freie Bauplätze")
                add("$prefix.1.option$i.change", "§BRequire $i freie Bauplätze")
            }
            add("$prefix.1.back", "§RZurück")
        }


        // Settings Submenu: pre-build jobs
        "ipa_settings_options_pre_build_jobs".let { prefix ->
            add(
                "$prefix.1.title", "§Vorgebaute Arbeitsplätze"
            )
            add(
                "$prefix.1.desc",
                "§RWARNING: §BThere is currently no way to check for disabled jobs. The Stellaris trigger free_jobs includes disabled jobs which means that you have to set this setting higher if you frequently disable jobs (for example in gestalt machine empires where you almost always disable maintenance jobs). I do not have a good workaround for this problem yet." +
                    "\\n" + "§REXPERIMENTAL: §BAttempt to pre-build a specified amount of jobs. This setting can be used in growth/resettlement heavy empires or when assimilating and transferring pops to your worlds from conquest to better cope with new pops and reduce the time spent in unemployment." +
                    "\\n" + "0 pre-build jobs will result in new jobs being created once a unemployed pop is present on a planet." +
                    "\\n" + "1 pre-build job will result in the automation trying to maintain at least 1 free job."
            )
            for (i in IpaSettingsOptionsPreBuildJobs.optionValues) {
                add("$prefix.1.option$i.current", "§GVorgebaute Arbeitsplätze: $i")
                add("$prefix.1.option$i.change", "§BVorgebaute Arbeitsplätze: $i")
            }
            add("$prefix.1.back", "§RZurück")
        }

    }

}
