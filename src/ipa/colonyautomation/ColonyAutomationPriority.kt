package ipa.colonyautomation

enum class ColonyAutomationPriority(val priority: Int) {
    Prebuild(17),
    Unity(18),
    DefaultReduced(19),
    Default(20),
    UnityMemorials(21),
    Districts(22),
    Strategic(23),
    Industrial(24),
    Research(25),
    NaturalStrategic(26),
    Enhancer(27),
    Population(28),
    Urgent(29),
    Crisis(30);

    val day: Int
        get() {
            return 30 - priority
        }
}
