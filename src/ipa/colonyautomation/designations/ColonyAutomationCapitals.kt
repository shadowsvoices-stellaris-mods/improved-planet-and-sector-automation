package ipa.colonyautomation.designations

import ipa.colonyautomation.ColonyAutomationCategories
import stellaris.colonyautomation.ColonyAutomation
import stellaris.types.Civic
import stellaris.types.District
import stellaris.types.PlanetDesignation

class ColonyAutomationCapital : ColonyAutomation("automate_capital_planet") {
    init {
        category = ColonyAutomationCategories.designation_construction

        available {
            OR {
                 hasDesignation(PlanetDesignation.capital)

            }
        }

        priorityDistricts.add(District.Industrial)
        priorityDistricts.add(District.Generator)
        priorityDistricts.add(District.Mining)
    }
}

class ColonyAutomationCapitalHive : ColonyAutomation("automate_capital_hive_planet") {
    init {
        category = ColonyAutomationCategories.designation_construction

        available {
            hasDesignation(PlanetDesignation.capital_hive)
        }

        priorityDistricts.add(District.Industrial)
        priorityDistricts.add(District.Generator)
        priorityDistricts.add(District.GeneratorUncapped)
        priorityDistricts.add(District.Mining)
        priorityDistricts.add(District.MiningUncapped)
    }
}

class ColonyAutomationCapitalMachine : ColonyAutomation("automate_capital_machine_planet") {
    init {
        category = ColonyAutomationCategories.designation_construction

        available {
            hasDesignation(PlanetDesignation.capital_machine)
        }

        priorityDistricts.add(District.Industrial)
        priorityDistricts.add(District.Generator)
        priorityDistricts.add(District.GeneratorUncapped)
        priorityDistricts.add(District.Mining)
        priorityDistricts.add(District.MiningUncapped)
    }
}

class ColonyAutomationCapitalMachineServitor : ColonyAutomation("automate_capital_machine_servitor_planet") {
    init {
        category = ColonyAutomationCategories.designation_construction

        available {
            hasDesignation(PlanetDesignation.capital_machine)
            exists("owner")
            owner {
                hasValidCivic(Civic.MachineServitor)
            }
        }

        priorityDistricts.add(District.Industrial)
        priorityDistricts.add(District.Generator)
        priorityDistricts.add(District.GeneratorUncapped)
        priorityDistricts.add(District.Mining)
        priorityDistricts.add(District.MiningUncapped)
        priorityDistricts.add(District.Farming)
        priorityDistricts.add(District.FarmingUncapped)
    }
}
