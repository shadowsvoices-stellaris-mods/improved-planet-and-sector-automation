package ipa.colonyautomation.designations

import ipa.colonyautomation.ColonyAutomationCategories
import stellaris.colonyautomation.ColonyAutomation
import stellaris.types.DistrictGroup
import stellaris.types.PlanetDesignationGroup

class ColonyAutomationIndustrialStrict : ColonyAutomation("automate_factory_planet_strict") {
    init {
        category = ColonyAutomationCategories.strict_districts

        available {
            hasDesignationIndustrialAny()
        }

        addPriorityDistrict(DistrictGroup.Industrial)
    }
}

class ColonyAutomationFarmingStrict : ColonyAutomation("automate_farming_planet_strict") {
    init {
        category = ColonyAutomationCategories.strict_districts

        available {
            hasDesignations(PlanetDesignationGroup.Farming)
        }

        addPriorityDistrict(DistrictGroup.Farming)
    }
}

class ColonyAutomationMiningStrict : ColonyAutomation("automate_mining_planet_strict") {
    init {
        category = ColonyAutomationCategories.strict_districts

        available {
            hasDesignations(PlanetDesignationGroup.Mining)
        }

        addPriorityDistrict(DistrictGroup.Mining)
    }
}

class ColonyAutomationGeneratorStrict : ColonyAutomation("automate_generator_planet_strict") {
    init {
        category = ColonyAutomationCategories.strict_districts

        available {
            hasDesignations(PlanetDesignationGroup.Generator)
        }

        addPriorityDistrict(DistrictGroup.Generator)
    }
}

class ColonyAutomationIndustrialMixed : ColonyAutomation("automate_factory_planet_mixed") {
    init {
        category = ColonyAutomationCategories.mixed_districts

        available {
            hasDesignationIndustrialAny()
        }

        addPriorityDistrict(DistrictGroup.Industrial)
        addPriorityDistrict(DistrictGroup.Generator)
        addPriorityDistrict(DistrictGroup.Mining)
        addPriorityDistrict(DistrictGroup.Farming)
    }
}

class ColonyAutomationFarmingMixed : ColonyAutomation("automate_farming_planet_mixed") {
    init {
        category = ColonyAutomationCategories.mixed_districts

        available {
            hasDesignations(PlanetDesignationGroup.Farming)
        }

        addPriorityDistrict(DistrictGroup.Farming)
        addPriorityDistrict(DistrictGroup.Generator)
        addPriorityDistrict(DistrictGroup.Mining)
        addPriorityDistrict(DistrictGroup.Industrial)
    }
}

class ColonyAutomationMiningMixed : ColonyAutomation("automate_mining_planet_mixed") {
    init {
        category = ColonyAutomationCategories.mixed_districts

        available {
            hasDesignations(PlanetDesignationGroup.Mining)
        }

        addPriorityDistrict(DistrictGroup.Mining)
        addPriorityDistrict(DistrictGroup.Generator)
        addPriorityDistrict(DistrictGroup.Industrial)
        addPriorityDistrict(DistrictGroup.Farming)
    }
}

class ColonyAutomationGeneratorMixed : ColonyAutomation("automate_generator_planet_mixed") {
    init {
        category = ColonyAutomationCategories.mixed_districts

        available {
            hasDesignations(PlanetDesignationGroup.Generator)
        }

        addPriorityDistrict(DistrictGroup.Generator)
        addPriorityDistrict(DistrictGroup.Mining)
        addPriorityDistrict(DistrictGroup.Industrial)
        addPriorityDistrict(DistrictGroup.Farming)
    }
}
