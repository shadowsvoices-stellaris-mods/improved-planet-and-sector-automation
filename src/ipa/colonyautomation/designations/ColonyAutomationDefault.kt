package ipa.colonyautomation.designations

import ipa.colonyautomation.ColonyAutomationCategories
import stellaris.colonyautomation.ColonyAutomation
import stellaris.types.District
import stellaris.types.DistrictGroup
import stellaris.types.PlanetDesignation

class ColonyAutomationDefault : ColonyAutomation("automate_colony_planet") {
    init {
        category = ColonyAutomationCategories.designation_construction

        available {
            hasDesignation(PlanetDesignation.default)
        }

        addPriorityDistrict(District.Industrial)
        addPriorityDistrict(District.Generator)
        addPriorityDistrict(District.GeneratorUncapped)
        addPriorityDistrict(District.Mining)
        addPriorityDistrict(District.MiningUncapped)
        addPriorityDistrict(District.FarmingUncapped)
    }
}

class ColonyAutomationDefaultMachine : ColonyAutomation("automate_colony_machine_planet") {
    init {
        category = ColonyAutomationCategories.designation_construction

        available {
            hasDesignation(PlanetDesignation.default_machine)
        }

        addPriorityDistrict(DistrictGroup.Industrial)
        addPriorityDistrict(DistrictGroup.Generator)
        addPriorityDistrict(DistrictGroup.Mining)
        addPriorityDistrict(DistrictGroup.Farming)
    }
}
