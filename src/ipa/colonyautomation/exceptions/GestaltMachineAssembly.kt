package ipa.colonyautomation.exceptions

import ipa.colonyautomation.ColonyAutomationCategories
import stellaris.colonyautomation.ColonyAutomationException
import stellaris.types.Building

class GestaltMachineAssembly : ColonyAutomationException("automate_machine_assembly") {
    init {
        category = ColonyAutomationCategories.pop_assembly

        available {
            owner {
                isMachineEmpire = true
            }
        }

        building(Building.machine_assembly_plant) {}
    }
}
