package ipa.colonyautomation.exceptions

import ipa.colonyautomation.ColonyAutomationPriority
import stellaris.colonyautomation.ColonyAutomationException
import stellaris.logic.Comparators
import stellaris.types.Building
import stellaris.types.DistrictGroup
import stellaris.types.Job

class GestaltMachineAmenities : ColonyAutomationException("automate_gestalt_amenities") {
    init {
        category = "ipa_planet_automation_amenities_custom"

        available {
            owner {
                OR {
                    hasAuthority("auth_machine_intelligence")
                    hasAuthority("auth_hive_mind")
                }
            }
            OR {
                AND {
                    hasColonyAutomationPriority(ColonyAutomationPriority.Prebuild)
                    freeAmenities(Comparators.LTEQ, 7)
                }
                // <= 5 urgent
                AND {
                    hasColonyAutomationPriority(ColonyAutomationPriority.Urgent)
                    freeAmenities(Comparators.LTEQ, 5)
                }
                // <= 1 crisis
                AND {
                    hasColonyAutomationPriority(ColonyAutomationPriority.Crisis)
                    freeAmenities(Comparators.LTEQ, 1)
                }
            }
        }

        // building(Building.maintenance_depot) {}

        for (district in DistrictGroup.Housing) {
            priorityDistricts.add(district)
        }

    }
}

class GestaltMachineAmenitiesJobs : ColonyAutomationException("automate_gestalt_amenities_jobs") {
    init {
        category = "ipa_planet_automation_amenities_custom"

        available {
            owner {
                isGestalt = true
            }
        }

        building(Building.maintenance_depot) {
            available {
                always(false)
            }
        }

        jobChange(Job.MaintenanceDrone, -1) {
            available {
                AND {
                    freeAmenities(Comparators.GTEQ, 12)

                    // only allow removing maintenance drone jobs when no unemployment is possible
                    // TODO improve this check to consider strata
                    numUnemployed(Comparators.EQ, 0)
                    //NOT {
                    //    anyOwnedPop {
                    //        isUnemployed(false)
                    //    }
                    //}
                    freeJobs(Comparators.GTEQ, 1)
                }
            }
        }

        jobChange(Job.MaintenanceDrone, 1) {
            available {
                freeAmenities(Comparators.LTEQ, 6)
            }
        }

    }
}
