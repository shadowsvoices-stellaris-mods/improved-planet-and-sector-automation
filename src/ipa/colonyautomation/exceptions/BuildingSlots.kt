package ipa.colonyautomation.exceptions

import ipa.colonyautomation.ColonyAutomationCategories
import stellaris.colonyautomation.ColonyAutomationException
import stellaris.logic.Comparators
import stellaris.types.DistrictGroup

class BuildingSlots : ColonyAutomationException("automate_building_slots") {
    init {
        category = ColonyAutomationCategories.building_slot

        available {
            freeBuildingSlots(Comparators.LT, 1)
            // TODO find better way to prevent massive overbuilding of housing
            freeHousing(Comparators.LTEQ, 30)
            // TODO find better way to count absolute number of building slots, look at mods adding more slots for the modifier and see if it can be used to determine the upper limit dynamically
            // numBuildings("any", Comparators.LT, 12)
        }

        addPriorityDistrict(DistrictGroup.Housing)
    }
}

