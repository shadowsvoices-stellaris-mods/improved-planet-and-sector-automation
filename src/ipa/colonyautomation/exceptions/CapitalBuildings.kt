package ipa.colonyautomation.exceptions

import ipa.colonyautomation.ColonyAutomationCategories
import stellaris.colonyautomation.ColonyAutomationException
import stellaris.types.Building
import stellaris.types.Resource

class CapitalBuildings : ColonyAutomationException("automate_capital_buildings") {
    init {
        category = ColonyAutomationCategories.upgrade_buildings

        available {
            exists("owner")
            owner {
                checkMissingIncome(Resource.Energy, false)
            }
        }

        building(Building.capital) {
            available {
                owner {
                    checkMissingIncome(Resource.ConsumerGoods,false)
                }
            }
            upgradeTrigger {
                owner {
                    checkMissingIncome(Resource.ConsumerGoods,false)
                }
            }
        }

        building(Building.hive_capital) {
            available {
                owner {
                    checkMissingIncome(Resource.Food,false)
                }
            }
            upgradeTrigger {
                owner {
                    checkMissingIncome(Resource.Food,false)
                }
            }
        }

        building(Building.machine_capital) {
            available {
                owner {
                    checkMissingIncome(Resource.Alloys,false)
                }
            }
            upgradeTrigger {
                owner {
                    checkMissingIncome(Resource.Alloys,false)
                }
            }
        }

    }
}

