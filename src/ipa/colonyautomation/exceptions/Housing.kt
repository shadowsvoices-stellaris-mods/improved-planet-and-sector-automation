package ipa.colonyautomation.exceptions

import ipa.colonyautomation.ColonyAutomationCategories
import stellaris.colonyautomation.ColonyAutomationException
import stellaris.logic.Comparators
import stellaris.types.DistrictGroup

class HousingEmergency : ColonyAutomationException("automate_housing_emergency") {
    init {
        category = ColonyAutomationCategories.housing
        emergency = true

        available {
            freeHousing(Comparators.LTEQ, 0)
        }

        addPriorityDistrict(DistrictGroup.Housing)
    }
}

class HousingPrebuild : ColonyAutomationException("automate_housing_prebuild") {
    init {
        category = ColonyAutomationCategories.housing
        emergency = false

        available {
            OR {
                AND {
                    freeHousing(Comparators.LTEQ, 5)
                    freeDistrictSlots(Comparators.GT, 1)
                }
                AND {
                    freeHousing(Comparators.LTEQ, 2)
                    freeDistrictSlots(Comparators.LTEQ, 1)
                }
            }
        }

        addPriorityDistrict(DistrictGroup.Housing)
    }
}

