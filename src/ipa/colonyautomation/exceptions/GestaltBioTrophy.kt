package ipa.colonyautomation.exceptions

import ipa.colonyautomation.ColonyAutomationCategories
import stellaris.colonyautomation.ColonyAutomationException
import stellaris.types.Building
import stellaris.types.CitizenshipType
import stellaris.types.Job

class GestaltBioTrophy : ColonyAutomationException("automate_gestalt_bio_trophy") {
    init {
        category = ColonyAutomationCategories.biotrophy_management

        available {
            owner {
                hasAuthority("auth_machine_intelligence")
                hasValidCivic("civic_machine_servitor")
            }
            // freeBuildingSlots(Comparators.GT, 1)
            anyOwnedPop {
                isUnemployed(true)
                hasCitizenshipType(CitizenshipType.OrganicTrophy)
            }
        }

        building(Building.organic_sanctuary) {
            available {
                NOT { hasAvailableJobs(Job.BioTrophy) }
                NOT { hasForbiddenJobs(Job.BioTrophy) }
            }
        }

        //jobChange(Job.BioTrophy, -1) {
        //    available {
        //    }
        //}
        //
        jobChange(Job.BioTrophy, 1) {
            available {
                AND {
                    anyOwnedPop {
                        isUnemployed(true)
                        hasCitizenshipType(CitizenshipType.OrganicTrophy)
                    }
                }
            }
        }

    }
}

