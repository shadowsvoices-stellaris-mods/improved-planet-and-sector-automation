package ipa.colonyautomation.exceptions

import ipa.colonyautomation.ColonyAutomationCategories
import stellaris.colonyautomation.ColonyAutomationException
import stellaris.logic.Comparators
import stellaris.types.Building
import stellaris.types.District
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Resource

class ResearchAutomation : ColonyAutomationException("automate_research") {
    init {
        emergency = false

        val onlyOnMatchingDesignation = true
        val notOnSpecializedWorlds = true

        category = ColonyAutomationCategories.research

        // for (building in arrayOf(Building.research_lab_1, Building.research_lab_2, Building.research_lab_3)) {
        for (building in arrayOf(Building.research_lab_1)) {
            building(building) {
                available {
                    // when jobs are needed
                    needMoreJobsCheck(true)

                    if (onlyOnMatchingDesignation) {
                        OR {
                            comment = "exclude non-research worlds"
                            hasDesignations(PlanetDesignationGroup.Research)
                            hasDesignations(PlanetDesignationGroup.Capital)
                        }
                    } else if (notOnSpecializedWorlds) {
                        NOR {
                            comment = "exclude specific production worlds"
                            hasDesignations(PlanetDesignationGroup.Factory)
                            hasDesignations(PlanetDesignationGroup.Refinery)
                            hasDesignations(PlanetDesignationGroup.Foundry)
                        }
                    }

                    NAND {
                        comment = "prevent construction of research labs while there are free research districts"
                        isPlanetClassHabitat()
                        numFreeDistricts(District.HabitatResearch, Comparators.GTEQ, 1)
                    }

                    NAND {
                        comment = "prevent construction of research labs while there are free research districts"
                        isPlanetClassRingworld()
                        numFreeDistricts(District.RingworldResearch, Comparators.GTEQ, 1)
                    }

                    NAND {
                        comment = "prevent construction of research labs while there are free research districts"
                        isPlanetClass("pc_giga_planetary_computer")
                        numFreeDistricts(District.giga_planet_science, Comparators.GTEQ, 1)
                    }

                    NOT {
                        comment = "not missing income"
                        //OR {
                        //    comment = "prevent construction of research labs while there are free research districts"
                        //    for (district in DistrictGroup.Research) {
                        //        numFreeDistricts(district, Comparators.GTEQ, 1)
                        //    }
                        //}

                        owner {
                            checkMissingIncome(Resource.Energy)
                            if (building != Building.research_lab_1) checkMissingIncome(Resource.ExoticGases, true)
                        }

                        addResearchRequirements()
                    }
                }
                upgradeTrigger {
                    upgradeMinFreeSlotsAllowed(true)
                }
            }
        }

        //for (building in arrayOf(Building.supercomputer, Building.institute)) {
        //    building(building) {
        //        available {
        //            NOT {
        //                comment = "Disallow when not research designation or to many free jobs"
        //                disableIfNoNeedForJobs()
        //
        //                if (onlyOnMatchingDesignation) {
        //                    NOR {
        //                        comment = "exclude non-research worlds"
        //                        hasDesignations(PlanetDesignationGroup.Research)
        //                        hasDesignations(PlanetDesignationGroup.Capital)
        //                    }
        //                } else if (notOnSpecializedWorlds) {
        //                    OR {
        //                        comment = "exclude specific production worlds"
        //                        hasDesignations(PlanetDesignationGroup.Factory)
        //                        hasDesignations(PlanetDesignationGroup.Refinery)
        //                        hasDesignations(PlanetDesignationGroup.Foundry)
        //                    }
        //                }
        //
        //                owner {
        //                    checkMissingIncome(Resource.Energy)
        //                }
        //
        //                AND {
        //                    comment = "disallow if no research buildings"
        //                    numBuildings(Building.research_lab_1, Comparators.EQ, 0)
        //                    numBuildings(Building.research_lab_2, Comparators.EQ, 0)
        //                    numBuildings(Building.research_lab_3, Comparators.EQ, 0)
        //                }
        //
        //                addResearchRequirements()
        //            }
        //        }
        //    }
        //}


    }
}

