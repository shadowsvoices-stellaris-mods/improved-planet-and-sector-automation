package ipa.colonyautomation.exceptions

import ipa.colonyautomation.ColonyAutomationCategories
import ipa.colonyautomation.ColonyAutomationPriority
import stellaris.colonyautomation.ColonyAutomationException
import stellaris.logic.Comparators
import stellaris.types.Building
import stellaris.types.Job

class GestaltMachineCrime : ColonyAutomationException("automate_gestalt_crime") {
    init {
        category = ColonyAutomationCategories.crime

        available {
            owner {
                isGestalt = true
            }
        }

        building(Building.sentinel_posts) {
            available {
                NOT { hasAvailableJobs(Job.PatrolDrone) }
                NOT { hasForbiddenJobs(Job.PatrolDrone) }

                OR {
                    // >= 20 urgent
                    AND {
                        hasColonyAutomationPriority(ColonyAutomationPriority.Urgent)
                        planetCrime(Comparators.GT, 21)
                    }
                    // >= 30 urgent
                    AND {
                        hasColonyAutomationPriority(ColonyAutomationPriority.Crisis)
                        planetCrime(Comparators.GT, 30)
                    }
                }
            }
        }

        jobChange(Job.PatrolDrone, -1) {
            available {
                planetCrime(Comparators.LTEQ, 0)
            }
        }

        jobChange(Job.PatrolDrone, 1) {
            available {
                planetCrime(Comparators.GT, 20)
            }
        }

    }
}

