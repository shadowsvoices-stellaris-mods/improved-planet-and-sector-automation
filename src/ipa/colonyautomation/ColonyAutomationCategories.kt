package ipa.colonyautomation

import stellaris.colonyautomation.ColonyAutomationCategory
import stellaris.template.TemplateFile

class ColonyAutomationCategories : TemplateFile("common/colony_automation_categories/00_categories.txt") {

    companion object {
        const val deficit_restriction = "planet_automation_deficit_restriction"
        const val upgrade_buildings = "planet_automation_upgrade_buildings"
        const val amenities = "planet_automation_amenities"
        const val amenities_custom = "ipa_planet_automation_amenities_custom"
        const val crime = "planet_automation_crime"
        const val pop_assembly = "planet_automation_pop_assembly"
        const val housing = "planet_automation_housing"
        const val building_slot = "planet_automation_building_slot"
        const val clear_blockers = "planet_automation_clear_blockers"
        const val research = "planet_automation_research"
        const val designation_construction = "planet_automation_designation_construction"
        const val strict_districts = "ipa_planet_automation_strict_districts"
        const val mixed_districts = "ipa_planet_automation_mixed_districts"
        const val rare_resources = "planet_automation_rare_resources"
        const val biotrophy_management = "planet_automation_biotrophy_management"
        const val worm = "planet_automation_worm"
        const val permanent_employment_center = "planet_automation_permanent_employment_center"
        const val psi_corps_building = "planet_automation_psi_corps_building"
    }

    init {

        addBlock(ColonyAutomationCategory(deficit_restriction))
        addBlock(ColonyAutomationCategory(upgrade_buildings))
        // don't use this one, it triggers paradox hardcoded update loop for amenities jobs which is buggy and idempotent
        addBlock(ColonyAutomationCategory(amenities))
        addBlock(ColonyAutomationCategory(amenities_custom))
        addBlock(ColonyAutomationCategory(crime))
        addBlock(ColonyAutomationCategory(housing))
        addBlock(ColonyAutomationCategory(pop_assembly))
        addBlock(ColonyAutomationCategory(building_slot))
        addBlock(ColonyAutomationCategory(clear_blockers))

        addBlock(ColonyAutomationCategory(designation_construction))
        addBlock(ColonyAutomationCategory(strict_districts))
        addBlock(ColonyAutomationCategory(mixed_districts))

        addBlock(ColonyAutomationCategory(research))
        addBlock(ColonyAutomationCategory(rare_resources))

        addBlock(ColonyAutomationCategory(biotrophy_management)) {
            available {
                exists("owner")
                owner {
                    hasAuthority("auth_machine_intelligence")
                    hasValidCivic("civic_machine_servitor")
                }
            }
        }

        addBlock(ColonyAutomationCategory(worm)) {
            available {
                exists("owner")
                owner {
                    hasTechnology("tech_akx_worm_3")
                    hasEventChain("WORM_CHAIN_1")
                    NOT { hasCountryFlag("alignment_built") }
                }
            }
        }

        addBlock(ColonyAutomationCategory(permanent_employment_center)) {
            available {
                exists("owner")
                owner {
                    hasValidCivic("civic_permanent_employment")
                }
            }
        }

        addBlock(ColonyAutomationCategory(psi_corps_building)) {
            available {
                exists("owner")
                owner {
                    OR {
                        hasOrigin("tech_akx_worm_3")
                        hasAscensionPerk("origin_shroudwalker_apprentice")
                    }
                }
            }
        }

    }
}
