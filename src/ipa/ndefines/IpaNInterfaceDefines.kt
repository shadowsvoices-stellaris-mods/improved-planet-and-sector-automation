package ipa.ndefines

import stellaris.various.DefinesBlock

class IpaNInterfaceDefines : DefinesBlock(Name) {

    companion object {
        const val Name = "NInterface"
    }

    init {
        addDefine(
            "AUTOMATION_RESOURCE_ALERT_MIN_POOL",
            2000,
            "Alert the player if pool is lower than this AND monthly transfer lower than the next entry, default is 300"
        )
        addDefine(
            "AUTOMATION_RESOURCE_ALERT_MIN_MONTHLY",
            100,
            "Alert the player if pool is previous entry AND monthly transfer lower than this, default is 10"
        )
    }

}

