package ipa.ndefines

import stellaris.various.DefinesBlock
class IpaNAIDefines : DefinesBlock(Name) {

    companion object {
        const val Name = "NAI"
    }

    init {
        addDefine(
            "COLONY_AUTOMATION_COOLDOWN",
            3,
            "Number of days automation will not be done after a build queue item on a planet completes"
        )
        addDefine(
            "COLONY_AUTOMATION_DISTRICT_PREFERENCE",
            1.5,
            "Prefer to build districts over buildings unless there are multiple building slots open or this many times as many districts as buildings, minimum 1"
        )
    }

}

