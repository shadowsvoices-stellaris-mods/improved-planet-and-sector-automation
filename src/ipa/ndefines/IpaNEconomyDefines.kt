package ipa.ndefines

import stellaris.various.DefinesBlock

class IpaNEconomyDefines : DefinesBlock(Name) {

    companion object {
        const val Name = "NEconomy"
    }

    init {
        addDefine(
            "SECTOR_AUTOMATION_CONTRUCTION_RAND_FACTOR",
            0,
            "Random factor applied to the score of constructions in the sector automation, default is 0.1"
        )
        addDefine(
            "SECTOR_AUTOMATION_FREE_JOBS_BUILD_CAP",
            20,
            "Sector automation will build if there is unemployment OR free jobs <= SECTOR_AUTOMATION_FREE_JOBS_BUILD_CAP OR free housing <= SECTOR_AUTOMATION_FREE_HOUSING_BUILD_CAP, default is 0"
        )
        addDefine(
            "SECTOR_AUTOMATION_FREE_HOUSING_BUILD_CAP",
            20,
            "Sector automation will build if there is unemployment OR free jobs <= SECTOR_AUTOMATION_FREE_JOBS_BUILD_CAP OR free housing <= SECTOR_AUTOMATION_FREE_HOUSING_BUILD_CAP, default is 1"
        )
        addDefine(
            "SECTOR_AUTOMATION_BASE_FOCUS",
            "basic",
            "Items in this sector focus list are appended to all other ones"
        )
    }

}

