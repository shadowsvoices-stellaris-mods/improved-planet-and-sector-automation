package ipa.scriptedtriggers

import stellaris.logic.Comparators
import stellaris.logic.PlanetaryScriptedTrigger
import ipa.events.menu.IpaSettingsOptionsPreBuildJobs
import stellaris.types.Variables

class IpaNeedMoreJobsCheck : PlanetaryScriptedTrigger(Name) {

    companion object {
        const val Name = "ipa_need_more_jobs_check"
    }

    init {
        OR {
            comment = "always true if missing jobs"
            numUnemployed(Comparators.GTEQ, 1)
            // conditions.add("has_unemployed_or_servants = no")

            for (i in IpaSettingsOptionsPreBuildJobs.optionValues) {
                if (i == 0) continue
                AND {
                    comment = "pre-build jobs set to $i and less than $i jobs available"
                    owner {
                        checkVariable(Variables.OptionsPreBuildJobs, Comparators.EQ, i)
                    }
                    freeJobs(Comparators.LT, i)
                }
            }
        }
    }

}

