package ipa.scriptedtriggers

import stellaris.logic.Comparators
import stellaris.logic.PlanetaryScriptedTrigger
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Variables

class IpaBuildCityForBuildings : PlanetaryScriptedTrigger(Name) {

    enum class Options(val value: Int, val key: String) {
        Disabled(0, "disabled"),
        ResearchOnly(1, "research_only"),
        Everywhere(999, "everywhere"),
    }

    companion object {
        const val Name = "ipa_build_city_for_buildings"
    }

    init {
        OR {

            AND {
                owner {
                    checkVariable(Variables.OptionsBuildCityForBuildings, Comparators.EQ, Options.ResearchOnly.value)
                }
                freeBuildingSlots(Comparators.LT, 1)
                OR {
                    hasDesignations(PlanetDesignationGroup.Research)
                    hasDesignations(PlanetDesignationGroup.Capital)
                }
            }

            AND {
                owner {
                    checkVariable(Variables.OptionsBuildCityForBuildings, Comparators.EQ, Options.Everywhere.value)
                }
                freeBuildingSlots(Comparators.LT, 1)
            }
        }
    }

}

