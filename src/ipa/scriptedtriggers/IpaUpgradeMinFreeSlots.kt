package ipa.scriptedtriggers

import stellaris.types.Variables
import stellaris.logic.Comparators
import stellaris.logic.PlanetaryScriptedTrigger

class IpaUpgradeMinFreeSlots : PlanetaryScriptedTrigger(Name) {

    companion object {
        const val Name = "ipa_upgrade_min_free_slots"
    }

    init {
        OR {
            // always true if no more free slots are available
            freeBuildingSlots(Comparators.EQ, 0)

            for (i in 1..10) {
                AND {
                    owner {
                        checkVariable(Variables.OptionsUpgradeMinFreeSlots, Comparators.EQ, i)
                    }
                    freeBuildingSlots(Comparators.GTEQ, i)
                }
            }
        }
    }

}

