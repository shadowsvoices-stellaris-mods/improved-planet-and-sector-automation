package ipa.scriptedtriggers

import stellaris.types.Variables
import stellaris.logic.Comparators
import stellaris.logic.CountryScriptedTrigger

class IpaAllowGrowthClinic : CountryScriptedTrigger("ipa_allow_growth_clinic") {
    init {
        OR {
            checkVariable(Variables.OptionsAllowGrowthClinic, Comparators.EQ, 0)
        }
    }
}

class IpaAllowGrowthHospital : CountryScriptedTrigger("ipa_allow_growth_hospital") {
    init {
        OR {
            checkVariable(Variables.OptionsAllowGrowthHospital, Comparators.EQ, 0)
        }
    }
}

class IpaAllowGrowthRobotAssembly : CountryScriptedTrigger("ipa_allow_growth_robot_assembly") {
    init {
        OR {
            checkVariable(Variables.OptionsAllowGrowthRobotAssembly, Comparators.EQ, 0)
        }
    }
}

class IpaAllowGrowthCloneVats : CountryScriptedTrigger("ipa_allow_growth_clone_vats") {
    init {
        OR {
            checkVariable(Variables.OptionsAllowGrowthCloneVats, Comparators.EQ, 0)
        }
    }
}

class IpaAllowGrowthMachineAssembly : CountryScriptedTrigger("ipa_allow_growth_machine_assembly") {
    init {
        OR {
            checkVariable(Variables.OptionsAllowGrowthMachineAssembly, Comparators.EQ, 0)
        }
    }
}

class IpaAllowGrowthHiveSpawningPool : CountryScriptedTrigger("ipa_allow_growth_hive_spawning_pool") {
    init {
        OR {
            checkVariable(Variables.OptionsAllowGrowthHiveSpawningPool, Comparators.EQ, 0)
        }
    }
}

class IpaAllowGrowthNecroidElevation : CountryScriptedTrigger("ipa_allow_growth_necroid_elevation") {
    init {
        OR {
            checkVariable(Variables.OptionsAllowGrowthNecroidElevation, Comparators.EQ, 0)
        }
    }
}



