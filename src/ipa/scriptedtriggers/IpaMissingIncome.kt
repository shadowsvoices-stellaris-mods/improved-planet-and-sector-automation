package ipa.scriptedtriggers

import stellaris.template.TemplateFile
import stellaris.logic.CountryScriptedTrigger
import stellaris.types.Resource

class IpaMissingIncomes(target: String) : TemplateFile(target) {
    init {
        for (resource in Resource.values()) {

            if (resource.varIncomeMinimum != null) {
                addBlock(CountryScriptedTrigger(resource.triggerMissingIncome)) {
                    addMinimumIncome(resource, resource.varIncomeMinimum, resource.incomeSteps)
                }
            }

            if (resource.varIncomeDesired != null) {
                addBlock(CountryScriptedTrigger(resource.triggerMissingDesiredIncome)) {
                    addDesiredIncome(resource, resource.varIncomeDesired, resource.incomeSteps)
                }
            }

        }
    }
}
