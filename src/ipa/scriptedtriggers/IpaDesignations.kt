package ipa.scriptedtriggers

import stellaris.template.TemplateFile
import stellaris.logic.PlanetaryScriptedTrigger
import stellaris.types.PlanetDesignationGroup


class IpaHasDesignations(target: String) : TemplateFile(target) {
    init {
        for (group in PlanetDesignationGroup.values()) {
            addBlock(PlanetaryScriptedTrigger(group.triggerName)) {
                OR {
                    group.forEach(::hasDesignation)
                }
            }
        }
    }
}

