package ipa.scriptedtriggers

import stellaris.logic.CountryScriptedTrigger

class IpaCountryUsesFood : CountryScriptedTrigger("ipa_country_uses_food") {

    init {
        NOR {
            AND {
                hasAuthority("auth_machine_intelligence")
                NOR {
                    hasValidCivic("civic_machine_servitor")
                    hasValidCivic("civic_machine_assimilator")
                }
            }
            hasCountryFlag("synthetic_empire")
            isLithoid = true
        }
    }

}
