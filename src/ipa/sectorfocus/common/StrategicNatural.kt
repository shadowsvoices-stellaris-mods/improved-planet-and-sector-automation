package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.Resource

fun SectorFocus.addCommonStrategicNatural() {

    addBuilding(Building.mote_harvesters, priorities.naturalStrategic, priority = true) {
        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkMissingIncome(Resource.VolatileMotes)
            }
        }
        modifierOR(priorities.disable) {
            comment = "Disallow if to many free jobs"
            disableIfNoNeedForJobs()
            numPops(Comparators.LT, 10) // dont build natural strategic early on
        }
    }

    addBuilding(Building.gas_extractors, priorities.naturalStrategic, priority = true) {
        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkMissingIncome(Resource.ExoticGases)
            }
        }
        modifierOR(priorities.disable) {
            comment = "Disallow if to many free jobs"
            disableIfNoNeedForJobs()
            numPops(Comparators.LT, 10) // dont build natural strategic early on
        }
    }

    addBuilding(Building.crystal_mines, priorities.naturalStrategic, priority = true) {
        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkMissingIncome(Resource.RareCrystals)
            }
        }
        modifierOR(priorities.disable) {
            comment = "Disallow if to many free jobs"
            disableIfNoNeedForJobs()
            numPops(Comparators.LT, 10) // dont build natural strategic early on
        }
    }

}
