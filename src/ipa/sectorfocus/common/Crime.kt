package ipa.sectorfocus.common

import ipa.scriptValues.JobCounts
import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.BuildingGroup
import stellaris.types.Job

fun SectorFocus.addCommonCrime() {

    // anti-crime T1
    for (building in BuildingGroup.CrimeT1) {
        addBuilding(building, priorities.base, priority = true) {

            modifierOR(priorities.prebuild / priorities.base) {
                comment = "Pre-build when approaching threshold"
                planetCrime(Comparators.LT, 7)
            }

            modifierAND(priorities.urgent / priorities.base) {
                comment = "Increase priority when approaching threshold"
                planetCrime(Comparators.GTEQ, 8)
                planetCrime(Comparators.LT, 15)
            }

            modifierOR(priorities.crisis / priorities.base) {
                comment = "Increase to crisis status if this gets out of hand"
                planetCrime(Comparators.GTEQ, 15)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when no crime exists"
                planetCrime(Comparators.LT, 7)

                when (building) {
                    Building.sentinel_posts -> {
                        hasAvailableJobs(Job.PatrolDrone)
                        checkVariableArithmetic("value:" + JobCounts.PatrolDrone.Suppressed, Comparators.GTEQ, 1)
                    }

                    Building.precinct_house -> {
                        hasAvailableJobs(Job.Clerk)
                        checkVariableArithmetic("value:" + JobCounts.Clerk.Suppressed, Comparators.GTEQ, 1)
                    }

                    else -> {}
                }

                AND {
                    comment = "Disallow when there is a worker shortage and crime is not yet at problematic levels"
                    freeJobs(Comparators.GT, 3)
                    planetCrime(Comparators.LTEQ, 10)
                }
            }
        }
    }

}
