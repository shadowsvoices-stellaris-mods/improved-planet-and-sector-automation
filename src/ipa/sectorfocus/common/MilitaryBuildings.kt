package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Resource
import stellaris.types.Variables

fun SectorFocus.addCommonMilitaryBuildings() {

    addBuilding(Building.stronghold, priorities.default) {
        modifierOR(priorities.disable) {
            hasDesignations(PlanetDesignationGroup.Fortress, false) // forbidden on non-fortress worlds
            disableIfNoNeedForJobs()
            controller {
                checkMissingIncome(Resource.Energy)
            }
        }
    }

    addBuilding(Building.fortress, priorities.defaultReduced) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
            upgradeMinFreeSlotsAllowed(false)
            controller {
                checkMissingIncome(Resource.VolatileMotes)
            }
            owner {
                checkVariable(Variables.OptionsAllowUpgradeStronghold, Comparators.EQ, 0)
            }
        }
    }

}
