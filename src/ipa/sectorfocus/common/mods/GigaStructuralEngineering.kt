package ipa.sectorfocus.common.mods

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.PlanetDesignationGroup

fun SectorFocus.addGigaStructuralEngineering(
    onlyOnMatchingDesignation: Boolean = true,
    notOnSpecializedWorlds: Boolean = true
) {

    // Shroud Capacitor
    addBuilding(
        Building.giga_shroud_capacitor,
        priorities.enhancers,
        priority = true,
        exemptFromJobsCheck = true // we want this asap and its only available in the lategame anyway
    ) {
        comment = "Shroud Capacitor"

        modifierOR(priorities.disable) {
            controller {
                setMissingIncomeFromResources(Building.giga_shroud_capacitor.upkeepResources)
            }

            NOT {
                OR {
                    hasDesignations(PlanetDesignationGroup.Capital)
                    hasDesignations(PlanetDesignationGroup.Research)
                    hasDesignations(PlanetDesignationGroup.Generator)
                    hasDesignations(PlanetDesignationGroup.Mining)
                }
            }
        }
    }

    // research enhancers
    for (building in arrayOf(
        Building.giga_institute_1,
        Building.giga_institute_2,
        Building.giga_supercomputer_1,
        Building.giga_supercomputer_2,
    )) {
        addBuilding(
            building,
            priorities.enhancers,
            priority = true,
            exemptFromJobsCheck = true // we want this asap and its only available in the lategame anyway
        ) {
            comment = "mod: GigaStructuralEngineering"

            modifierOR(priorities.disable) {
                comment = "Disallow when not research designation or to many free jobs"
                disableIfNoNeedForJobs()

                if (onlyOnMatchingDesignation) {
                    NOR {
                        comment = "exclude non-research worlds"
                        hasDesignations(PlanetDesignationGroup.Research)
                        hasDesignations(PlanetDesignationGroup.Capital)
                    }
                } else if (notOnSpecializedWorlds) {
                    OR {
                        comment = "exclude specific production worlds"
                        hasDesignations(PlanetDesignationGroup.Factory)
                        hasDesignations(PlanetDesignationGroup.Refinery)
                        hasDesignations(PlanetDesignationGroup.Foundry)
                    }
                }

                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }

                AND {
                    comment = "disallow if no research buildings"
                    numBuildings(Building.research_lab_1, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_2, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_3, Comparators.EQ, 0)
                }

                addResearchRequirements()
            }
        }
    }

}
