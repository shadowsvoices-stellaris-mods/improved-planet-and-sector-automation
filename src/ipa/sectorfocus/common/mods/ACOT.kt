package ipa.sectorfocus.common.mods

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.PlanetDesignationGroup

fun SectorFocus.addACOT(
    onlyOnMatchingDesignation: Boolean = true,
    notOnSpecializedWorlds: Boolean = true
) {

    for (building in arrayOf(
        Building.dark_matter_synthersizer, // Dimensional Harvester
        Building.ae_dark_matter_synthersizer, // Dimensional Nexus
        Building.fe_machine_dome, // Personality Uplink
        Building.ae_machine_dome, // Infinity Core
    )) {
        addBuilding(building, priorities.enhancers, priority = true, exemptFromJobsCheck = true) {
            comment = "mod: ACOT"

            modifierOR(priorities.disable) {
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }
            }
        }
    }

    Building.stellarite_generator.let { building ->
        addBuilding(building, priorities.enhancers, priority = true, exemptFromJobsCheck = true) {
            comment = "mod: ACOT"

            modifierOR(priorities.disable) {
                // only allow a single stellarite generator
                numBuildings(building, Comparators.GT, 0)
                numBuildings(Building.stellarite_generator_ae, Comparators.GT, 0)
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }
            }
        }
    }

    Building.stellarite_generator_ae.let { building ->
        addBuilding(building, priorities.enhancers, priority = true, exemptFromJobsCheck = true) {
            comment = "mod: ACOT"

            modifierOR(priorities.disable) {
                // only allow a single stellarite generator
                numBuildings(building, Comparators.GT, 0)
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }
            }
        }
    }

    Building.stellarite_nexus.let { building ->
        addBuilding(building, priorities.enhancers, priority = true, exemptFromJobsCheck = true) {
            comment = "mod: ACOT"

            modifierOR(priorities.disable) {
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }
            }
        }
    }

    // research enhancers
    for (building in arrayOf(
        Building.fe_institute, // Delta Science Complex
        Building.ae_institute, // Alpha Master Research Complex
    )) {
        addBuilding(building, priorities.enhancers, priority = true) {
            comment = "mod: ACOT"

            modifierOR(priorities.disable) {
                comment = "Disallow when not research designation or to many free jobs"
                disableIfNoNeedForJobs()

                if (onlyOnMatchingDesignation) {
                    NOR {
                        comment = "exclude non-research worlds"
                        hasDesignations(PlanetDesignationGroup.Research)
                        hasDesignations(PlanetDesignationGroup.Capital)
                    }
                } else if (notOnSpecializedWorlds) {
                    OR {
                        comment = "exclude specific production worlds"
                        hasDesignations(PlanetDesignationGroup.Factory)
                        hasDesignations(PlanetDesignationGroup.Refinery)
                        hasDesignations(PlanetDesignationGroup.Foundry)
                    }
                }

                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }

                AND {
                    comment = "disallow if no research buildings"
                    numBuildings(Building.research_lab_1, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_2, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_3, Comparators.EQ, 0)
                }

                addResearchRequirements()
            }
        }
    }

    // machine gestalt assembly
    for (building in arrayOf(
        Building.super_assembly_machine, // Hard-Light Machine Assembly
        Building.ae_super_assembly_machine, // Alpha Machine Assembly
    )) {
        addBuilding(
            building,
            priorities.population,
            priority = true,
            exemptFromJobsCheck = true // we want this asap and its only available in the lategame anyway
        ) {
            modifierOR(priorities.disable) {
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }
                owner {
                    allowGrowthBuilding(Building.machine_assembly_plant.code, false)
                }
                hasModifier("planet_robot_assembly_control")
                hasModifier("planet_population_control_gestalt")
            }
        }
    }

    // robot assembly
    for (building in arrayOf(
        Building.super_assembly, // Delta Machine Assembly
        Building.ae_super_assembly, // Alpha Machine Assembly
    )) {
        addBuilding(
            building,
            priorities.population,
            priority = true,
            exemptFromJobsCheck = true // we want this asap and its only available in the lategame anyway
        ) {
            modifierOR(priorities.disable) {
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }
                owner {
                    allowGrowthBuilding(Building.robot_assembly_plant.code, false)
                }
                hasModifier("planet_robot_assembly_control")
                hasModifier("planet_population_control_gestalt")
            }
        }
    }

    // Capital: Void Nexus
    addBuilding(
        Building.ae_ancient_control_center,
        priorities.urgent,
        priority = true,
        exemptFromJobsCheck = true // we want this asap and its only available in the lategame anyway
    )

}
