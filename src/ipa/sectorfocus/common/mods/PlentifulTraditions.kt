package ipa.sectorfocus.common.mods

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Resource

fun SectorFocus.addPlentifulTraditionsResearch(
    onlyOnMatchingDesignation: Boolean = true,
    notOnSpecializedWorlds: Boolean = true
) {

    // research enhancers
    for (building in arrayOf(
        Building.plentiful_traditions_cybernetics_facility,
        Building.plentiful_traditions_biogenesis_lab,
        Building.plentiful_traditions_university_of_wisdom,
        Building.plentiful_traditions_experimental_weapons_facility,
    )) {
        addBuilding(building, priorities.enhancers - 1, priority = true) {
            comment = "mod: Plentiful Traditions"

            modifierOR(priorities.disable) {
                comment = "Disallow when not research designation or to many free jobs"
                disableIfNoNeedForJobs()

                if (onlyOnMatchingDesignation) {
                    NOR {
                        comment = "exclude non-research worlds"
                        hasDesignations(PlanetDesignationGroup.Research)
                        hasDesignations(PlanetDesignationGroup.Capital)
                    }
                } else if (notOnSpecializedWorlds) {
                    OR {
                        comment = "exclude specific production worlds"
                        hasDesignations(PlanetDesignationGroup.Factory)
                        hasDesignations(PlanetDesignationGroup.Refinery)
                        hasDesignations(PlanetDesignationGroup.Foundry)
                    }
                }

                controller {
                    OR {
                        checkMissingIncome(Resource.Energy)
                        when (building) {
                            Building.plentiful_traditions_university_of_wisdom -> {}
                            Building.plentiful_traditions_cybernetics_facility -> {
                                checkMissingIncome(Resource.VolatileMotes)
                            }

                            Building.plentiful_traditions_experimental_weapons_facility -> {
                                checkMissingIncome(Resource.RareCrystals)
                            }

                            Building.plentiful_traditions_biogenesis_lab -> {
                                checkMissingIncome(Resource.ExoticGases)
                            }

                            else -> {}
                        }
                    }
                }

                AND {
                    comment = "disallow if no research buildings"
                    numBuildings(Building.research_lab_1, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_2, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_3, Comparators.EQ, 0)
                }

                addResearchRequirements()
            }
        }
    }

}
