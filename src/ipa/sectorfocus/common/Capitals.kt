package ipa.sectorfocus.common

import stellaris.sector.SectorFocus
import stellaris.types.BuildingGroup

fun SectorFocus.addCommonCapitals() {

    for (building in BuildingGroup.CapitalBuildings) {
        addBuilding(building, priorities.urgent, priority = true)
    }

}
