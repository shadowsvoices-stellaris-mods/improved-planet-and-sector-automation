package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.*

fun SectorFocus.addCommonFactory() {

    addBuilding(Building.factory_1, priorities.industrial, priority = true) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkMissingIncome(Resource.ConsumerGoods)
            }
        }

        modifierOR(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignations(PlanetDesignationGroup.Research)
            hasDesignations(PlanetDesignationGroup.Refinery)
            hasDesignations(PlanetDesignationGroup.Foundry)
            hasDesignations(PlanetDesignationGroup.Leisure)
            hasDesignations(PlanetDesignationGroup.Resort)
            disableIfNoNeedForJobs()
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Minerals)
                    // always build to improve efficiency, even if not missing income
                    // missingDesiredIncomeConsumerGoods = false
                }
            }
            owner {
                usesConsumerGoods = false
            }
            AND {
                numDistricts(District.Industrial, Comparators.LTEQ, 1)
            }
        }
    }

    addBuilding(Building.factory_2, priorities.industrial, priority = true) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkMissingIncome(Resource.ConsumerGoods)
            }
        }

        modifierOR(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Minerals)
                    checkMissingIncome(Resource.RareCrystals)
                    // always upgrade to improve efficiency, even if not missing income
                    // missingDesiredIncomeConsumerGoods = false
                }
            }
            owner {
                usesConsumerGoods = false
            }
            /*AND {
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LTEQ, 1)
                }
            }*/
        }
    }

    addBuilding(Building.factory_3, priorities.industrial, priority = true) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkMissingIncome(Resource.ConsumerGoods)
            }
        }

        modifierOR(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Minerals)
                    checkMissingIncome(Resource.RareCrystals)
                    // always upgrade to improve efficiency, even if not missing income
                    // missingDesiredIncomeConsumerGoods = false
                }
            }
            owner {
                usesConsumerGoods = false
            }
            /*AND {
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LTEQ, 1)
                }
            }*/
        }
    }

}
