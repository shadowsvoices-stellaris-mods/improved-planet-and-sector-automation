package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.Resource

fun SectorFocus.addCommonGrowth() {

    // hive
    for (building in arrayOf(Building.spawning_pool, Building.offspring_nest)) {
        addBuilding(building, priorities.population, priority = true) {
            modifierOR(priorities.disable) {
                controller {
                    OR {
                        checkMissingIncome(Resource.Energy)
                        addFoodIncomeBlock()
                    }
                }
                owner {
                    allowGrowthBuilding(sectorConstruct.constructKey, false)
                }
                hasModifier("planet_population_control_gestalt")
            }
        }
    }

    // robot non-gestalt
    addBuilding(
        Building.robot_assembly_plant,
        priorities.population,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Minerals)
                }
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_robot_assembly_control")
            hasModifier("planet_population_control_gestalt")
        }
    }

    // machine gestalt
    addBuilding(
        Building.machine_assembly_plant,
        priorities.population,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    hasMonthlyIncome(Resource.Alloys, Comparators.LTEQ, 10)
                }
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_robot_assembly_control")
            hasModifier("planet_population_control_gestalt")
        }
    }
    addBuilding(
        Building.machine_assembly_complex,
        priorities.population + 1,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    hasMonthlyIncome(Resource.Alloys, Comparators.LTEQ, 10)
                    checkMissingIncome(Resource.RareCrystals)
                }
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_robot_assembly_control")
            hasModifier("planet_population_control_gestalt")
        }
    }

    // bio
    addBuilding(
        Building.clinic,
        priorities.population,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    AND {
                        usesConsumerGoods = true
                        checkMissingIncome(Resource.ConsumerGoods)
                    }
                }
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control")
            hasModifier("planet_population_control_gestalt")
        }
    }
    addBuilding(
        Building.hospital,
        priorities.population,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.ExoticGases)
                }
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control")
        }
    }
    addBuilding(
        Building.clone_vats,
        priorities.population,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                checkMissingIncome(Resource.Energy)
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
            hasModifier("planet_population_control")
        }
    }

    // necroids
    addBuilding(
        Building.necrophage_elevation_chamber,
        priorities.population,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                OR {
                    addFoodIncomeBlock()
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Food)
                    checkMissingIncome(Resource.ConsumerGoods)
                }
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
        }
    }
    addBuilding(
        Building.necrophage_house_of_apotheosis,
        priorities.population + 1,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                OR {
                    addFoodIncomeBlock()
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Food)
                    checkMissingIncome(Resource.ConsumerGoods)
                }
            }
            owner {
                allowGrowthBuilding(sectorConstruct.constructKey, false)
            }
        }
    }

}
