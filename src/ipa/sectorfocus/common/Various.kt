package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Resource
import stellaris.types.Variables

fun SectorFocus.addCommonHydroponicsFarm() {
    addBuilding(Building.hydroponics_farm, priorities.defaultReduced) {
        modifierOR(priorities.disable) {
            hasDesignations(PlanetDesignationGroup.Farming, false)
            disableIfNoNeedForJobs()
            controller {
                checkMissingIncome(Resource.Energy)
            }
            owner {
                checkVariable(Variables.OptionsAllowAgriFarmBuildings, Comparators.EQ, 0)
            }
        }
    }
}

fun SectorFocus.addCommonBetharianPowerPlant() {
    addBuilding(Building.betharian_power_plant, priorities.districts + 1, priority = true) {
        modifierOR(priorities.disable) {
            disableIfNoNeedForJobs()
        }
    }
}

fun SectorFocus.addCommonEmbassies() {
    addBuilding(Building.embassy, priorities.enhancers, priority = true, exemptFromJobsCheck = true) {
        modifierOR(priorities.disable) {
            controller {
                setMissingIncomeFromResources(Building.embassy.upkeepResources)
            }
        }
    }
    addBuilding(Building.grand_embassy, priorities.enhancers, priority = true, exemptFromJobsCheck = true) {
        modifierOR(priorities.disable) {
            controller {
                setMissingIncomeFromResources(Building.grand_embassy.upkeepResources)
            }
        }
    }
}
