package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.District
import stellaris.types.DistrictGroup
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Resource

fun SectorFocus.addDistricts(
    strictDesignations: Boolean = false,
    habitatSpecialization: Boolean = true
) {

    for (district in DistrictGroup.Generator) {
        addDistrict(district, priorities.districts) {

            modifierOR(1.02) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    checkMissingIncome(Resource.Energy)
                }
            }

            modifierOR(1.10) {
                comment = "increase priority for planets with matching focus" +
                    ", slightly stronger than missing income to ensure districts are build by designations first"
                hasDesignations(PlanetDesignationGroup.Generator)
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                if (strictDesignations) {
                    hasDesignations(PlanetDesignationGroup.Generator, false)
                } else if (habitatSpecialization) {
                    AND {
                        comment = "disallow on non-matching habitats as these need more strict specialization"
                        isPlanetClassHabitat()
                        hasDesignations(PlanetDesignationGroup.Generator, false)
                    }
                }

                AND {
                    comment = "Disallow districts when possible to build research"
                    hasDesignations(PlanetDesignationGroup.Research)
                    freeBuildingSlots(Comparators.GT, 0)
                }
            }
        }
    }

    for (district in DistrictGroup.Farming) {
        addDistrict(district, priorities.districts) {

            modifierOR(1.02) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    checkMissingIncome(Resource.Food)
                }
            }

            modifierOR(1.10) {
                comment = "increase priority for planets with matching focus" +
                    ", slightly stronger than missing income to ensure districts are build by designations first"
                hasDesignations(PlanetDesignationGroup.Farming)
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                if (strictDesignations) {
                    hasDesignations(PlanetDesignationGroup.Farming, false)
                } else if (habitatSpecialization) {
                    AND {
                        comment = "disallow on non-matching habitats as these need more strict specialization"
                        isPlanetClassHabitat()
                        hasDesignations(PlanetDesignationGroup.Farming, false)
                    }
                }

                AND {
                    // TODO consider if this is a good choice, sometimes you might rather prefer the pops resettling to other worlds were they are actually useful instead of creating more than needed food
                    comment = "only disallow farming districts above desired income if not a agri world"
                    hasDesignations(PlanetDesignationGroup.Farming, false)
                    controller {
                        checkDesiredIncome(Resource.Food, false)
                    }
                }

                owner {
                    usesFood = false
                }

                AND {
                    comment = "Disallow districts when possible to build research"
                    hasDesignations(PlanetDesignationGroup.Research)
                    freeBuildingSlots(Comparators.GT, 0)
                }
            }
        }
    }

    for (district in DistrictGroup.Mining) {
        addDistrict(district, priorities.districts) {

            modifierOR(1.02) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    checkMissingIncome(Resource.Minerals)
                }
            }

            modifierOR(1.10) {
                comment = "increase priority for planets with matching focus" +
                    ", slightly stronger than missing income to ensure districts are build by designations first"
                hasDesignations(PlanetDesignationGroup.Mining)
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                if (strictDesignations) {
                    hasDesignations(PlanetDesignationGroup.Mining, false)
                } else if (habitatSpecialization) {
                    AND {
                        comment = "disallow on non-matching habitats as these need more strict specialization"
                        isPlanetClassHabitat()
                        hasDesignations(PlanetDesignationGroup.Mining, false)
                    }
                }

                AND {
                    comment = "Disallow districts when possible to build research"
                    hasDesignations(PlanetDesignationGroup.Research)
                    freeBuildingSlots(Comparators.GT, 0)
                }
            }
        }
    }

    for (district in DistrictGroup.Research) {
        addDistrict(district, priorities.research) {

            modifierOR(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignations(PlanetDesignationGroup.Research)
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                //if (strictDesignations) {
                //    hasDesignations(PlanetDesignationGroup.Research, false)
                //} else if (habitatSpecialization) {
                //    AND {
                //        comment = "disallow on non-matching habitats as these need more strict specialization"
                //        isPlanetClassHabitat()
                //        hasDesignations(PlanetDesignationGroup.Research, false)
                //    }
                //}

                // disallow research districts on non-research worlds
                AND {
                    hasDesignations(PlanetDesignationGroup.Research, false)
                    hasDesignations(PlanetDesignationGroup.Capital, false)
                }

                controller {
                    OR {
                        checkMissingIncome(Resource.Energy)
                        checkMissingIncome(Resource.Minerals)
                    }
                }

                addResearchRequirements()
            }
        }
    }

    for (district in DistrictGroup.Leisure) {
        addDistrict(district, priorities.districts) {

            modifierOR(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignations(PlanetDesignationGroup.Leisure)
            }

            modifierOR(priorities.disable) {
                disableIfNoNeedForJobs()

                controller {
                    OR {
                        checkMissingIncome(Resource.Energy)
                        checkMissingIncome(Resource.ConsumerGoods)
                    }
                }

                hasDesignations(PlanetDesignationGroup.Leisure, false)
            }
        }
    }

    for (district in DistrictGroup.Industrial) {
        addDistrict(district, priorities.industrial) {

            modifierOR(1.01) {
                comment = "increase priority slightly if below desired resource income"
                controller {
                    OR {
                        checkDesiredIncome(Resource.Alloys)
                        checkMissingIncome(Resource.ConsumerGoods)
                    }
                }
            }

            modifierOR(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignationIndustrialAny()
            }

            modifierOR(priorities.disable) {
                comment = "Disallow for various conditions"
                disableIfNoNeedForJobs()

                if (strictDesignations) {
                    comment += " with strict designations enabled"
                    // disallow anywhere but industrial worlds in strict mode
                    hasDesignationIndustrialAny(false)
                } else if (habitatSpecialization) {
                    AND {
                        comment = "disallow on non-matching habitats as these need more strict specialization"
                        isPlanetClassHabitat()
                        hasDesignationIndustrialAny(false)
                    }
                }

                controller {
                    comment = "disallow when lacking required incomes"
                    OR {
                        checkMissingIncome(Resource.Energy)
                        checkMissingIncome(Resource.Minerals)
                    }
                }

                AND {
                    comment = "only check desired income on non-industrial designations"
                    hasDesignationIndustrialAny(false) // do not prevent continued production on industrial worlds
                    controller {
                        OR {
                            AND {
                                comment = "disallow if both are above desired value"
                                usesConsumerGoods = true
                                checkDesiredIncome(Resource.Alloys, false)
                                checkDesiredIncome(Resource.ConsumerGoods, false)
                            }
                            AND {
                                comment = "disallow if above desired value"
                                usesConsumerGoods = false
                                checkDesiredIncome(Resource.Alloys, false)
                            }
                        }
                    }
                }

                // prevent on some specialized worlds by default
                hasDesignations(PlanetDesignationGroup.Research)
                hasDesignations(PlanetDesignationGroup.Penal)
                hasDesignations(PlanetDesignationGroup.Resort)
            }
        }
    }

    addDistrict(District.ArcologyArmsIndustry, priorities.industrial) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkDesiredIncome(Resource.Alloys)
            }
        }

        modifierOR(1.10) {
            comment = "increase priority for planets with matching focus"
            hasDesignations(PlanetDesignationGroup.Foundry, true)
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()

            if (strictDesignations) {
                comment += " with strict designations enabled"
                // disallow anywhere but industrial worlds in strict mode
                hasDesignations(PlanetDesignationGroup.Foundry, false)
            }

            controller {
                comment = "disallow when lacking required incomes"
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Minerals)
                    checkMissingIncome(Resource.VolatileMotes)
                }
            }

            AND {
                comment = "only check desired income on non-industrial designations"
                hasDesignationIndustrialAny(false) // do not prevent continued production on industrial worlds
                controller {
                    checkDesiredIncome(Resource.Alloys, false)
                }
            }

            // prevent on some specialized worlds by default
            hasDesignations(PlanetDesignationGroup.Research)
        }
    }

    addDistrict(District.ArcologyCivilianIndustry, priorities.industrial) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkDesiredIncome(Resource.ConsumerGoods)
            }
        }

        modifierOR(1.10) {
            comment = "increase priority for planets with matching focus"
            hasDesignations(PlanetDesignationGroup.Factory, true)
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()

            if (strictDesignations) {
                comment += " with strict designations enabled"
                // disallow anywhere but industrial worlds in strict mode
                hasDesignations(PlanetDesignationGroup.Factory, false)
            }

            controller {
                comment = "disallow when lacking required incomes"
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Minerals)
                    checkMissingIncome(Resource.RareCrystals)
                }
            }

            AND {
                comment = "only check desired income on non-industrial designations"
                hasDesignationIndustrialAny(false) // do not prevent continued production on industrial worlds
                controller {
                    checkDesiredIncome(Resource.ConsumerGoods, false)
                }
            }

            // prevent on some specialized worlds by default
            hasDesignations(PlanetDesignationGroup.Research)
        }
    }


    for (district in DistrictGroup.Trade) {
        addDistrict(district, priorities.districts) {

            modifierOR(1.10) {
                comment = "increase priority for planets with matching focus"
                hasDesignations(PlanetDesignationGroup.Trade)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow for various conditions"
                disableIfNoNeedForJobs()
                hasDesignations(PlanetDesignationGroup.Trade, false)
            }
        }
    }

}
