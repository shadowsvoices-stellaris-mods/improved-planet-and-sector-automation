package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.*

fun SectorFocus.addCommonFoundry() {

    addBuilding(Building.foundry_1, priorities.industrial, priority = true) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkDesiredIncome(Resource.Alloys)
            }
        }

        modifierOR(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            hasDesignations(PlanetDesignationGroup.Research)
            hasDesignations(PlanetDesignationGroup.Refinery)
            hasDesignations(PlanetDesignationGroup.Factory)
            hasDesignations(PlanetDesignationGroup.Leisure)
            hasDesignations(PlanetDesignationGroup.Resort)
            disableIfNoNeedForJobs()
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Minerals)
                    // always build to improve efficiency, even if not missing income
                    // missingDesiredIncomeAlloys = false
                }
            }
            AND {
                numDistricts(District.Industrial, Comparators.LTEQ, 1)
            }
        }
    }

    addBuilding(Building.foundry_2, priorities.industrial, priority = true) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkDesiredIncome(Resource.Alloys)
            }
        }

        modifierOR(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Minerals)
                    checkMissingIncome(Resource.VolatileMotes)
                    // always upgrade to improve efficiency, even if not missing income
                    // missingDesiredIncomeAlloys = false
                }
            }
            /*AND {
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LTEQ, 1)
                }
            }*/
        }
    }

    addBuilding(Building.foundry_3, priorities.industrial, priority = true) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkDesiredIncome(Resource.Alloys)
            }
        }

        modifierOR(priorities.enhancers / priorities.industrial) {
            comment = "Boost priority if industrial districts are present"
            for (district in DistrictGroup.Industrial) {
                numDistricts(district, Comparators.GTEQ, 3)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()
            controller {
                OR {
                    checkMissingIncome(Resource.Energy)
                    checkMissingIncome(Resource.Minerals)
                    checkMissingIncome(Resource.VolatileMotes)
                    // always upgrade to improve efficiency, even if not missing income
                    // missingDesiredIncomeAlloys = false
                }
            }
            /*AND {
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LTEQ, 1)
                }
            }*/
        }
    }

}
