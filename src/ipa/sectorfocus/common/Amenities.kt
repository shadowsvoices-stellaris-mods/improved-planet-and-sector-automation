package ipa.sectorfocus.common


import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.BuildingGroup
import stellaris.types.Resource

fun SectorFocus.addCommonAmenities() {

    val prebuildThreshold = 6
    val urgentThreshold = 1
    val crisisThreshold = -2

    // amenities T1
    for (building in BuildingGroup.AmenitiesBuildingsT1) {
        addBuilding(building, priorities.base, priority = true, exemptFromJobsCheck = true) {

            modifierAND(priorities.prebuild) {
                comment = "Pre-build when approaching threshold and not to many free jobs"
                freeAmenities(Comparators.LTEQ, prebuildThreshold)
                freeAmenities(Comparators.GT, urgentThreshold)
                freeJobs(Comparators.LT, 5)
            }

            modifierAND(priorities.urgent) {
                comment = "Increase priority when out of amenities"
                freeAmenities(Comparators.LTEQ, urgentThreshold)
                freeAmenities(Comparators.GT, crisisThreshold)
            }

            modifierAND(priorities.crisis) {
                comment = "Increase to crisis status if this gets out of hand"
                freeAmenities(Comparators.LTEQ, crisisThreshold)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when no workers or free amenities"

                freeAmenities(Comparators.GT, prebuildThreshold)

                AND {
                    freeAmenities(Comparators.GT, 0)
                    disableIfNoNeedForJobs()
                }

                //AND {
                //    freeAmenities(Comparators.LT, 0)
                //    numFreeDistrictsAny(Comparators.GT, 0)
                //}
            }
        }
    }

    // amenities T2
    for (building in BuildingGroup.AmenitiesBuildingsT2) {
        addBuilding(building, priorities.base + 1, priority = true, exemptFromJobsCheck = true) {

            modifierAND(priorities.prebuild) {
                comment = "Pre-build when approaching threshold and not to many free jobs"
                freeAmenities(Comparators.LTEQ, prebuildThreshold)
                freeAmenities(Comparators.GT, urgentThreshold)
                freeJobs(Comparators.LT, 5)
            }

            modifierAND(priorities.urgent) {
                comment = "Increase priority when out of amenities"
                freeAmenities(Comparators.LTEQ, urgentThreshold)
                freeAmenities(Comparators.GT, crisisThreshold)
            }

            modifierAND(priorities.crisis) {
                comment = "Increase to crisis status if this gets out of hand"
                freeAmenities(Comparators.LTEQ, crisisThreshold)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when no workers or free amenities"

                freeAmenities(Comparators.GT, prebuildThreshold)

                AND {
                    freeAmenities(Comparators.GT, 0)
                    disableIfNoNeedForJobs()
                }

                numFreeDistrictsAny(Comparators.GT, 0)
                // upgradeMinFreeSlotsAllowed(false) // prefer amenities building upgrades to conserve slots

                controller {
                    checkMissingIncome(Resource.ExoticGases)
                }

                //AND {
                //    freeAmenities(Comparators.LT, 0)
                //    numFreeDistrictsAny(Comparators.GT, 0)
                //}
            }
        }
    }

}
