package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.BuildingGroup
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Resource

fun SectorFocus.addCommonUnity() {
    addCommonUnityJobs()
    addCommonUnityJobBoosters()
    addCommonUnityMemorials()
}

fun SectorFocus.addCommonUnityJobs() {

    for (building in BuildingGroup.UnityJobsT1) {
        addBuilding(building, priorities.unity) {
            modifierOR(priorities.disable) {
                unityAllowed(false)
                disableIfNoNeedForJobs()
                hasDesignations(PlanetDesignationGroup.Capital) // disallow unity on capital (= primarily research) planets
                hasDesignations(PlanetDesignationGroup.Research) // disallow unity on research planets
                controller {
                    OR {
                        setMissingIncomeFromResources(building.upkeepResources)
                        if (building.upkeepResources.contains(Resource.Food)) {
                            checkMissingIncome(Resource.Food, null) // unset value set by usesResources
                            addFoodIncomeBlock()
                        }
                    }
                }
            }
        }
    }

    for (building in BuildingGroup.UnityJobsT2) {
        addBuilding(building, priorities.unity + 0.1) {
            modifierOR(priorities.disable) {
                unityAllowed(false)
                disableIfNoNeedForJobs()
                upgradeMinFreeSlotsAllowed(false)
                controller {
                    OR {
                        setMissingIncomeFromResources(building.upkeepResources)
                        if (building.upkeepResources.contains(Resource.Food)) {
                            checkMissingIncome(Resource.Food, null) // unset value set by usesResources
                            addFoodIncomeBlock()
                        }
                    }
                }
            }
        }
    }

    for (building in BuildingGroup.UnityJobsT3) {
        addBuilding(building, priorities.unity + 0.2) {
            modifierOR(priorities.disable) {
                unityAllowed(false)
                disableIfNoNeedForJobs()
                upgradeMinFreeSlotsAllowed(false)
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                    if (building.upkeepResources.contains(Resource.Food)) {
                        checkMissingIncome(Resource.Food, null) // unset value set by usesResources
                        addFoodIncomeBlock()
                    }
                }
            }
        }
    }

}

fun SectorFocus.addCommonUnityJobBoosters() {
    for (building in BuildingGroup.UnityJobsBooster) {
        addBuilding(building, priorities.unity + 2) {
            modifierOR(priorities.disable) {
                unityAllowed(false)
                hasDesignations(PlanetDesignationGroup.Research) // disallow unity on research planets
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }

                NOT {
                    comment = "Require >= 4 unity jobs or disallow"
                    OR {
                        for (b in arrayOf("bureaucrat", "coordinator", "synapse_drone", "priest", "duelist")) {
                            numAssignedJobs(b, Comparators.GTEQ, 4)
                        }
                    }
                }
            }
        }
    }
}

fun SectorFocus.addCommonUnityMemorials() {

    for (building in BuildingGroup.UnityMemorialsT1) {
        addBuilding(building, priorities.unityMemorials, priority = true, exemptFromJobsCheck = true) {
            modifierOR(priorities.disable) {
                unityAllowed(false)
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }
                // freeBuildingSlots(Comparators.LT, 2)
                numPops(Comparators.LT, 10)
            }
        }
    }

    for (building in BuildingGroup.UnityMemorialsT2) {
        addBuilding(building, priorities.unityMemorials, priority = true, exemptFromJobsCheck = true) {
            modifierOR(priorities.disable) {
                unityAllowed(false)
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }
            }
        }
    }

    for (building in BuildingGroup.UnityMemorialsT3) {
        addBuilding(building, priorities.unityMemorials, priority = true, exemptFromJobsCheck = true) {
            modifierOR(priorities.disable) {
                unityAllowed(false)
                controller {
                    setMissingIncomeFromResources(building.upkeepResources)
                }
            }
        }
    }

}
