package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.BuildingGroup
import stellaris.types.DistrictGroup
import stellaris.types.Resource

fun SectorFocus.addCommonHousing() {

    // housing districts
    for (district in DistrictGroup.Housing) {
        addDistrict(district, priorities.base) {

            modifierOR(priorities.urgent) {
                comment = "Increased priority when almost out of housing"
                freeHousing(Comparators.LTEQ, 1)
                AND {
                    comment = "or when out of amenities for gestalt"
                    owner { isGestalt = true }
                    freeAmenities(Comparators.LTEQ, 1)
                }
            }

            modifierOR(priorities.crisis / priorities.urgent) {
                comment = "Critical priority when out of housing"
                freeHousing(Comparators.LTEQ, 0)
                AND {
                    comment = "or when out of amenities for gestalt"
                    owner { isGestalt = true }
                    freeAmenities(Comparators.LTEQ, 0)
                }
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when not needed"

                AND {
                    comment = "For gestalt consider free amenities in addition to housing"
                    owner { isGestalt = true }
                    freeAmenities(Comparators.GTEQ, 1)
                    if (experimental) buildCityForBuildingSlots(false)

                    OR {
                        comment = "Control pre-building of housing based on free districts to reduce growth reduction"
                        // TODO consider checking num_uncleared_blockers in addition to free district slots
                        freeHousing(Comparators.GT, 5)

                        AND {
                            comment = "pre-build less if only one district is free"
                            freeDistrictSlots(Comparators.LTEQ, 1)
                            freeHousing(Comparators.GTEQ, 2)
                        }
                    }
                }

                AND {
                    comment = "Disallow for non-gestalt based on housing alone"
                    owner { isGestalt = false }
                    if (experimental) buildCityForBuildingSlots(false)

                    OR {
                        comment = "Control pre-building of housing based on free districts to reduce growth reduction"
                        // TODO consider checking num_uncleared_blockers in addition to free district slots
                        freeHousing(Comparators.GT, 5)

                        AND {
                            comment = "pre-build less if only one district is free"
                            freeDistrictSlots(Comparators.LTEQ, 1)
                            freeHousing(Comparators.GTEQ, 2)
                        }
                    }
                }
            }
        }
    }

    // housing buildings T1
    for (building in BuildingGroup.HousingBuildingsT1) {
        addBuilding(building, priorities.base) {

            modifierOR(priorities.urgent) {
                comment = "Increased priority when almost out of housing"
                freeHousing(Comparators.LTEQ, 1)
            }

            modifierOR(priorities.crisis / priorities.urgent) {
                comment = "Critical priority when out of housing"
                freeHousing(Comparators.LTEQ, 0)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when not needed"
                freeDistrictSlots(Comparators.GT, 0)
                freeHousing(Comparators.GTEQ, 1)
            }
        }
    }

    // housing buildings T2
    for (building in BuildingGroup.HousingBuildingsT2) {
        addBuilding(building, priorities.base + 0.01) {

            modifierOR(priorities.urgent) {
                comment = "Increased priority when almost out of housing"
                freeHousing(Comparators.LTEQ, 1)
            }

            modifierOR(priorities.crisis / priorities.urgent) {
                comment = "Critical priority when out of housing"
                freeHousing(Comparators.LTEQ, 0)
            }

            modifierOR(priorities.disable) {
                comment = "Disallow when not needed"
                freeDistrictSlots(Comparators.GT, 0)
                freeHousing(Comparators.GTEQ, 1)

                controller {
                    checkMissingIncome(Resource.RareCrystals)
                }
            }
        }
    }

}
