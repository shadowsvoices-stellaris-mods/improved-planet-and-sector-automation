package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.District
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Resource

fun SectorFocus.addCommonResearch(
    onlyOnMatchingDesignation: Boolean = true,
    notOnSpecializedWorlds: Boolean = true
) {

    // research enhancers
    for (building in arrayOf(Building.supercomputer, Building.institute)) {
        addBuilding(building, priorities.enhancers, priority = true) {
            modifierOR(priorities.disable) {
                comment = "Disallow when not research designation or to many free jobs"
                disableIfNoNeedForJobs()

                if (onlyOnMatchingDesignation) {
                    NOR {
                        comment = "exclude non-research worlds"
                        hasDesignations(PlanetDesignationGroup.Research)
                        hasDesignations(PlanetDesignationGroup.Capital)
                    }
                } else if (notOnSpecializedWorlds) {
                    OR {
                        comment = "exclude specific production worlds"
                        hasDesignations(PlanetDesignationGroup.Factory)
                        hasDesignations(PlanetDesignationGroup.Refinery)
                        hasDesignations(PlanetDesignationGroup.Foundry)
                    }
                }

                controller {
                    checkMissingIncome(Resource.Energy)
                }

                AND {
                    comment = "disallow if no research buildings"
                    numBuildings(Building.research_lab_1, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_2, Comparators.EQ, 0)
                    numBuildings(Building.research_lab_3, Comparators.EQ, 0)
                }

                addResearchRequirements()
            }
        }
    }

    addResearchLab(
        Building.research_lab_1,
        priorities.research,
        requireExoticGasesIncome = null,
        isUpgrade = false,
        onlyOnMatchingDesignation = onlyOnMatchingDesignation,
        notOnSpecializedWorlds = notOnSpecializedWorlds
    )

    addResearchLab(
        Building.research_lab_2,
        priorities.research + 1,
        requireExoticGasesIncome = true,
        isUpgrade = true,
        onlyOnMatchingDesignation = onlyOnMatchingDesignation,
        notOnSpecializedWorlds = notOnSpecializedWorlds
    )

    addResearchLab(
        Building.research_lab_3,
        priorities.research + 2,
        requireExoticGasesIncome = true,
        isUpgrade = true,
        onlyOnMatchingDesignation = onlyOnMatchingDesignation,
        notOnSpecializedWorlds = notOnSpecializedWorlds
    )

}

private fun SectorFocus.addResearchLab(
    building: Building,
    baseWeight: Double,
    requireExoticGasesIncome: Boolean? = null,
    isUpgrade: Boolean = false,
    onlyOnMatchingDesignation: Boolean = true,
    notOnSpecializedWorlds: Boolean = true
) = addBuilding(building, baseWeight, priority = true) {
    modifierOR(priorities.disable) {
        comment = "Disallow when: not research designation, to many free jobs or not enough income"
        disableIfNoNeedForJobs()
        if (isUpgrade) {
            upgradeMinFreeSlotsAllowed(false)
        }

        if (onlyOnMatchingDesignation) {
            NOR {
                comment = "exclude non-research worlds"
                hasDesignations(PlanetDesignationGroup.Research)
                hasDesignations(PlanetDesignationGroup.Capital)
            }
        } else if (notOnSpecializedWorlds) {
            OR {
                comment = "exclude specific production worlds"
                hasDesignations(PlanetDesignationGroup.Factory)
                hasDesignations(PlanetDesignationGroup.Refinery)
                hasDesignations(PlanetDesignationGroup.Foundry)
            }
        }

        AND {
            comment = "prevent construction of research labs while there are free research districts"
            isPlanetClassHabitat()
            numFreeDistricts(District.HabitatResearch, Comparators.GTEQ, 1)
        }

        AND {
            comment = "prevent construction of research labs while there are free research districts"
            isPlanetClassRingworld()
            numFreeDistricts(District.RingworldResearch, Comparators.GTEQ, 1)
        }

        AND {
            comment = "prevent construction of research labs while there are free research districts"
            isPlanetClass("pc_giga_planetary_computer")
            numFreeDistricts(District.giga_planet_science, Comparators.GTEQ, 1)
        }

        //OR {
        //    comment = "prevent construction of research labs while there are free research districts"
        //    for (district in DistrictGroup.Research) {
        //        numFreeDistricts(district, Comparators.GTEQ, 1)
        //    }
        //}

        controller {
            OR {
                checkMissingIncome(Resource.Energy)
                checkMissingIncome(Resource.ExoticGases, requireExoticGasesIncome)
            }
        }

        addResearchRequirements()
    }
}
