package ipa.sectorfocus.common

import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Resource

fun SectorFocus.addCommonStrategicFactories() {

    addBuilding(Building.chemical_plant, priorities.strategic, priority = true) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkMissingIncome(Resource.VolatileMotes)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow if to many free jobs or above desired income"
            disableIfNoNeedForJobs()

            controller {
                OR {
                    checkMissingIncome(Resource.Minerals)
                    checkDesiredIncome(Resource.VolatileMotes, false)
                }
            }

            // prevent on some specialized worlds by default
            hasDesignations(PlanetDesignationGroup.Research)
            hasDesignations(PlanetDesignationGroup.Penal)
            hasDesignations(PlanetDesignationGroup.Resort)
        }
    }

    addBuilding(Building.refinery, priorities.strategic, priority = true) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkMissingIncome(Resource.ExoticGases)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()

            controller {
                OR {
                    checkMissingIncome(Resource.Minerals)
                    checkDesiredIncome(Resource.ExoticGases, false)
                }
            }

            // prevent on some specialized worlds by default
            hasDesignations(PlanetDesignationGroup.Research)
            hasDesignations(PlanetDesignationGroup.Penal)
            hasDesignations(PlanetDesignationGroup.Resort)
        }
    }

    addBuilding(Building.crystal_plant, priorities.strategic, priority = true) {

        modifierOR(1.01) {
            comment = "increase priority slightly if below desired resource income"
            controller {
                checkMissingIncome(Resource.RareCrystals)
            }
        }

        modifierOR(priorities.disable) {
            comment = "Disallow for various conditions"
            disableIfNoNeedForJobs()

            controller {
                OR {
                    checkMissingIncome(Resource.Minerals)
                    checkDesiredIncome(Resource.RareCrystals, false)
                }
            }

            // prevent on some specialized worlds by default
            hasDesignations(PlanetDesignationGroup.Research)
            hasDesignations(PlanetDesignationGroup.Penal)
            hasDesignations(PlanetDesignationGroup.Resort)
        }
    }

}
