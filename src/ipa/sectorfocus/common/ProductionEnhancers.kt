package ipa.sectorfocus.common

import stellaris.logic.Comparators
import stellaris.sector.SectorFocus
import stellaris.types.Building
import stellaris.types.DistrictGroup
import stellaris.types.PlanetDesignationGroup
import stellaris.types.Resource

fun SectorFocus.addCommonProductionEnhancers(
    onlyOnMatchingDesignation: Boolean = true,
    minMatchingDistricts: Int = 3
) {

    addBuilding(
        Building.mineral_purification_plant,
        priorities.enhancers,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Mining) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
            if (onlyOnMatchingDesignation) {
                hasDesignations(PlanetDesignationGroup.Mining, false)
            }
        }
    }
    addBuilding(
        Building.mineral_purification_hub,
        priorities.enhancers + 1,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                checkMissingIncome(Resource.VolatileMotes)
            }
        }
    }

    addBuilding(
        Building.energy_grid,
        priorities.enhancers,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Generator) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
            if (onlyOnMatchingDesignation) {
                hasDesignations(PlanetDesignationGroup.Generator, false)
            }
        }
    }

    addBuilding(
        Building.energy_nexus,
        priorities.enhancers + 1,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                checkMissingIncome(Resource.ExoticGases)
            }
        }
    }

    addBuilding(
        Building.food_processing_facility,
        priorities.enhancers,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            owner {
                usesFood = false
            }
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Farming) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
            if (onlyOnMatchingDesignation) {
                hasDesignations(PlanetDesignationGroup.Farming, false)
            }
        }
    }

    addBuilding(
        Building.food_processing_center,
        priorities.enhancers + 1,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            owner {
                usesFood = false
            }
            controller {
                checkMissingIncome(Resource.VolatileMotes)
            }
        }
    }

    addBuilding(
        Building.ministry_production,
        priorities.enhancers,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                checkMissingIncome(Resource.VolatileMotes)
            }
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
        }
    }

    addBuilding(
        Building.production_center,
        priorities.enhancers,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                checkMissingIncome(Resource.VolatileMotes)
            }
            AND {
                comment = "Disallow if not enough districts build"
                for (district in DistrictGroup.Industrial) {
                    numDistricts(district, Comparators.LT, minMatchingDistricts)
                }
            }
        }
    }

    addBuilding(
        Building.psi_corps,
        priorities.enhancers,
        priority = true
    ) {
        modifierOR(priorities.disable) {
            controller {
                checkMissingIncome(Resource.Energy)
            }
        }
    }

}
