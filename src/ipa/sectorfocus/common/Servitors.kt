package ipa.sectorfocus.common

import stellaris.types.Building
import stellaris.types.CitizenshipType
import stellaris.sector.SectorFocus

fun SectorFocus.addCommonOrganicSanctuary() {

    addBuilding(Building.organic_sanctuary, priorities.urgent, priority = true, exemptFromJobsCheck = true) {
        modifierOR(priorities.disable) {
            NOT {
                AND {
                    owner {
                        hasAuthority("auth_machine_intelligence")
                        hasValidCivic("civic_machine_servitor")
                    }
                    anyOwnedPop {
                        comment = "Only build when there is an actual need for sanctuaries"
                        isUnemployed(true)
                        hasCitizenshipType(CitizenshipType.OrganicTrophy)
                    }
                }
            }
            controller {
                setMissingIncomeFromResources(Building.organic_sanctuary.upkeepResources)
            }
        }
    }

    addBuilding(Building.organic_paradise, priorities.urgent + 1, priority = true, exemptFromJobsCheck = true) {
        modifierOR(priorities.disable) {
            NOT {
                AND {
                    owner {
                        hasAuthority("auth_machine_intelligence")
                        hasValidCivic("civic_machine_servitor")
                    }
                    anyOwnedPop {
                        comment = "Only build when there is an actual need for sanctuaries"
                        isUnemployed(true)
                        hasCitizenshipType(CitizenshipType.OrganicTrophy)
                    }
                }
            }
            controller {
                setMissingIncomeFromResources(Building.organic_paradise.upkeepResources)
            }
        }
    }

}
