package ipa.sectorfocus

import stellaris.template.TemplateFile
import stellaris.sector.SectorFocus
import ipa.sectorfocus.common.*
import ipa.sectorfocus.common.mods.addACOT
import ipa.sectorfocus.common.mods.addGigaStructuralEngineering
import ipa.sectorfocus.common.mods.addPlentifulTraditionsResearch

class Mixed : TemplateFile("common/sector_focuses/21_mixed.txt", {
    addBlock(SectorFocus("mixed")) {

        // shift priorities for mixed focus
        priorities.apply {
            districts = defaultReduced
            strategic = default
            industrial = default
            research = default
            naturalStrategic = default
            unity = default
        }

        addCommonHousing()
        addCommonUnity()
        addCommonGrowth()

        // research
        addCommonResearch(false, true)

        // district resource production
        addDistricts()

        // industrial
        addCommonFoundry()
        addCommonFactory()

        // strategic resources
        addCommonStrategicFactories()
        addCommonStrategicNatural()

        // special buildings
        addCommonProductionEnhancers(false)

        // misc
        addCommonMilitaryBuildings()
        addCommonHydroponicsFarm()
        addCommonBetharianPowerPlant()
        addCommonEmbassies()
        addCommonOrganicSanctuary()

        // mods
        addPlentifulTraditionsResearch()
        addACOT()
        addGigaStructuralEngineering()

    }
})
