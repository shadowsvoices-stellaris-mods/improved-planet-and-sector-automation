package ipa.sectorfocus

import stellaris.template.TemplateFile
import stellaris.sector.SectorFocus
import ipa.sectorfocus.common.*
import ipa.sectorfocus.common.mods.addACOT
import ipa.sectorfocus.common.mods.addGigaStructuralEngineering
import ipa.sectorfocus.common.mods.addPlentifulTraditionsResearch

class Strict : TemplateFile("common/sector_focuses/22_strict.txt", {
    addBlock(SectorFocus("strict")) {
        hidden = false

        addCommonHousing()
        addCommonUnity()
        addCommonGrowth()

        // research
        addCommonResearch()

        // district resource production
        addDistricts(strictDesignations = true)

        // industrial
        addCommonFoundry()
        addCommonFactory()

        // strategic resources
        addCommonStrategicFactories()
        addCommonStrategicNatural()

        // special buildings
        addCommonProductionEnhancers()

        // misc
        addCommonMilitaryBuildings()
        addCommonHydroponicsFarm()
        addCommonBetharianPowerPlant()
        addCommonEmbassies()
        addCommonOrganicSanctuary()

        // mods
        addPlentifulTraditionsResearch()
        addACOT()
        addGigaStructuralEngineering()

    }
})
