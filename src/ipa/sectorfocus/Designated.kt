package ipa.sectorfocus

import stellaris.template.TemplateFile
import stellaris.sector.SectorFocus
import ipa.sectorfocus.common.*
import ipa.sectorfocus.common.mods.addACOT
import ipa.sectorfocus.common.mods.addGigaStructuralEngineering
import ipa.sectorfocus.common.mods.addPlentifulTraditionsResearch

class Designated : TemplateFile("common/sector_focuses/20_designated.txt", {
    addBlock(SectorFocus("designated")) {

        // designated is default
        aiWeight = 100

        addCommonHousing()
        addCommonUnity()
        addCommonGrowth()

        // research
        addCommonResearch()

        // district resource production
        addDistricts()

        // industrial
        addCommonFoundry()
        addCommonFactory()

        // strategic resources
        addCommonStrategicFactories()
        addCommonStrategicNatural()

        // special buildings
        addCommonProductionEnhancers()

        // misc
        addCommonMilitaryBuildings()
        addCommonHydroponicsFarm()
        addCommonBetharianPowerPlant()
        addCommonEmbassies()
        addCommonOrganicSanctuary()

        // mods
        addPlentifulTraditionsResearch()
        addACOT()
        addGigaStructuralEngineering()

    }
})
