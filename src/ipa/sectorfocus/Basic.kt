package ipa.sectorfocus

import stellaris.template.TemplateFile
import stellaris.sector.SectorFocus
import ipa.sectorfocus.common.addCommonAmenities
import ipa.sectorfocus.common.addCommonCapitals
import ipa.sectorfocus.common.addCommonCrime

class Basic : TemplateFile("common/sector_focuses/00_focus.txt", {
    addBlock(SectorFocus("basic")) {
        comment = "clear original and reset basic as it interferes with advanced types"
        hidden = true
        clearBlockers = false
        aiWeight = null

        addCommonCapitals()
        addCommonAmenities()
        addCommonCrime()
    }
})
