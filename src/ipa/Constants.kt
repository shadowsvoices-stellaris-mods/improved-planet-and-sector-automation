package ipa

import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

const val GENERATE_SECTOR_AUTOMATION = true
const val GENERATE_COLONY_AUTOMATION = false
const val GENERATE_DISABLE_VANILLA_COLONY_AUTOMATION = true
const val GENERATE_JOB_MANAGEMENT_EVENTS = true

object ModInfo {
    const val Version = "0.15.5"
    val UpdatedAt = Instant.now().let {
        DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm")
            .withLocale(Locale.GERMANY)
            .withZone(ZoneId.of("GMT+2"))
            .format(it)
    }
}

object Policies {
    val incomeStepUnlimited = 500000 // must be sufficiently larger than incomeSteps
    val incomeStepDisabled = -10000
    val incomeSteps = arrayOf(
        0, 1, 5, 10, 20, 30, 50, 75, 100, 150, 200, 250, 500, 750, 1000,
        1500, 2000, 3000, 5000, 10000, 15000, 20000, 30000, 40000, 50000
    )
    val incomeStepsStrategic = arrayOf(0, 1, 3, 5, 10, 20, 30, 50, 75, 100, 150, 200, 250)
}

