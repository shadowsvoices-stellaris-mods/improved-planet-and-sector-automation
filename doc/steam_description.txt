Updated for 3.4.4 Patch. IMPORTANT: Remember to turn off "Colony Automation"!
Improves Planet and Sector automation to make it usable.

[h1]Description[/h1]
The current planet focus-based automation is downright broken and follows a pre-scripted build order with absolutely no flexibility. This is highly problematic and not usable beyond simple colony building. It also does not take any modifiers for things such as amenities, crime and housing into account (neither from the base game or other mods) and does not scale with mod changes to planets in general for example more building slots.

This mod is meant to reduce the tedious micromanagement required for colonies such as ensuring there is enough housing, keeping crime down etc and allow the player to focus on grander decisions while maintaining flexible control over colonies.

[h1]Basic Use[/h1]
- Make sure you planets belong to a sector
- Select a a sector focus in the sector menu (F4)
- Disable Automation on the planets menu (Important, it will block sector automation if left on)
- Set a planet designation on the planets menu (or use auto, but manual selection is recommended for stability)
- Make sure the sector pool has resources to build

[h1]Release notes[/h1]
See the mod's Steam release notes: https://steamcommunity.com/sharedfiles/filedetails/changelog/1764704051

[h1]Feature overview[/h1]

[b]This mod seeks to automate the following aspects:[/b]
- Build as much economy as possible regarding energy, minerals and food
- Maintain a healthy balance of housing, crime, amenities on planets (this does not guarantee that for example amenities won't fall below 0 temporarily, but that's ok)
- Build standard buildings such as resource processing facilities to increase output, unity, spawning pools, clinics, robot assemblies, etc
- Exploit all natural strategic resources
- Ensure a positive balance of alloys, consumer goods, strategic resources, eg. attempt to maintain a positive income of X for those resources but do not spam every slot with them while also making sure that minerals and energy are not exhausted
- Buildings that require resources in upkeep (energy, minerals, food, alloys, consumer goods, strategic) will only be build if that resource has a positive income (with some reasonable exceptions such as holo theaters requiring consumer goods).
- Build special buildings such a Psi Corps and others, economic boosters like foundry buildings, supercomputers, etc.

[b]Sector Focus: Designated[/b]
A new sector focus that manages planets based on their set colony types/designations. Mining worlds will prefer mining districts, energy worlds generator districts and so on. Also builds consumer goods, alloys and strategic resources on demand.

[b]Sector Focus: Mixed[/b]
Similar to designated but with relaxed conditions for designations.

[b]Sector Focus: Strict[/b]
Optimizes colonies like mining by never building generator/farming districts and vice-versa. The same applies for other designation types. Most effective in larger empires and will optimize the output of pops working on planets. Planets running out of districts will instead fallback to unity and strategic resources.

[b]Automation Goals[/b]
- Keep crime below < 8% if nothing else is to do
- Ensure amenities >= 0, housing >= 0
- Build Spawning Pools, Clinics, Robot Assemblies, Templates, ... ("Essentials")
- Build mining/farming/generator districts, focusing on the planets designation if available
- Build buildings that enhance production such as Mineral Purification Planets and Energy Grids (only on planets that have a corresponding designation or several districts of that type)
- Build consumer goods/alloys/strategic resources
- Build research/unity buildings

[h1]Mod Menu[/h1]
The mod menu (located under Edicts) allows you to configure minimum and desired incomes accordingly. It is highly recommended to adapt those to your empire. Options do have a ingame description.

[b]Minimum Income:[/b] Specifies the absolute minimum income required to allow the automation to build any building requiring that resource.
[b]Desired Income:[/b] Specifies the mods desired income for a resource which is the amount of income the mod attempts to maintain. The mod will stop building production of that resource once the desired income has been reached.

[b]More options available for growth, unity, strongholds, etc.[/b]
Controls if the mod will build these buildings if enough income is available.

[h1]Issues / Suggestions / Feature Requests[/h1]
Please report any issues, feature requests or suggestions to build orders. I'll take a look at them asap.

[b]How to report bugs[/b]
Thanks for reporting an issue. Feel free to report it in the comments or forum.
Please include the following information:
[i]All Planet Details (preferably screenshot)[/i]
- Total Pops
- Free Districts
- Unemployed Pops
- Free Amenities
- Free Housing
- Free Building Slots Count
- Planet Designation
- Planet Automation Status (On/Off)
- Planet's sector focus selection (Balanced, Production, Designated, ...)
[i]Empire Details[/i]
- Mineral/Energy/Consumer Goods income
- Alloys/Exotic Gas/Volatile Motes/Rare Crystals income
- Unity Income
- IPA Policies settings

Thank you!

[b]Planned/New Features and Requests[/b]
Let me know in the comments which features are most important and if they are actually possible I'll let you know!

[h1]Colonoy Automation (Abandoned)[/h1]
The planet automation without sector focus introduced by patch 2.3.0 is severely limited. Initially this mod was implemented using it but I soon realized it is severely limited compared to the sector focus.

[h1]Compatibility[/h1]
Should be compatible with everything that does not change the following resources:
- Overwrites everything in "sector_focuses/*"
- Adds "scripted_triggers/ipa_*.txt" files
- Adds "edicts/ipa_*.txt" files
- Adds "defines/01_ipa.txt" file

[h1]Source & Contributions[/h1]
The mod is hosted on Gitlab: https://gitlab.com/shadowsvoices-stellaris-mods/improved-planet-and-sector-automation
Issues, PRs or Feature requests are always welcome.

[b]Contributors[/b]
[b]hypersot[/b] - Thanks for helping me test the mod and providing feedback
[b]Gavril[/b] - Thanks for helping me test the mod and providing feedback
[b]VoidNull[/b] - Thanks for providing the chinese translation.

