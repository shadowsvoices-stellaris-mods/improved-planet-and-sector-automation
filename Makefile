
open-stellaris-code:
	code "$$HOME/.steam/steam/steamapps/common/Stellaris/"

open-stellaris-local:
	code "$$HOME/.local/share/Paradox Interactive/Stellaris/"

open-steam-mods:
	code "$$HOME/.local/share/Steam/steamapps/workshop/content/281990/"

gradle-install-all:
	./gradlew installAll


start-stellaris:
	steam steam://rungameid/281990
